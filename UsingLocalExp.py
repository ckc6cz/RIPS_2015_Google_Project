# -*- coding: utf-8 -*-
"""
comparing local expert to yelp elites
@author: cchandler
"""

from build_expert_dictionaries import exp_dict1
from pymongo import MongoClient

##example of how to find expert of given category
#fash_expert = exp_dict1['Fashion']

##how many categories are the experts an expert in?
#how_expert = {}
#for expert in lxp_list:
#    count = lxp_list.count(expert)
#    how_expert[expert] = count


def make_rcount_dict(exp_list, rcount_dictionary):
    """
    Given a list of uids and a master dictionary of uid to review counts,
    returns a dictionary of just those uids to review count.
    """
    from collections import defaultdict
    exp_rcount = defaultdict(int)
    for exp in exp_list:
        exp_rcount[exp] = rcount_dictionary[exp]
    return exp_rcount


def avg_over_dictionary(rcount_dict):
    return sum(rcount_dict.values()) / float(len(rcount_dict))


def make_all_rcount_dicts():
    """
    Makes a user to review count dictionary from the Charlotte db.
    Creates lists of user_ids for local experts and yelp elites,
    as well as for their union, intersection, and disjoints,
    and for each of those lists creates a user to review count dictionary.
    Returns a dictionary that maps the name of the user list
    to its review count dictionary.
    """

    # get expert to review count dictionaries
    from collections import defaultdict

    rev = MongoClient().char.rev

    rcount = defaultdict(int)
    for review in rev.find():
        rcount[review['user_id']] += 1

    #make a list of local expert ids
    lxp_list = []
    for key, value in exp_dict1.iteritems():
        lxp_list.append(str(value))

    #get list of elite ids
    user = MongoClient().char.user
    elt_list = []
    total = 0
    for u in user.find():
        total += 1
        if len(u['elite']) != 0:
            pretty = str(u['user_id'])
            elt_list.append(pretty)

            #list of the local experts who are not elites
    lxp_not_elt_list = list(set(lxp_list) - set(elt_list))

    #list of the elites who are not local experts
    elt_not_lxp_list = list(set(elt_list) - set(lxp_list))

    # list of all users who are either type of expert
    union_list = set().union(lxp_list, elt_list)
    union_list = list(union_list)

    # list of all users who are both types of expert
    intersect_list = list(set(lxp_list) - set(lxp_not_elt_list))

    lxp_rcount = make_rcount_dict(lxp_list, rcount)  # local experts
    elt_rcount = make_rcount_dict(elt_list, rcount)  # yelp elites
    lxp_not_elt_rcount = make_rcount_dict(lxp_not_elt_list, rcount)  # lxps who are not elts
    elt_not_lxp_rcount = make_rcount_dict(elt_not_lxp_list, rcount)  # elts who are not lxps
    union_rcount = make_rcount_dict(union_list, rcount)  # all users in lxp, elt, or both
    intersect_rcount = make_rcount_dict(intersect_list, rcount)  # overlap of lxp and elt

    dict_of_rcounts = {
        'All Local Experts': lxp_rcount,
        'All Yelp Elites': elt_rcount,
        'Local Experts not Elites': lxp_not_elt_rcount,
        'Elites not Local Experts': elt_not_lxp_rcount,
        'Union': union_rcount,
        'Intersection': intersect_rcount
    }

    return dict_of_rcounts

    #Trying stuff!


dict_of_rcounts = make_all_rcount_dicts()

for name, rcount_dict in sorted(dict_of_rcounts.iteritems()):
    print name
    print 'average review count: ', avg_over_dictionary(rcount_dict)
#    print '25th percentile: ', np.percentile(rcount_dict.values(),25)
#    print '75th percentile: ', np.percentile(rcount_dict.values(),75)

from make_review_count_histogram import make_rcount_hist
make_rcount_hist(dict_of_rcounts['All Yelp Elites'], 1, 'elitereviewcount.pdf')

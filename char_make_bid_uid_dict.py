"""
provides a function to make a dictionary linking bids to uids in charlotte

author: daniel
date: 2015.07.15
"""


def char_make_bid_uid_dict():
    """ returns a bid to uid dictionary for charlotte """
    from pymongo import MongoClient
    from collections import defaultdict

    # connect to the charlotte reviews database
    rev = MongoClient().char.rev

    # build a dictionary linkind bids to uids
    out = defaultdict(set)
    for r in rev.find():
        out[r['business_id']].add(r['user_id'])

    return out

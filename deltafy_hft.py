import cPickle as pickle
import sys


def deltafy_hft(fname):
    delta = {}
    with open(fname, 'r') as f:
        for line in f.readlines():
            try:
                uid, bid, truth, guess = line.split()
                delta[(uid, bid)] = float(guess) - float(truth)
            except ValueError:
                # case where there's a blankline
                continue
    return delta


if __name__ == '__main__':
    args = sys.argv[1:]
    if not args:
        args = raw_input('Please provide filenames\n').split()

    deltas = []
    for arg in args:
        try:
            print 'trying on {}'.format(arg)
            deltas.append(deltafy_hft(arg))
        except:
            print 'failed for {}'.format(arg)

    for index, arg in enumerate(args):
        fname = raw_input(
            'Please provide a filename to save the deltas for {}?\n'.format(
                arg))
        if fname:
            with open(fname, 'w') as f:
                pickle.dump(deltas[index], f)

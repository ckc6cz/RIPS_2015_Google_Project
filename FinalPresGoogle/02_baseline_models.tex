%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BASIC OFFSETS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Baseline Models}

\begin{frame}{Basic Offsets Model} \pause
    Consider a sparse ratings matrix
    \[ R
        = \hspace*{2em} \rotatebox{90}{\textit{\small users}}
        \stackrel{\textit{businesses}}{%
    \begin{bmatrix}
            r_{11} &  & r_{1j} & & & &r_{1n}\\
             &  r_{22} &  & & & &\\
           \vdots &  &  & \ddots & & & \vdots \\
            r_{m1} &  &  & & & & r_{mn}\\
       \end{bmatrix}%
  }
  \]

  where $r_{ij} = $ rating that user $i$ gives to business $j$.

\end{frame}

\begin{frame}{Basic Offsets Model---Explained}
  We compute the overall average
  $$\mu = \overline{R}$$
   \pause
   \begin{equation*} \begin{aligned}
  \label{eq:basic-offsets}
  \includegraphics[scale=0.1]{images/man.png}  \quad  & \alpha_i &=&  \ \overline{u_i} - \mu  &  \overline{u_i} = \text{user i average}\\[1em] \pause
 \includegraphics[scale=0.2]{images/b1.png} \quad & \beta_j &=&  \  \overline{b_j} - \mu & \overline{b_j} = \text{business j average}\\[1em] \pause
\includegraphics[scale=0.2]{images/star.png} \quad & \hat{r_{ij}} &=& \  \mu + \alpha_i + \beta_j
\end{aligned} \end{equation*}

\end{frame}

\begin{frame}{Basic Offsets Model---An Example}
We wish to estimate $r_{ij}$ for user $i$ and business $j$. \\ \pause

Suppose:
\begin{itemize}
  \item overall mean, $\mu = 3$ \pause
  \item user $i$ gives on average 4 stars \pause
  \item business $j$ has a 1 star average \pause
\end{itemize}

$\hat{r_{ij}} = 3 + (4 - 3) + (1 - 3) = 2
  \includegraphics[scale = 0.2]{images/star.png}$

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LATENT FACTORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Latent Factors Model}
We characterize users and businesses on $k$ factors inferred from rating patterns.

\[
\begin{aligned}
\includegraphics[scale=.2]{images/woman.png} & \includegraphics[scale=.2]{images/man.png} & \includegraphics[scale=.2]{images/woman.png} & \includegraphics[scale=.2]{images/man.png} & \includegraphics[scale=.2]{images/woman.png}
\end{aligned}
\]

\[
\begin{aligned}
\includegraphics[scale=.4]{images/b1.png} & \includegraphics[scale=.4]{images/b2.png} & \includegraphics[scale=.4]{images/b3.png}
\end{aligned} \]


\end{frame}

\begin{frame}{Latent Factors for Businesses}
\vspace{-0.75cm}
$$\vec{q} \in \mathbb{R}^k$$
  \begin{figure} \centering
  \includegraphics[scale = 0.5]{images/b1.png}
  \end{figure}
  \begin{equation*} 
    \vec{q} =\rotatebox{90}{ \footnotesize{ $k$ factors}} \begin{bmatrix} service \\ quality \\ price \\ \vdots \\ \end{bmatrix} \pause = \quad
    \begin{bmatrix} 2 \\ 5 \\ 1 \\ 2 \\ \end{bmatrix}
  \end{equation*}
\end{frame}

\begin{frame}{Latent Factors for Users}
\vspace{-0.5 cm}
$$ \vec{p} \in \mathbb{R}^k$$
  \[ \begin{aligned}
     & \includegraphics[scale=0.2]{images/woman.png}
        \qquad & \includegraphics[scale=0.2]{images/man.png} \\
      \vec{p}
      = \begin{bmatrix}
        \text{service} \\
        \text{quality} \\
        \text{price} \\
        \vdots \end{bmatrix}
      = & \begin{bmatrix} 1 \\ 5 \\ 1 \\ 5 \end{bmatrix}
        \qquad
      & \begin{bmatrix} 5 \\ 2 \\ 1 \\ 3 \end{bmatrix}
  \end{aligned} \]
\end{frame}




 \begin{frame}{Latent Factors Model---Rating Generation}

\[ \begin{aligned}
\underset{rating}{\includegraphics[scale=0.17]{images/star.png}} \quad
    & = \underset{\text{user}}{\includegraphics[scale=0.11]{images/man.png}} \quad
        \underset{\text{business}}{\includegraphics[scale=0.23]{images/b1.png}} & \\[1 em]
    \hat{r}_{ij} &= p_i^t \ q_j  \    & p_i &= \text{user vector} \\
     			&		    	   	& q_j &= \text{business vector} \\[1 em] \pause
     R & = P^t  Q   \  \\
    \begin{bmatrix}
        1  &   & 4 & 5 \\
           &   &   & 5 \\
           &  3 & &   \\
         2 & 3 &  & 3 \\
    \end{bmatrix}
        & =
    \begin{bmatrix}
         & p_1^t & \\
         & p_2^t & \\
         & \vdots & \\
         & p_m^t  & \\
    \end{bmatrix}
        &
    \begin{bmatrix}
           &    &    &    \\
        q_1 & q_2 & \cdots & q_n  \\
           &    &    &
    \end{bmatrix}
\end{aligned}
\]


\end{frame}


\begin{frame}{Latent Factors Model---Matrix Factorization}

    \begin{itemize}
    \item We minimize the regularized squared error over $\kappa$

    $\kappa = \cbr{(i,j) : r_{ij} \text{ known rating }}$

    \begin{equation*}
    \label{eq:latent-factors}
    \min_{p,q} \sum_{(i,j) \in \kappa}\del{r_{ij} - p_i^t \cdot q_j}^2 + \lambda \Omega(p_i,q_j)
    %\lambda(\|q\|^2 + \|p\|^2)
    \end{equation*}
    where $\Omega$ is a function introduced to avoid
    overfitting.
    \item In our implementation $\Omega(p_i,q_j) =\|q\|^2 + \|p\|^2$
    \end{itemize}

\end{frame}

\begin{frame}{Latent Factors Model--- Implementation}

\begin{itemize}
\item Package \texttt{recosystem} for \texttt{R} developed by Yixuan Qiu, Chih-Jen Lin, Yu-Chin Juan, Yong Zhuang.
\includegraphics[scale=.4]{images/lf_code.png}
\end{itemize}

\end{frame}
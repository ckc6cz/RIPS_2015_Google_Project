# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 14:35:57 2015

@author: mribero
LBPA
Phase 3: Preference aware candidate selection
Select Local experts for a particular user in a new city
"""
import numpy as np
import pandas as pd
import random
import pdb
from lbpa_compute_similarity import get_user_depth, get_dict_at_level

#Input: the user u and the dictionary  U of expertise per category for each user that has reviewed a particular business.

#Output: Local experts for collaborative filtering


def localExperts(user, expertise, train, b, dictionary):
    n_levels = get_user_depth(user)
    experts = []
    for l in range(n_levels):
        cat, cat_scores = get_dict_at_level(user, l)
        cat = cat_scores.keys()
        wMin = cat_scores[min(cat_scores)]
        for c in cat:
            k = int(np.ceil(abs(cat_scores[c] / wMin)))
            n_exp = 0
            looked = 0
            potencialExperts = []
            maximo = expertise[c].shape[0]
            while n_exp < k and looked < maximo:
                potencialExperts.extend(list(set(expertise[c].filter(
                    dictionary[b]).nlargest(
                        looked + k - n_exp).index))[looked:])
                looked += k - n_exp
                potencialExperts = [temp for temp in potencialExperts
                                    if (temp, b) in train.keys()]
                n_exp = len(potencialExperts)
            experts.extend(potencialExperts)
    return list(set(experts))


def localExperts2(user, expertise, potExperts):
    n_levels = get_user_depth(user)
    experts = []
    for l in range(n_levels):
        cat, cat_scores = get_dict_at_level(user, l)
        cat = cat_scores.keys()
        wMin = cat_scores[min(cat_scores)]
        for c in cat:
            if c.islower():
                continue
            else:
                k = int(np.ceil(abs(cat_scores[c] / wMin)))
                experts.extend(
                    expertise[c].filter(potExperts).nlargest(k).index)
    return list(set(experts))

#####EXAMPLE

#U = {'Food':{'userA':0.9,'userB':0.2,'userC':0.5},'Rest':{'userA':0.8,'userD':0.3,'userB':0.5},'Asian':{'userA':0.2,'userB':0.5},'Italian':{'userA':0.3,'userD':0.3,'userC':0.1}, 'French':{'userB':0.8,'userA':0.3},'Cafe':{'userA':0.3,'userB':0.5},'Entertainment':{'userA':0.8,'userB':0.3,'userC':0.5}}

#dictionary = bid_uid_dict
#userA = {('Food',1.0): {('Rest',.5): {('Asian',.3): {},('Italian',.3):{}, ('French',.1):{}}, ('Cafe',.3): {}}}
#userB = {('Food',0.8): {('Rest',.5): {('Asian',.1): {},('Italian',.4):{}}, ('Cafe',.3): {}}, ('Entertainment',.2):{}}
#userC = {('Food',1.0): {('Rest',.5): {('Asian',random.uniform(0,1)): {},('Italian',.3):{}, ('French',.1):{}}, ('Cafe',random.uniform(0,1)): {}}}
#userD = {('Food',random.uniform(0,1)): {('Rest',random.uniform(0,1)): {('Asian',random.uniform(0,1)): {},('Italian',.4):{}}, ('Cafe',random.uniform(0,1)): {}}, ('Entertainment',.2):{}}

#experts = localExperts(userA,U)

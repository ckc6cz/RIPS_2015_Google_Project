"""
Look through all cities, tabulating the average star rating per-city

City: all

Author: Daniel
"""

# want to compare avg star ratings with cities
# does not use individual ratings but instead merely only business avg ratings

from assign_true_cities import *
import numpy as np
import csv

# pull in the data and assign each business its proper true city
businesses = import_businesses()
assign_true_cities(businesses)

# get a short-list of the 10 true cities
true_city_list = set()
for business in businesses:
    true_city_list.add(business['true_city'])

# create a dict for cities and all bussiness avg ratings
city_avg_ratings = {}
for city in true_city_list:
    city_avg_ratings[city] = []

for business in businesses:
    city_avg_ratings[business['true_city']].append(business['stars'])

# write each city and rating to a csv file "dict.csv"
f = open('dict.csv', 'wb')
writer = csv.writer(f)
for key, value in city_avg_ratings.items():
    writer.writerow([key, value])
f.close()

# create a dict for cities and their overall avg ratings
city_avg_rating = city_avg_ratings
for city in city_avg_rating:
    city_avg_rating[city] = \
		[np.mean(city_avg_ratings[city]), len(city_avg_ratings[city])]

# spoiler alert: there's basically no correlation between number of reviews in
# city and the number of reviews documented in that city
# additionally, all cities have roughly the same average

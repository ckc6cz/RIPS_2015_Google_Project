"""
Build User-Business Ratings Matrix

city: all

author: daniel
"""

from pymongo import MongoClient

client = MongoClient()
db = client.yelpdb

user_list = db.user.distinct('user_id')
bus_list = db.bus.distinct('business_id')

# as a Mongo databse
for review in db.review.find():
    db.rating.insert_one({
        'user_id': review['user_id'],
        'business_id': review['business_id'],
        'stars': review['stars']
    })

# as a SparseDataFrame (slow to make)
from pandas import SparseDataFrame
rating_df = SparseDataFrame(index=user_list, columns=bus_list)

# as a straight numpy array (fails - no memory)
import numpy as np
# rating_np = np.zeros([len(user_list), len(bus_list)], 
#      dtype = [('user_id', 'a22'), ('business_id', 'a22'), ('stars', 'f4')])

# as a sparse (i, j, k) matrix (fails - can't figure out how to append)
from scipy.sparse import coo_matrix
# rating_sp = coo_matrix((len(user_list), len(bus_list)),
#     [('user_id', 'a22'), ('business_id', 'a22'), ('stars', 'f4')])

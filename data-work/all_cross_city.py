from pymongo import MongoClient
from collections import defaultdict
from all_import_bus import *
from all_assign_true_city import *

db = MongoClient().yelpdb

bus = import_businesses()
assign_true_cities(bus)

bid_to_city = {b['business_id']: b['true_city'] for b in bus}

uid_to_city = defaultdict(list)
for r in db.review.find():
    uid_to_city[r['user_id']].append(bid_to_city[r['business_id']])

"""
author: Daniel Metz
date: 2015.06.24

description: 
The following will add the field "master_category" to each individual business
so that each business can be sorted more easily into only the most popular of
its listed categories.

example: 
    # pull in the data and assign each business its proper true city
    from import_businesses import *
    businesses = import_businesses()
    assign_true_cities(businesses)
"""


def gen_all_category_counts(businesses):
    """
	returns a dictionary containing every business category and respective
	occurance counts

	e.g. {restaurant: 4,
		  grocery: 3}
	"""
    type_counts = {}
    for business in businesses:
        for category in business['categories']:
            type_counts[category] = type_counts.get(category, 0) + 1
    return type_counts


def gen_master_category_counts(businesses):
    """
	returns a dictionary like produced by gen_all_category_counts() but
	assigns each business to only one category, selected by assignment
	to the most-populous of the possible category assignments
	"""
    # get the complete category count dictionary
    type_counts = gen_all_category_counts(businesses)

    # convert the dictionary into a list of tuples to allow sorting
    type_count_tuples = type_counts.items()
    counts = []
    for pair in type_count_tuples:
        counts.append(pair[1])

    master_type_counts = sorted(type_count_tuples,
                                key=lambda pair: pair[1],
                                reverse=True)

    # repeat above, but assign each business to only its most popular tag
    master_counts = {}
    for business in businesses:
        # case for no business categories:
        if not business['categories']:
            continue

        # all other cases
        max_cat = [business['categories'][0], 0]
        for category in business['categories']:
            if type_counts[category] > max_cat[1]:
                max_cat[0] = category
                max_cat[1] = type_counts[category]
        master_counts[max_cat[0]] = master_counts.get(max_cat[0], 0) + 1

    return master_counts


def assign_master_category(businesses):
    """
	creates a new "master_category" field for each object, assigning to this
	field the most popular of all listed categories for each particular business	
	"""
    # generate the master category dictionary
    master_categories = gen_master_category_counts(businesses)

    # iterate through each business and make assignments
    for business in businesses:
        # case for no business categories
        if not business['categories']:
            continue

        # all other cases
        business['master_category'] = business['categories'][0]
        for category in business['categories']:
            if category in master_categories:
                business['master_category'] = category
                continue

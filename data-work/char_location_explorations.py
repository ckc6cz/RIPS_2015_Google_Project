"""
produce some basic data exploration using the charlotte data

author: daniel
"""

from pymongo import MongoClient
import numpy as np

# connect to Mongo and alias the bus table
client = MongoClient()
bus = client.char.bus
rev = client.char.rev

# make bid to review count dict
bids = rev.distinct('business_id')
bid_rev_count = {bid: rev.find({'business_id': bid}).count() for bid in bids}
# make bid to lat and bid to long dicts
bid_lat = {
    bid: bus.find({'business_id': bid}).distinct('latitude')[0]
    for bid in bids
}
bid_long = {
    bid: bus.find({'business_id': bid}).distinct('longitude')[0]
    for bid in bids
}

# prepare the matrix for export into R for graphing
out = np.empty((len(bids), 3))
for bid in bids:
    out[bids.index(bid), :] = bid_lat[bid], bid_long[bid], bid_rev_count[bid]

# write the file
np.savetxt('latlong_rev_counts.csv', out,
           delimiter=',',
           header='lat, long, rev_count')

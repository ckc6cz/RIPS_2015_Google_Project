from pymongo import MongoClient
from all_import_bus import *
from all_assign_true_city import *

db_bus = MongoClient().yelpdb.bus

bus = import_businesses()
assign_true_cities(bus)

for b in bus:
    bid = b['business_id']
    tc = b['true_city']
    db_bus.update_one({'business_id': bid}, {'$set': {'true_city': tc}})

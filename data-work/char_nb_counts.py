"""
Generate a histogram of Charlotte neighborhood counts

City: Charlotte

Author: Daniel
"""
from pymongo import MongoClient
from collections import defaultdict

# connect to the mongo database
client = MongoClient()
# alias the business table
bus = client.char.bus

# initialize a dictionary whose default value is 0
nb_count = defaultdict(int)
# increment the neighborhood count once per neighborhood listed by each business
for b in bus.find():
    for nb in b['neighborhoods']:
        nb_count[nb] += 1

# count the number of businesses that list no neighborhood at all
no_nb = 0
for b in bus.find():
    if not b['neighborhoods']:
        no_nb += 1

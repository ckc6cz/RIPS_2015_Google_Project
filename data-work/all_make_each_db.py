from pymongo import MongoClient

city_db_choices = {
    'Phoenix': MongoClient().phoe,
    'Edinburgh': MongoClient().edin,
    'Montreal': MongoClient().mont,
    'Madison': MongoClient().madi,
    'Charlotte': MongoClient().char,
    'Las Vegas': MongoClient().vegas,
    'Karlsruhe': MongoClient().karl,
    'Pittsburgh': MongoClient().pitt,
    'Urbana-Champaign': MongoClient().urbana,
    'Waterloo': MongoClient().water
}


def makeCityDB():
    # connect to yelpdb
    allbus = MongoClient().yelpdb.bus
    allrev = MongoClient().yelpdb.rev
    alluser = MongoClient().yelpdb.user

    # create a {bid: true_city} dictionary
    bid_city = {bus['business_id']: bus['true_city'] for bus in allbus.find()}

    for city in city_db_choices:
        db = city_db_choices[city]
        if db.rev.find().count():
            print 'skipping {city}'.format(city=city)
            continue
        else:
            print 'starting on {city}'.format(city=city)

        # insert businesses into city db if appropriate
        db.bus.insert_many([bus for bus in allbus.find({'true_city': city})])
        print '    inserted', db.bus.find().count(), 'businesses into', city
        db.bus.create_index('business_id')
        print '    created bus-bid index for', city

        db.rev.insert_many([rev for rev in allrev.find()
                            if bid_city[rev['business_id']] == city])
        print '    inserted', db.rev.find().count(), 'reviews into', city
        db.rev.create_index('user_id')
        print '    created rev-uid index for', city
        db.rev.create_index('business_id')
        print '    created rev-bid index for', city

        # now for users
        city_uids = set(db.rev.distinct('user_id'))
        db.user.insert_many([user for user in alluser.find()
                             if user['user_id'] in city_uids])
        print '    inserted', db.user.find().count(), 'users into', city
        db.user.create_index('user_id')
        print '    created user-uid index for', city

        # make some indexes!
        print '    finished for', city

from pymongo import MongoClient
from collections import defaultdict
import pandas as pd

# connect to the mongo client and alias the bus table
client = MongoClient()
bus = client.char.bus
rev = client.char.rev

# create a dict linking business ids to categories
bid_to_cat = {b['business_id']: b['categories'] for b in bus.find()}

# count how many reviews there are for businesses of each category type
cat_counts = defaultdict(int)
for r in rev.find():
    bid = r['business_id']
    cats = bid_to_cat[bid]
    for cat in cats:
        cat_counts[cat] += 1

cat_counts_sorted = pd.Series(cat_counts, index=cat_counts.keys())
cat_counts_sorted.sort()

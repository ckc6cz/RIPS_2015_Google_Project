"""
author: Daniel Metz
date: 2015.07.29
"""

import ujson


def import_businesses():
    """
	assumes the "yelp_academic_dataset_business" is in the current working
	directory.

	returns the properly loaded list of json objects
	"""
    with open(
        '../../data/yelp_academic_dataset_business.json') as business_file:
        businesses = [ujson.loads(line) for line in business_file]
    return businesses

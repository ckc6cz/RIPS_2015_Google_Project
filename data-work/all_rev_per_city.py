"""
get a count of reviews per true_city

city: all

author: daniel
"""

# load businesses
from import_businesses import *
businesses = import_businesses()

# assign true_city
from assign_true_cities import *
assign_true_cities(businesses)

# convert to pandas df
import pandas as pd
bus_df = pd.DataFrame(businesses)


# make a simple function that gets a city given a business id and a pandas df
def get_true_city(bus_id, df):
    return df.query("business_id == @bus_id")['true_city'].ix[0]


def get_true_city(bus_id, df):
    return df[df['business_id'] == bus_id]['true_city'][0]

# read in the reviews
import ujson
with open('../data/yelp_academic_dataset_review.json') as f:
    review_list = [ujson.loads(line) for line in f]
    review_df = pd.DataFrame(review_list)

city_review_counts = {}
for index, row in review_df.iterrows():
    true_city = get_true_city(row['business_id'], bus_df)
    print true_city
    city_review_counts[true_city] = \
        city_review_counts.get(true_city, 0) + 1

from pprint import pprint
pprint(city_review_counts)

review_df.apply(lambda row: get_true_city(row['business_id'], bus_df), axis=1)

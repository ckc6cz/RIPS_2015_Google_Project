# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 14:16:02 2015

@author: cchandler
"""

import numpy as np
from sklearn.cross_validation import KFold
from collections import defaultdict
from pymongo import MongoClient
import sys
import cPickle as pickle

# connect to relevant collections
rev = MongoClient().yelpdb.rev
bus = MongoClient().yelpdb.bus


def make_dolla_rev_dictionary(cities=None):
    """ make {dolla: {(uid, bid): stars}} """
    if not cities:
        cities = set(bus.distinct('true_city'))

    bid_city = {b['business_id']: b['true_city'] for b in bus.find()}

    #business to price range dictionary
    bid_to_dolla = {
        b['business_id']: b['attributes']['Price Range']
        for b in bus.find()
        if 'Price Range' in b['attributes'] and bid_city[b['business_id']
                         ] in cities
    }
    bid_to_dolla = defaultdict(int)
    for b in bus.find():
        if bid_city[b['business_id']] not in cities:
            continue
        try:
            bid_to_dolla[b['business_id']] = b['attributes']['Price Range']
        except KeyError:
            bid_to_dolla[b['business_id']] = 0

    #price to bids dictionary
    dolla_to_bids = defaultdict(list)
    for bid, dolla in bid_to_dolla.iteritems():
        dolla_to_bids[dolla].append(bid)

    dolla_rev = defaultdict(dict)
    for r in rev.find():
        uid, bid, star = r['user_id'], r['business_id'], r['stars']
        city = bid_city[bid]
        if city in cities:
            dolla_rev[bid_to_dolla[bid]].update({(uid, bid): star})

    return dolla_rev


def basic_offset_prediction(data):
    """
    args:
        data (dict): {dolla: {(uid, bid): rating}}

    returns:
        dict: {(uid, bid): rating} filled by basic offset method
    """

    # prepare to set-up training and test sets
    pred = defaultdict(list)

    for _, dolla_data in data.iteritems():
        uid_bid_keys = np.array(dolla_data.keys())
        kf = KFold(len(dolla_data), n_folds=5, shuffle=True)
        for train_index, test_index in kf:
            train, test = uid_bid_keys[train_index], uid_bid_keys[test_index]
            uid_ratings, bid_ratings = defaultdict(list), defaultdict(list)

            # prepare for offset calculations
            for (uid, bid) in train:
                uid_ratings[uid].append(dolla_data[(uid, bid)])
                bid_ratings[bid].append(dolla_data[(uid, bid)])

            # calculate offsets
            dolla_mu = np.mean([dolla_data[(uid, bid)] for (uid, bid) in train
                                ])
            uid_offset, bid_offset = defaultdict(int), defaultdict(int)
            uid_offset.update({
                uid: np.mean(uid_ratings[uid]) - dolla_mu
                for uid in uid_ratings
            })
            bid_offset.update({
                bid: np.mean(bid_ratings[bid]) - dolla_mu
                for bid in bid_ratings
            })
            # make predictions!
            for (uid, bid) in test:
                pred[(uid, bid)].append(
                    dolla_mu + uid_offset[uid] + bid_offset[bid])

    for (uid, bid), predictions in pred.iteritems():
        pred[(uid, bid)] = np.mean(predictions)

    return pred


if __name__ == '__main__':
    args = set(sys.argv[1:])
    if not args:
        args = raw_input(
            'Choose input cities (e.g. \'Charlotte Pittsburgh\')\n')
        args = {arg.capitalize() for arg in args.split()}
    cities = args.intersection(bus.distinct('true_city'))

    print 'formatting the dolla dolla bills for the following cities y\'all...'
    print '{}'.format(cities)
    data = make_dolla_rev_dictionary(cities)

    print 'generating predictions...'
    pred = basic_offset_prediction(data)

    print 'comparing for results...'
    flatdata = {
        (uid, bid): rating
        for cat in data.values() for (uid, bid), rating in cat.items()
    }
    delta = {
        (uid, bid): pred[(uid, bid)] - flatdata[(uid, bid)]
        for (uid, bid) in pred
    }

    rmse = np.sqrt(np.mean(np.array(delta.values()) ** 2))
    print 'rmse:', rmse

    fname = raw_input('To save the data, please give a filename\n')
    if fname:
        with open(fname, 'w') as f:
            pickle.dump(delta, f)

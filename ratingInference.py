# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 10:08:01 2015

@author: mribero

given a user and the local experts for that particular user calculates the predicted rating
"""
import numpy as np
from lbpa_compute_similarity import similarity
from PrefAwareCandidates import localExperts

#This function creates a dense matrix with the relevant ratings
#input: 
#user: preference tree from the user we try to predict
#experts: dictionary where the keys are the local experts for the user 'user' and their preference trees
#ratings: dictionary with the rating from each local expert to the particular business we want to predict

#out
#predicted rating using weights and predicted rating using the most similar


#compute rating weighting by similarity
def computeRating(user, experts, ratings):
    sim = {i: similarity(user, experts[i]) for i in experts.keys()}
    total = sum([sim[i] for i in sim.keys()])
    simN = {i: sim[i] / total for i in sim.keys()}
    mostSim = max(sim.iterkeys(), key=(lambda key: sim[key]))
    return sum([simN[i] * ratings[i] for i in sim.keys()]), ratings[mostSim]

    ##TEST

    #U = {'Food':{'userA':0.9,'userB':0.2,'userC':0.5},'Rest':{'userA':0.8,'userD':0.3,'userB':0.5},'Asian':{'userA':0.2,'userB':0.5},'Italian':{'userA':0.3,'userD':0.3,'userC':0.1}, 'French':{'userB':0.8,'userA':0.3},'Cafe':{'userA':0.3,'userB':0.5},'Entertainment':{'userA':0.8,'userB':0.3,'userC':0.5}}

    #userA = {('Food',1.0): {('Rest',.5): {('Asian',.3): {},('Italian',.3):{}, ('French',.1):{}}, ('Cafe',.3): {}}}
    #userB = {('Food',0.8): {('Rest',.5): {('Asian',.1): {},('Italian',.4):{}}, ('Cafe',.3): {}}, ('Entertainment',.2):{}}
    #userC = {('Food',1.0): {('Rest',.5): {('Asian',random.uniform(0,1)): {},('Italian',.3):{}, ('French',.1):{}}, ('Cafe',random.uniform(0,1)): {}}}
    #userD = {('Food',random.uniform(0,1)): {('Rest',random.uniform(0,1)): {('Asian',random.uniform(0,1)): {},('Italian',.4):{}}, ('Cafe',random.uniform(0,1)): {}}, ('Entertainment',.2):{}}

    #experts = {'userC':userC, 'userB':userB}
    #ratings = {'userB':5,'userC':4}

    #computeRating(userA,experts,ratings)

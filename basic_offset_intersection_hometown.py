import numpy as np
from collections import defaultdict, Counter
from pymongo import MongoClient
from lbpa_nonparallel import bid_city
import cPickle as pickle


def basic_offset_prediction(train, test):
    """
    args:
        train (dict): {city: {(uid, bid): rating}}
        test (dict): {city: {(uid, bid): rating}}

    returns:
        dict: {city: {(uid, bid): rating}} filled by basic offset method
    """

    # prepare to set-up training and test sets
    pred = defaultdict(dict)

    for city in test:
        uid_ratings, bid_ratings = defaultdict(list), defaultdict(list)

        # prepare for offset calculations
        for (uid, bid), stars in train[city].iteritems():
            uid_ratings[uid].append(stars)
            bid_ratings[bid].append(stars)

        # calculate offsets
        mu = np.mean([stars for _, stars in train[city].iteritems()])
        uid_offset, bid_offset = defaultdict(int), defaultdict(int)
        uid_offset.update(
            {uid: np.mean(uid_ratings[uid]) - mu
             for uid in uid_ratings})
        bid_offset.update(
            {bid: np.mean(bid_ratings[bid]) - mu
             for bid in bid_ratings})

        # make predictions!
        pred[city].update({
            (uid, bid): mu + uid_offset[uid] + bid_offset[bid]
            for (uid, bid) in test[city]
        })

    return pred


if __name__ == '__main__':
    rev = MongoClient().yelpdb.rev
    bus = MongoClient().yelpdb.bus

    print 'assigning home cities for each user'
    uid_citycount = defaultdict(Counter)
    for r in rev.find():
        uid_citycount[r['user_id']][bid_city[r['business_id']]] += 1

    uid_homecity = {
        uid: citycount.most_common(1)[0][0]
        for uid, citycount in uid_citycount.iteritems()
    }

    print 'identifying test uids'
    test_uids = {
        r['user_id']
        for r in rev.find() if bid_city[r['business_id']] == 'Las Vegas' and
        uid_homecity[r['user_id']] != 'Las Vegas'
    }

    print 'assembling the test and training data'
    test, train = defaultdict(dict), defaultdict(dict)
    uid_bid = defaultdict(list)
    for r in rev.find():
        uid = r['user_id']
        bid = r['business_id']
        bcity = bid_city[bid]
        stars = r['stars']
        if uid in test_uids:
            uhomecity = uid_homecity[uid]
            if uhomecity == bcity:
                test[bcity][(uid, bid)] = stars
                uid_bid[uid].append(bid)
        else:
            train[bcity][(uid, bid)] = stars
            uid_bid[uid].append(bid)

    print 'generating predictions...'
    pred = basic_offset_prediction(train, test)
    for city in test:
        print '  for {}'.format(city)
        delta = [pred[city][(uid, bid)] - test[city][(uid, bid)]
                 for (uid, bid) in pred[city]]
        delta = np.array(delta)
        rmse = np.sqrt(np.mean(delta ** 2))
        print '\n  rmse for {}: {}'.format(city, rmse)

    save = raw_input(
        'Would you like to save the results? (y/[n])\n').lower() == 'y'
    if save:
        suggested_fname = 'basic_offset_intersection_hometown.pickle'
        fname = raw_input(
            'Where would you like to save the results? [{}]\n'.format(
                suggested_fname))
        fname = fname if fname else suggested_fname
        print 'saving the predictions to {}'.format(fname)
        with open(fname, 'w') as f:
            pickle.dump(pred, f)

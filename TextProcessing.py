# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 10:16:50 2015

@author: mribero

Description: Code to preprocess text before using LDA for topic modeling
"""

import nltk.stem
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer

#removes words that are too frequent
english_stemmer = nltk.stem.SnowballStemmer('english')


class StemmedCountVectorizer(CountVectorizer):
    def build_analyzer(self):
        analyzer = super(StemmedCountVectorizer, self).build_analyzer()
        return lambda doc: (english_stemmer.stem(w) for w in analyzer(doc))


vectorizer = StemmedCountVectorizer(min_df=1, stop_words='english')


#removes words that are too frequent but also the ones in many documents
class StemmedTfidfVectorizer(TfidfVectorizer):
    def build_analyzer(self):
        analyzer = super(TfidfVectorizer, self).build_analyzer()
        return lambda doc: (english_stemmer.stem(w) for w in analyzer(doc))
        vectorizer = StemmedTfidfVectorizer(min_df=1,
                                            stop_words='english',
                                            charset_error='ignore')

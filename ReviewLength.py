# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 09:34:22 2015

@author: cchandler
"""


def find_av_rev_len(users):
    """
    given a list of users, find the average number of words used in
    their reviews
    """

    from pymongo import MongoClient
    from collections import defaultdict

    rev = MongoClient().char.rev

    #make a dictionary that maps user id to a list of each review text
    user_to_text = defaultdict(list)
    for user in users:
        reviews = [r['text'] for r in rev.find({'user_id': user})]
        user_to_text[user] = reviews

    #make a dictionary that maps user id to a list of word length of each review
    user_to_lengths = defaultdict(list)
    for user, all_text in user_to_text.iteritems():
        lengths = []
        for one_review in all_text:
            lengths.append(len(one_review.split()))
        user_to_lengths[user] = lengths

        #make a dictionary that maps user id to their average review length
    user_to_av = defaultdict(int)
    for user, lengths in user_to_lengths.iteritems():
        user_to_av[user] = (sum(lengths) / len(lengths))

    #get the average for ALL charlotte users
    av_length = sum(user_to_av.values()) / len(user_to_av.values())

    return av_length


def find_av_char_amount(users):
    """
    given a list of users, find the average number of characters used in
    their reviews
    """

    from pymongo import MongoClient
    from collections import defaultdict

    rev = MongoClient().char.rev

    #make a dictionary that maps user id to a list of each review text
    user_to_text = defaultdict(list)
    for user in users:
        reviews = [r['text'] for r in rev.find({'user_id': user})]
        user_to_text[user] = reviews

    #make a dictionary that maps user id to a list of word length of each review
    user_to_lengths = defaultdict(list)
    for user, all_text in user_to_text.iteritems():
        lengths = []
        for one_review in all_text:
            lengths.append(len(one_review))
        user_to_lengths[user] = lengths

        #make a dictionary that maps user id to their average review length
    user_to_av = defaultdict(int)
    for user, lengths in user_to_lengths.iteritems():
        user_to_av[user] = (sum(lengths) / len(lengths))

    #get the average for ALL charlotte users
    av_length = sum(user_to_av.values()) / len(user_to_av.values())

    return av_length

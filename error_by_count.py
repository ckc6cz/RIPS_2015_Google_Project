# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 14:46:40 2015

@author: cchandler
"""


def error_calculator():
    from pymongo import MongoClient
    from collections import defaultdict
    import numpy as np

    rev = MongoClient().phoe.rev

    #make a dictionary of uid to review count
    rcount = defaultdict(int)
    for review in rev.find():
        rcount[review['user_id']] += 1

        #list of users with only num reviews   
        #    num_count = []
        #    for user, count in rcount.iteritems():
        #        if count == number:
        #            num_count.append(user)

        #read in predictions file (uid, bid, actual, prediction)
        #store in a dictionary of uid -> error
    uid_error = {}
    file = open(
        '/home/cchandler/anaconda/RIPS_2015_Google_Project/phoe_hft_predictions.out')
    for line in file:
        fields = line.strip().split()
        if len(fields) != 0:
            uid = fields[0]
            actual = float(fields[2])
            prediction = float(fields[3])
            error = np.sqrt(np.mean((float(actual) - float(prediction)) ** 2))
            uid_error[uid] = error

    count_to_error = defaultdict(list)
    for uid, count in rcount.iteritems():
        count_to_error[count].append(uid_error[uid])

    return count_to_error
#    num_errors = []    
#    for user in num_count:
#        num_errors.append(uid_error[user])
#    
#    #print "error for users with %f reviews: %f"%(number, sum(num_errors)/len(num_errors))
#    return sum(num_errors)/len(num_errors)


def latexify(fig_width=None, fig_height=None, columns=1):
    """Set up matplotlib's RC params for LaTeX plotting.
    Call this before plotting a figure.
    
    Parameters
    ----------
    fig_width : float, optional, inches
    fig_height : float,  optional, inches
    columns : {1, 2}
    """

    # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

    # Width and max height in inches for IEEE journals taken from
    # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    import matplotlib
    from math import sqrt

    assert (columns in [1, 2])

    if fig_width is None:
        fig_width = 3.39 if columns == 1 else 6.9  # width in inches

    if fig_height is None:
        golden_mean = (sqrt(5) - 1.0) / 2.0  # Aesthetic ratio
        fig_height = fig_width * golden_mean  # height in inches

    MAX_HEIGHT_INCHES = 8.0
    if fig_height > MAX_HEIGHT_INCHES:
        print("WARNING: fig_height too large:" + fig_height +
              "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
        fig_height = MAX_HEIGHT_INCHES

    params = {
        'backend': 'ps',
        'text.latex.preamble': ['\usepackage{gensymb}'],
        'axes.labelsize': 8,  # fontsize for x and y labels (was 10)
        'axes.titlesize': 8,
        'font.size': 8,  # was 10
        'legend.fontsize': 8,  # was 10
        'xtick.labelsize': 8,
        'ytick.labelsize': 8,
        'text.usetex': True,
        'figure.figsize': [fig_width, fig_height],
        'font.family': 'serif'
    }

    matplotlib.rcParams.update(params)


import numpy as np
import matplotlib.pyplot as plt

count_error = error_calculator()
counts = []
errors = []
for key, value in count_error.iteritems():
    for x in np.arange(len(value)):
        counts.append(key)
    for v in value:
        errors.append(v)

latexify()
plt.scatter(counts, errors, color="black", s=1)
plt.xlim(0)
plt.ylim(0)
plt.title("Number of Reviews v Magnitude of Errors")
plt.xlabel("Number of Reviews")
plt.ylabel("Magnitude of Errors")
plt.tight_layout()
plt.savefig("error_trend_PHOE_user.png", dpi=300)
plt.clf()

#from pymongo import MongoClient
#from collections import defaultdict
#rev = MongoClient().char.rev  
#
#rcount = defaultdict(int)
#for review in rev.find():
#    rcount[review['user_id']] += 1
#count_to_error = {}
#for count in np.arange(1, 20):
#    count_to_error[count] = error_calculator(count)
#plt.plot(count_to_error.keys(), count_to_error.values())
#plt.title("RMSE vs review count")
#plt.xlabel("Review Count")
#plt.ylabel("RMSE")
#plt.bar(count_to_error.keys(), count_to_error.values())
#plt.title("RMSE vs review count for Phoe")
#plt.xlabel("Review Count")
#plt.ylabel("RMSE")
#plt.show()

# -*- coding: utf-8 -*-
"""
calculate basic local expert
@author: cchandler
"""

from pymongo import MongoClient
from collections import defaultdict
import pickle
import pandas as pd


def expert_dictionary(number):
    """
    if number == 1: returns a dictionary that maps a category to a user id who 
    has the highest visit count to that category
    if number == n: return a dictionary that maps a category to a user id who 
    has the nth highest visit count to that category
    """
    #open users to businesses dictionary
    with open('./charlotte/char_uid_bid_dict.pickle', 'r') as f:
        user_to_businesses = pickle.load(f)

    #get database info
    client = MongoClient()
    bus = client.char.bus
    user = client.char.user

    #create a dictionary that relates businesses to their categories
    bid_to_cat = {}
    for b in bus.find():
        bid_to_cat[b['business_id']] = b['categories']

    #create a dictionary that relates users to categories they review for
    user_to_cat = defaultdict(list)
    for b in bus.find():
        for u, bu in user_to_businesses.iteritems():
            if b['business_id'] in bu:
                user_to_cat[u].append(b['categories'])

    #make users' categories into a single list
    for u, c in user_to_cat.iteritems():
        newcat = reduce(lambda x, y: x + y, c)
        user_to_cat[u] = newcat

        #dictionary that relates user to a dictionary of their categories with counts of each
    for key, value in user_to_cat.iteritems():
        dict_of_counts = {}
        for v in value:
            if v in dict_of_counts:
                dict_of_counts[v] += 1
            else:
                dict_of_counts[v] = 1
        user_to_cat[key] = dict_of_counts

        #dictionary with key: category and value: vector that associates user id with category count
    cats = bus.distinct('categories')
    cat_vects = {}
    for cat1 in cats:
        counts = []
        index = []
        for user, diict in user_to_cat.iteritems():
            for cat, num in diict.iteritems():
                if cat == cat1:
                    counts.append(num)
                    index.append(user)
                    cat_vects[cat] = pd.Series(counts, index=index)
    for key, value in cat_vects.iteritems():
        value.sort()

        #creat a dictionary that maps category to a users name with the highest count for that category
    local_experts = {}
    for key, value in cat_vects.iteritems():
        if len(value) >= number:
            nth_val = value.index[-number]
            local_experts[key] = nth_val
        else:
            local_experts[key] = "N/A"

    return local_experts

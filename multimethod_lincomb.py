def gridsearch(bodeltas, lbpadeltas, n=50):
    import numpy as np
    alphas = np.linspace(0, 1, num=n)
    best_alpha = None
    best_error = np.sqrt(np.mean(np.array(bodeltas.values()) ** 2))
    for alpha in alphas:
        er = rmse(bodeltas, lbpadeltas, alpha)
        #     if er < best_error:
        #         best_alpha = alpha
        #         best_error = er
        # print 'best alpha is', best_alpha
        # print 'best rmse is', best_error
        print '(alpha, rmse) is {:.2f}, {:.4f}'.format(alpha, er)


def rmse(d1, d2, alpha):
    import numpy as np
    error = [alpha * d1[uid, bid] + (1 - alpha) * d2[uid, bid]
             for uid, bid in d1]
    return np.sqrt(np.mean(np.array(error) ** 2))

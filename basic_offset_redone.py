# basic offsets redone
from pymongo import MongoClient
from sklearn.cross_validation import KFold
from collections import defaultdict
import cPickle as pickle
import numpy as np
import sys

# connect to relevant collections
rev = MongoClient().yelpdb.rev
bus = MongoClient().yelpdb.bus


def make_master_dictionary(cities=None):
    """
    args:
        cities (list of str): list of cities to include
            if empty, assumes all cities are desired

    returns:
        dict: {(uid, bid): rating}
    """
    if not cities:
        cities = bus.distinct('true_city')
    bid_city = {b['business_id']: b['true_city'] for b in bus.find()}
    return {
        (r['user_id'], r['business_id']): r['stars']
        for r in rev.find() if bid_city[r['business_id']] in cities
    }


def basic_offset_prediction(data):
    """
    args:
        data (dict): {(uid, bid): rating}

    returns:
        dict: {(uid, bid): rating} filled by basic offset method
    """

    # prepare to set-up training and test sets
    uid_bid_keys = np.array(data.keys())
    pred = {}

    kf = KFold(len(data), n_folds=5, shuffle=True)
    for train_index, test_index in kf:
        train, test = uid_bid_keys[train_index], uid_bid_keys[test_index]
        uid_ratings, bid_ratings = defaultdict(list), defaultdict(list)

        # prepare for offset calculations
        for (uid, bid) in train:
            uid_ratings[uid].append(data[(uid, bid)])
            bid_ratings[bid].append(data[(uid, bid)])

        # calculate offsets
        mu = np.mean([data[(uid, bid)] for (uid, bid) in train])
        uid_offset, bid_offset = defaultdict(int), defaultdict(int)
        uid_offset.update(
            {uid: np.mean(uid_ratings[uid]) - mu
             for uid in uid_ratings})
        bid_offset.update(
            {bid: np.mean(bid_ratings[bid]) - mu
             for bid in bid_ratings})

        # make predictions!
        pred.update({
            (uid, bid): mu + uid_offset[uid] + bid_offset[bid]
            for (uid, bid) in test
        })

    return pred


if __name__ == '__main__':
    args = set(sys.argv[1:])
    if not args:
        args = raw_input(
            'Choose input cities (e.g. \'Charlotte Pittsburgh\')\n')
        args = {arg.capitalize() for arg in args.split()}
    if 'English' in args:
        cities = {'Charlotte', 'Edinburgh', 'Las Vegas', 'Madison', 'Waterloo',
            'Phoenix', 'Pittsburgh', 'Urbana-Champaign'}
    cities = args.intersection(bus.distinct('true_city'))

    if cities:
        print 'running on\n  {}'.format(cities)

    print 'gathering the data...'
    data = make_master_dictionary(cities)

    print 'running predictions...'
    pred = basic_offset_prediction(data)

    print 'calculating results...'
    delta = {
        (uid, bid): guess - data[(uid, bid)]
        for (uid, bid), guess in pred.iteritems()
    }
    rmse = np.sqrt(np.mean(np.array(delta.values()) ** 2))
    print 'rmse: {:f}'.format(rmse)

    fname = raw_input('To save the result, please give a filename\n')
    if fname:
        with open(fname, 'w') as f:
            pickle.dump(delta, f)

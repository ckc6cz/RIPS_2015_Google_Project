# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 14:22:32 2015

@author: tlichter
"""

from pymongo import MongoClient
import cPickle as pickle

userdb = MongoClient().char.user
elt_list = []
total = 0
for u in userdb.find():
    total += 1
    if len(u['elite']) != 0:
        pretty = str(u['user_id'])
        elt_list.append(pretty)

with open('char_elt_list.pickle', 'wb') as handler:
    pickle.dump(elt_list, handler)

# -*- coding: utf-8 -*-
"""
make a dictionary of the expert AND elite users who have less than 20 counts in charlotte that
maps their id to their OVERALL review count. Because maybe they have such small
review count numbers in charlotte because they travelled from elsewhere!? 

so in conclusion: out of the users who are experts AND elites and have a small (less than 20) 
number of reviews in charlotte, only 3 of them seem to have traveled from elsewhere,
namely RJFu0PXBTYF6C0ZaaUoYzg, RLmUj1QGMz2dLBcdayuBiw, wj8Ge6f_hoY4p5_6W7oNGA

@author: cchandler
"""

from BasicLocalExpert import expert_dictionary
from pymongo import MongoClient
from collections import defaultdict
from pprint import pprint

#get all experts
exp_dict1 = expert_dictionary(1)
lxp_list = []
for key, value in exp_dict1.iteritems():
    lxp_list.append(str(value))

#get total review counts in charlotte
rev = MongoClient().char.rev
rcount = defaultdict(int)
for review in rev.find():
    rcount[review['user_id']] += 1

    #get list of elites
user = MongoClient().char.user
elt_list = []
total = 0
for u in user.find():
    total += 1
    if len(u['elite']) != 0:
        pretty = str(u['user_id'])
        elt_list.append(pretty)

#get expert and elite list and make a dict of charlotte review counts
lxp_not_elt_list = list(set(lxp_list) - set(elt_list))
intersect_list = list(set(lxp_list) - set(lxp_not_elt_list))
intersect_dict = {}
for i in intersect_list:
    intersect_dict[i] = rcount[i]
pprint(intersect_dict)

#make user to review counts dictionary FOR ALL USERS
rev = MongoClient().yelpdb.rev
users = MongoClient().yelpdb.user
master_counts = defaultdict(int)
for r in rev.find():
    master_counts[r['user_id']] += 1

#get expert and elite users who have a small number of reviews in charlotte and find out if their total number is higher
travelers = {}
for user, count in intersect_dict.iteritems():
    if count < 20:
        travelers[user] = master_counts[user]

#dictionry that maps uid to charlotte rcount and overall rcount
charlotte_to_overall = {}
for t, c in travelers.iteritems():
    charlotte_to_overall[t] = (rcount[t], master_counts[t])

pprint(charlotte_to_overall)

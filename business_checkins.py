# -*- coding: utf-8 -*-
"""
author: Chelsea Chandler

date: 2015.06.23

description: 
This program loads JSON data from the Yelp! Challenge Dataset. It analyzes the 
checkins JSON file and creates a dictionary that relates business ID's to
their total number of checkins per week. It then creates a histogram that 
displays the number of checkins.
**IN PROGRESS**

"""

#import sklearn
import json
import itertools
import pylab as pl
import numpy as np

checkin_file = '/home/cchandler/Documents/yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_checkin.json'

data = []
with open(checkin_file) as data_file:
    for line in data_file:
        data.append(json.loads(line))

bus_ids = []
for b in data:
    bus_ids.append(b['business_id'])

checkin = []
for d in data:
    checkin.append(d['checkin_info'])

total = 0
totals = []
for i in checkin:
    for key, value in i.iteritems():
        total = total + value
    totals.append(total)
    total = 0

bus_checkins = dict(itertools.izip(bus_ids, totals))

X = np.arange(len(bus_checkins))
pl.bar(X, bus_checkins.values(), align='center', width=0.5)
pl.xticks(X, bus_checkins.keys())
ymax = max(bus_checkins()) + 1
pl.ylim(1, ymax)
pl.show()

\chapter{Cross-city results} \label{ch:cross-city}

A major goal of the project was to analyze the geographic diversity that the Yelp dataset offers. The dataset comes from ten different cities in  two continents and four countries. This diversity can also be seen through the topics that arose when running the HFT model. Figure \ref{museums} shows different words appearing with the different cities when running the model for the museums category in each city. Furthermore, running the model for all categories and assigning names to topics helped to visualize the different preferences across cities as shown in Figure \ref{topicsCities}.

\begin{figure}
\includegraphics[scale=.3]{Graphics/AMERICA.png}
\caption{Sample topics output for the Museums category.}
\label{museums}
\end{figure}


\begin{table}
        \centering
        \begin{tabular}
            {l|ccccc} \toprule City & \multicolumn{5}{c}{Topics} \\
            \midrule Charlotte & Beauty & Coffee & --- & Mexican & American \\
            Edinburgh & Bar & Culture & Food & Store & Travel \\
            Las Vegas & Grill & Beauty & Health & Food & Travel \\
            Madison & Beauty & Bar & Coffee & Food & Mexican\\
            Montreal & Brunch & --- & Food & Places & Travel \\
            Phoenix & Beauty & Italian & Travel & Fast Food & Health \\
            Pittsburgh & Italian & Asian & Beauty & Bakery & Bar\\
            Urbana & --- & --- & Coffee & Asian & Bar \\
            Waterloo & Coffee & Mexican & Bakery & Japanese & Chinese \\
            \bottomrule
        \end{tabular}
        \caption{Topics across cities.}
        \label{topicsCities}
\end{table}




Some users in the dataset have reviewed in multiple cities. Since Yelp does not provide an origin for each user we assigned home locations for cross-city users as the place where they have the most reviews. According to this definition, every city's review set is comprised of between 5\% and 10\%  ``tourist'' reviews.

With this in mind, we tested the recommender systems on the cross-city data. Testing on these particular users is an interesting direction to study because we imagine that many of the users who need recommendations are tourists. In our case, the impediment for further research in this topic is the amount of data available to us. Even if the dataset includes more than 350,000 users, user overlap between pairs of cities is too small to test in most cases. Table \ref{overlap} shows the number of users from one city visiting another. According to these results, the only city where we had sufficient information to test was Las Vegas.


\begin{table}
\centering
\caption{Number of cross-city users}
\label{overlap}
	\begin{tabular}{@{}llr@{}}
	\toprule
	\textbf{Origin} & \textbf{Visiting} & \textbf{Total} \\
            \midrule
            Las Vegas & Phoenix & 6155\\
Phoenix & Las Vegas & 4756 \\
Las Vegas & Charlotte & 827 \\
Las Vegas & Pittsburgh & 778\\
  Charlotte& Las Vegas&   762\\
     Montreal   & Las Vegas &   624 \\
   Las Vegas &  Montreal &   569 \\
     Las Vegas  & Madison  &  475 \\
     Charlotte   &Phoenix&    435\\
 Phoenix&  Charlotte&   394\\
 Phoenix &Pittsburgh &373\\
 Pittsburgh &Las Vegas &   366\\
     Madison &    Las Vegas&      258\\
 Phoenix &Madison   & 234\\
     Montreal&   Phoenix&    214\\
     Las Vegas  & Urbana-Champaign &   197\\
 Pittsburgh &Phoenix &184\\
 Charlotte   &Pittsburgh &182\\
 Phoenix& Montreal    & 153\\
     Las Vegas   &Edinburgh &   141\\
     Madison  & Phoenix& 137\\
       Pittsburgh      &Charlotte &  135\\
     Montreal   &  Pittsburgh     & 123\\
  Charlotte &Montreal  &  112\\
     Edinburgh&     Las Vegas&      103\\
       Pittsburgh     &Madison &   89\\
     Urbana-Champaign&   Madison&    86\\
       Pittsburgh   &  Montreal  &  82\\
     Urbana-Champaign  &   Las Vegas    &  80\\
     Montreal &  Charlotte&    72\\
  Charlotte & Madison    &68\\
     Montreal  & Madison &   66\\
     Edinburgh &  Phoenix &65\\
     Phoenix  & Edinburgh  &  59\\
     Madison    & Pittsburgh&      57\\
 Phoenix & Urbana-Champaign      &56\\
     Urbana-Champaign &  Phoenix &   54\\
     Madison  &  Charlotte &49\\
     Madison  & Urbana-Champaign &   49\\
     Waterloo &  Montreal  &  45\\
     Madison &  Montreal   & 40\\
            \bottomrule
        \end{tabular}
\end{table}




Probably due to the greater amount of information in a user's hometown, HFT predicts better locally, as shown in \Cref{sep_vs_one}. This table shows the error predictions for the above mentioned users when training a model in their hometown and when training a model in the city they travel to (Las Vegas).

Even when combining the two cities in one model, i.e. training with the reviews from both cities, predictions are on average better at home than in a city the user traveled to. This is shown in \Cref{sep_vs_one}. In this table, it is remarkable that in most cases training separate models is better than one large model. This is not surprising for a user's hometown since there is sufficient information there, but when visiting a new city, the user does not have much relevant information in their past. This could be because combining the reviews adds sparsity to the model or simply because the behavior of a user when visiting a different city changes. This remains an open question that could be studied in the future.

When predicting for tourists, we expected LBPA to gain in performance relative to other models. However, results in \Cref{LBPA_HFT} show that HFT still makes significantly better predictions.


\begin{table}[H] \centering \caption{Local vs Tourist Predictions}
\label{sep_vs_one}
        \begin{tabular}{@{}clllll@{}} \toprule
            ~ & ~ & \multicolumn{2}{c}{\ul{Train Separately}} & \multicolumn{2}{c}{\ul{Train Together}} \\
            ~ & Predicted for: & Hometown & Las Vegas & Hometown & Las Vegas \\ \midrule
            \multirow{4}{*}{\rotatebox{90}{Users From}} & Phoenix & 1.0003 & 1.0397 & \textcolor{blue}{0.9984}(-0.2 \%) & 1.0659 \\
            ~ & Charlotte & \textcolor{blue}{0.8408}(-6.2 \%) & 1.0319 & 0.8968 & 1.0698 \\
            ~ & Montreal & \textcolor{blue}{0.8607}(-9.4 \%) & 0.9966 & 0.9497 & 1.0513 \\
            ~ & Pittsburgh & \textcolor{blue}{0.8862}(-6.5 \%) & 0.9596 & 0.9478 & 0.9964 \\
            \bottomrule
        \end{tabular}
    \end{table}


\begin{table}[]
\centering \caption{LBPA vs. HFT}
\label{LBPA_HFT}
            \begin{tabular}{@{}clllll@{}} \toprule
            ~ & ~ & \multicolumn{2}{c}{\ul{LBPA}} & \multicolumn{2}{c}{\ul{HFT}} \\
            ~ & Predicted for: & Hometown & Las Vegas & Hometown & Las Vegas \\ \midrule
            \multirow{4}{*}{\rotatebox{90}{Users From}} & Phoenix & 1.4294 & 1.2265 & \textcolor{blue}{1.0003}(-29.8 \%) & 1.0397 \\
            ~ & Charlotte & 0.9951 & 1.2665 & \textcolor{blue}{0.8408}(-15.5 \%) & 1.0319 \\
            ~ & Montreal & 1.0351 & 1.1187 & \textcolor{blue}{0.8607}(-16.8 \%) & 0.9966 \\
            ~ & Pittsburgh & 1.0472 & 1.1593 & \textcolor{blue}{0.8862}(-15.4 \%) & 0.9596\\
            \bottomrule
        \end{tabular}
\end{table}



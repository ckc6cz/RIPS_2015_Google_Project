\chapter{Results} \label{ch:results}

The considered models were tested under four conditions:

\begin{enumerate}[a), noitemsep]
\item all users and all businesses
\item users with at least 3 reviews and businesses with at least 5
    reviews (Review Count Limited)
\item all users, and only businesses that listed ``Food'' or ``Restaurant''
    among their categories (Category Limited)
\item users with at least 3 reviews, and businesses with at least
    5 reviews that also listed ``Food'' or ``Restaurant'' among their categories (Category \& Review Count Limited)
\end{enumerate}

For each condition we calculate the root-mean-square error. This is an estimate
of the model accuracy based on cross-validation with 80\% of the data used for
training and 20\% for testing. We also calculate the root-mean-square error when
predictions are rounded and bounded to integer values between 1 and 5, i.e. the
nearest possible star rating. We ran our models on the entire dataset as well as for single cities. The results were generally better for models trained on single cities, so we show results for a single city. For simplicity, the tables and histograms below display results from a single city, Charlotte, NC.

\section{Basic Offsets Model}

In \Cref{fig:base-error-hists} we can see that the baseline model tends to
make predictions within one star of the true value. \Cref{tab:base-error}
shows that our prediction errors are smaller when dealing only with users and
businesses with at least three or five reviews respectively. This leads us to
believe that these users and businesses contain more signal which causes them
to be more predictable in the context of the model. Additionally, limiting by
category appears to have little effect on rating accuracy, so their histograms are not shown.

\begin{table}[H] \begin{center}
    \begin{tabular}{lcc} \toprule
        Condition & RMSE & Rounded \& Bounded RMSE \\ \midrule
        All Users \& Businesses & $1.234 \pm 0.002$ & $1.250 \pm 0.003$ \\
        Review Count Limited & $1.109 \pm 0.004$ & $1.136 \pm 0.004$ \\
        Category Limited & $1.209 \pm 0.001$ & $1.228 \pm 0.001$ \\
        Category \& Review Count Limited & $1.107 \pm 0.002$ & $1.139 \pm 0.003$ \\ \bottomrule
    \end{tabular}
    \caption{Root-Mean-Square Errors for the Basic Offsets Model}
    \label{tab:base-error}
\end{center} \end{table}

\begin{figure}[H] \begin{center}
    \begin{subfigure}{0.45\textwidth}
        \caption{All Users and Businesses}
        \includegraphics[height=1.5in]{Graphics/hist_basic_offsets_char00_BIG.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/hist_basic_offsets_char35_BIG.pdf}
    \end{subfigure} \\
    \caption{Histogram of Basic Offsets Model Errors}
    \label{fig:base-error-hists}
\end{center} \end{figure}

\section{Latent Factors Model}

The latent factors model performs significantly better when the data is
restricted to users and businesses with at least three or five reviews
respectively. \Cref{tab:latent-error} shows that the root-mean-square error is
far smaller when review count is limited. \Cref{fig:latent-error-hists} shows
that the ``tail'' of these predictions is narrower in the restricted model.

This is probably due to what is known as the \emph{cold start} problem. Matrix
factorization techniques are generally ill-equipped to make predictions when
data is very sparse \cite{Koren2009MatrixSystems}. That is, when a user or
business has few reviews, it is difficult for a latent factors model to make
predictions about that user or business.

% Currently the restricted latent factors model performs at about the same level
% as the restricted basic offsets model. However, our latent factors model's
% performance is on track with previous application of latent factors to Yelp
% data \cite{McAuley2013HiddenText}.

Currently the restricted latent factors model performs at about the same level
as the restricted basic offsets model. This was unexpected, as latent factors
models generally perform better than baseline models \cite{Koren2009MatrixSystems}. It is possible that if we
further restrict the data, we will see an improvement to the latent factors
model not matched by the baseline.

\begin{table}[H] \begin{center}
    \begin{tabular}{lcc} \toprule
        Condition & RMSE & Rounded \& Bounded RMSE \\ \midrule
        All Users \& Businesses & $1.562 \pm 0.005$ & $1.578 \pm 0.005$ \\
        Review Count Limited & $1.125 \pm 0.002$ & $1.154 \pm 0.003$ \\
        Category Limited & $1.566 \pm 0.005$ & $1.578 \pm 0.006$ \\
        Category \& Review Count Limited & $1.122 \pm 0.002$ & $1.153 \pm 0.002$ \\ \bottomrule
    \end{tabular}
    \caption{Root-Mean-Square Errors for the Latent Factors Model}
    \label{tab:latent-error}
\end{center} \end{table}

\begin{figure}[H] \begin{center}
    \begin{subfigure}{0.45\textwidth}
        \caption{All Users and Businesses}
        \includegraphics[height=1.5in]{Graphics/hist_comp_basic_offsets_latent_factors_char00_BIG.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/hist_comp_basic_offsets_latent_factors_char35_BIG.pdf}
    \end{subfigure} \\
    \caption{Histogram of Latent Factors Model Errors Compared to Baseline}
    \label{fig:latent-error-hists}
\end{center} \end{figure}

\section{Location-Based Preference Aware Model}

The Location-Based Preference Aware model has similar results to the basic offsets
model. \Cref{tab:lbpa-error} and \Cref{fig:lbpa-error-hists} show results for
models restricted and not restricted by review count. We do not restrict by
category because this model incorporates venue type into its rating predictions. This model does worse than the basic offsets model when all users and businesses are considered, but improves upon basic offsets when users and businesses are restricted by review count.

\begin{table}[h] \centering
    \begin{tabular}{ccc} \toprule
    Condition & RMSE & Rounded and Bounded RMSE \\ \midrule
    All Users \& Businesses & $1.289 \pm 0.003$ & $1.293 \pm 0.002$ \\
    Review Count Limited & $1.120 \pm 0.002$ & $1.154 \pm 0.001$ \\ \bottomrule
    \end{tabular}
    \caption{Root-Mean-Square Errors for the
        Location-Based Preference Aware Model}
    \label{tab:lbpa-error}
\end{table}

\begin{figure}[H] \begin{center}
    \begin{subfigure}{0.45\textwidth}
        \caption{All Users and Businesses}
        \includegraphics[height=1.5in]{Graphics/hist_comp_basic_offsets_lbpa_char00_BIG.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/hist_comp_basic_offsets_lbpa_char35_BIG.pdf}
    \end{subfigure} \\
    \caption{Histogram of LBPA Model Errors Compared to Baseline}
    \label{fig:lbpa-error-hists}
\end{center} \end{figure}

\section{Hidden Factors as Topics Model}

The HFT model performs the best out of all of the models we tested.
\Cref{tab:hfht-error} shows the model's root-mean-square error, which
improves significantly on the basic offsets model. \Cref{fig:hft-hists} shows that this
is due in part to the fact that HFT produces very few errors of 4 stars or
above. This may be due to its use of review text. It seems that text contains
additional signal which can reduce error in cold-start situations.

\begin{table}[H] \begin{center}
    \begin{tabular}{lc} \toprule
        Condition & RMSE \\ \midrule
        All Users \& Businesses & $0.9793$ \\
        Review Count Limited & $0.9305$ \\ \bottomrule
    \end{tabular}
    \caption{Root-Mean-Square Errors for the HFT Model}
    \label{tab:hfht-error}
\end{center} \end{table}

\begin{figure}[H] \begin{center}
    \begin{subfigure}{0.45\textwidth}
        \caption{All Users and Businesses}
        \includegraphics[height=1.5in]{Graphics/hist_comp_basic_offsets_hft_char00_BIG.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/hist_comp_basic_offsets_hft_char35_BIG.pdf}
    \end{subfigure} \\
    \caption{Histogram of HFT Errors Compared to Baseline}
    \label{fig:hft-hists}
\end{center} \end{figure}

\section{Category Offsets Model}

The category offsets model has better results than the baseline model, but is
still not as accurate as HFT. \Cref{tab:catoff-error} shows that the root-mean-square error
of the category offsets model is smaller than that of the basic offsets model
and larger than that of HFT. \Cref{fig:catoff-error-hists} shows that its
distribution of errors is right in between those of the basic offsets model
and of HFT.

\begin{table}[H] \begin{center}
    \begin{tabular}{lc} \toprule
        Condition & RMSE \\ \midrule
        All Users \& Businesses & $1.1749$ \\
        Review Count Limited & $1.0904$ \\ \bottomrule
    \end{tabular}
    \caption{Root-Mean-Square Errors for the Category Offsets Model}
    \label{tab:catoff-error}
\end{center} \end{table}

\begin{figure}[H] \begin{center}
    \begin{subfigure}{0.45\textwidth}
        \caption{All Users and Businesses}
        \includegraphics[height=1.5in]{Graphics/hist_comp_basic_offsets_category_offsets_hft_char00_BIG}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Review Count Limited}
            \includegraphics[height=1.5in]{Graphics/hist_comp_basic_offsets_category_offsets_hft_char35_BIG}
    \end{subfigure}
    \caption{Histogram of Category Offsets Model Errors Compared to Basic Offsets and HFT}
    \label{fig:catoff-error-hists}
\end{center} \end{figure}

\section{Models Compared}

%The root-mean-square error results for the basic offset, latent factors, and
%LBPA models under the All Users \& All Businesses condition and under the
%Review Count Limited condition are collected in \Cref{tab:models-compared}. We
%see that the basic offsets model slightly outperforms the LBPA model, which in
%turn significantly outperforms the latent factors model, when all users and
%all businesses are considered.
%
%As earlier mentioned, this comes as little suprise; the basic offsets model is
%conservative in its estimates, LBPA assigns big weights to users with greater
%numbers of reviews, and the latent factors model is known to have difficulty
%with the cold start problem. When we limit consideration so as only to include
%users with three or more reviews and businesses with five or more reviews, all
%models perform comparably; the basic offsets model improves slightly, the
%latent factors model warms up, and LBPA shows a slight improvement as
%well.
%
%It is when we go forward to include multiple locations that we expect LBPA to
%shine. Within the context that we have so far tested, where the reviews are
%limited to only those in Charlotte, the LBPA model uses location no more than
%either of the two baseline models do. As soon as we add our second city to our
%testing, we expect that the LBPA model will take advantage of geographic data
%to improve predictions.
%
%\begin{table}[h] \centering
%    \begin{tabular}{ccc} \toprule
%    Model & RMSE (All Users \& All Businesses) & RMSE (Review Count Limited) \\ \midrule
%    Basic Offsets & $1.234 \pm 0.002$ & $1.109 \pm 0.004$ \\
%    Latent Factors & $1.562 \pm 0.005$ & $1.125 \pm 0.002$ \\
%    LBPA & $1.289 \pm 0.003$ & $1.120 \pm 0.002$ \\ \bottomrule
%    \end{tabular}
%    \caption{Basic Offsets, Latent Factors, and LBPA Compared}
%    \label{tab:models-compared}
%\end{table}
%
%\Cref{fig:error-mag-plots} shows the error magnitude both of users and
%businesses with respect to the number of reviews they have. As we would expect,
%the average error is significantly smaller for users and businesses with more
%reviews.
%
%These graphs also illustrate the cold start problem. Specially, the latent
%factors model performs poorly when dealing with users and businesses with few
%reviews, but it recovers and performs almost equally to the offsets model when
%dealing with users and businesses with more reviews.
%
%\begin{figure}[h]
%    \begin{subfigure}{.5\textwidth}
%        \caption{Basic Offsets, Businesses}
%        \includegraphics[height=2in]{Graphics/error_trend_BO_bus}
%    \end{subfigure}
%    \begin{subfigure}{.4\textwidth}
%        \caption{Basic Offsets, Users}
%        \includegraphics[height=2in]{Graphics/error_trend_BO_user}
%    \end{subfigure} \\
%    \begin{subfigure}{.5\textwidth}
%        \caption{Latent Factors, Businesses}
%        \includegraphics[height=2in]{Graphics/error_trend_LF_bus}
%    \end{subfigure}
%    \begin{subfigure}{.4\textwidth}
%        \caption{Latent Factors, Users}
%        \includegraphics[height=2in]{Graphics/error_trend_LF_user}
%    \end{subfigure}
%    \caption{Error magnitude per number of reviews.}
%    \label{fig:error-mag-plots}
%\end{figure}

The root-mean-square error results for the basic offsets, latent factors, LBPA, HFT, and category offsets models under the All Users \& All Businesses condition and under the Review Count Limited condition are collected in \Cref{tab:models-compared}. We see that HFT outperforms all other models in both cases. In general, we are able to say that the models ranked in order of best to worst performance are: HFT, category offsets, basic offsets, LBPA, then latent factors.

\Cref{fig:error-mag-plots1} compares the basic offsets, category offsets, and HFT models.  It can be seen that HFT and category offsets have a greater number of small errors and a smaller number of large errors than basic offsets, but HFT still outperforms category offsets in the same way.

\Cref{fig:error-trend1} shows the error magnitude of users and
businesses with respect to the number of reviews they have. As we would expect,
the average error is significantly smaller for users and businesses with more
reviews in all models. These plots also show that HFT suffers less of a ``cold start'' problem than latent factors. We conclude from this that reviews with text contain more signal than reviews that only have star ratings.

\begin{table}[H] \centering
\begin{tabular}{lll}
    \toprule
    Model & All Users \& Businesses & Review Count Limited \\ \midrule
    % uncomment below as appropriate
    \errorbo \\
    \errorlf \\
    \errorlbpa \\
    \errorhft \\
    \errorbco \\
    \bottomrule
\end{tabular}
\caption{All Models Compared}
\label{tab:models-compared}
\end{table}

\begin{figure}[H]
\begin{center}
\includegraphics[width=.7\linewidth]{Graphics/hist_comp_basic_offsets_category_offsets_hft_char00}
\caption{Comparison of Errors from Basic Offsets, Category Offsets, and HFT}
\label{fig:error-mag-plots1}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
	\begin{subfigure}{.45\textwidth}
		\caption{LF Error Trend by Review Count}
		\includegraphics[height=1.7in]{Graphics/error_trend_LF_user.png}
	\end{subfigure}
	\begin{subfigure}{.45\textwidth}
		\caption{HFT Error Trend by Review Count}
		\includegraphics[height=1.7in]{Graphics/hft_error_trend_CHAR_user.png}
	\end{subfigure}
	\caption{Trends in Error by Number of Reviews}
	\label{fig:error-trend1}
\end{center}
\end{figure}

\newpage

\section{Text Analysis} \label{sec:textanalysis}
\subsection{HFT Topics}
The following is a chart of topics generated by HFT run on Phoenix, AZ. HFT is run with the \emph{stop words} (the most common words used in a language) removed and the remaining words stemmed (broken down to their root-words). This chart depicts the five highest probability topics with ten words included in each. We have assigned topic names to each column by hand. The variation in interpretability among topics can be seen most when comparing topics 2 and 4. \\

\begin{center}
    \begin{tabular}{ccccc}
    \toprule
    Topic 1 & Topic 2 & Topic 3 & Topic 4 & Topic 5 \\ \midrule
    sushi & stylist & theater & beer & nail \\ % & beer & sauc & burger & dr & thai  \\
    cupcak & store & trail & bartend & salon \\ %& bartend  & pizza & sandwich & dentist & donut \\
    buffet & walmart & airport & wing & pedicur \\ %& wing  & chicken & bacon & vet & boba \\
    bagel & dog & hotel & bar & repair \\ %& bar  & taco & chees & doctor & curri \\
    roll & pierc & hike & drink & manicur \\ %& drink  & rice & fri & dental & pad \\
    sashimi & tattoo & termin & danc & car \\ %& danc  & dish & yogurt & yoga & cigar \\
    japanes & hair & shuttl & pub & gel \\ %& pub  & salsa & bread & medic & panang \\
    indian & makeup & pool & game & instal \\ %& game  & mexican & bruschetta & studio & bosa \\
    naan & groceri & resort & tap & pedi \\ %& tap  & salad & toast & exam & smoothi \\
    pasti & flower & theatr & waitress & wax \\     \midrule %& waitress  & pork & breakfast & class & shoe  \\
    Food & Beauty, Pets, & Travel, & Bars & Nails \\
    & Shopping & Entertainment && \\
    \bottomrule
    \end{tabular}
\end{center}

\newpage

\subsection{HFT Topics vs Yelp Categories}
We ran the Hidden Factors as Topics model \cite{McAuley2013HiddenText} on
category-restricted subsets of the data. We ran it for all venues in a
particular category as well as for venues in that category in just one
city. Three categories we examined were `Bars', `Museums', and `Active Life'. The difference in output from each of these categories could be indicative of the different natures of their respective content. Additionally, these restricted models allowed for interesting city-specific observations.\\

\textbf{Bars}\\

HFT for bars generated more meaningful topics (to a human observer) when
trained on all data than on particular cities.

The topic ``dive, band, karaok, hookah, piano, jukebox, dart,
tiki, pub, edinburgh,'' included within its top words several Yelp business categories, such as Dive Bars, Piano Bars, and Hookah. All other topics appear to represent broader classifications, such as high-end food and drinks (e.g. wine, lobster) and cheaper food and drinks (e.g. beer, pizza).\\

\textbf{Museums}\\

In contrast to the models trained on bars, HFT for museums generated more
meaningful topics when trained on single-city data than on all data. This may
be due to the different nature of the two types of venues. An bar in Charlotte
is likely to share features with a bar in Edinburgh. But the NASCAR Hall of
Fame in Charlotte (``hall, nascar, race, simul, sport, raptor, car, eagl,
moonshin, trail'') has little in common with the Mob Museum in Las Vegas
(``mob, crime, 51, weapon, mobster, mafia, interact, courtroom, courthous,
read''). The Yelp API has no subcategories for museums, so it is interesting
to see how HFT organizes these establishments.

Interestingly, even though topics are meaningful, the single-city museum
models for Charlotte and Edinburgh both performed worse than the baseline in
rating predictions. The model for all museums performed better than baseline,
as did the model for museums in Las Vegas only.\\

\textbf{Active Life}\\

For a small number of topics (e.g. 5), HFT for Active Life generated more meaningful
topics when trained on single-city data. Because Active Life
is a fairly diverse category, similarly meaningful topics were generated when
the number of topics was large (e.g. 20). In all cases, some of the topics
generated corresponded to Yelp categories, while other topics were more
general.

Some local patterns emerged for specific cities. For example, there was a
``gun'' topic generated for active life in Las Vegas, and separate ``gym'' and ``yoga'' topics for Charlotte (as opposed to a combined topic for both).

\subsection{Word Clouds Generated from Topics Alone}
    \begin{center}
        \includegraphics{Graphics/HFT-topics-char.png}
    \end{center}
    This \emph{word cloud} was generated from running the HFT model on Charlotte, NC. A \emph{word cloud} is an image composed of words used in a particular subject in which the most frequently used or important words are larger than the less important words. In this case, words are weighted by probability of showing up in the topic. While there are some straightforward topics such as ``menu, sauce, chicken...'' (general food and restaurants) and ``cupcake, yogurt...''(frozen yogurt and desserts), there are also less interpretable topics like all three on the left-hand side. It is clear both from this word cloud and from the raw output above that topics range from highly interpretable to barely interpretable at all. An interesting takeaway is that rating accuracy is in no way correlated with the clarity of topics produced.
\newpage
    \begin{center}
        \includegraphics[height=3in]{Graphics/vegas_museum_topics.png}
        \includegraphics[height=3in]{Graphics/bar_topics.png}
    \end{center}
    Some interesting topics emerge when we restrict data to particular business categories. The word cloud on the left is generated from HFT output run on the Las Vegas data restricted only to the Museums category. This shows the top 50 words associated with a particular topic. The word cloud on the right is generated from HFT output run on the entire dataset restructured to Bars with the top 50 words of one topic shown. While these topics are very interpretable, there are many HFT topics that are jumbled and seemingly randomly placed together, as shown in the above section of HFT topic output.
\subsection{Word Clouds Generated from Review Text Alone}
    \begin{center}
        \includegraphics[height=3in]{Graphics/starbucks_topics_2.png}
    \end{center}
    This word cloud was generated from the entire text corpus for Starbucks reviews. The words are not stemmed, nor are topics pulled out. It is interesting to see how the words deviate when analyzing merely by word count rather than by topics.
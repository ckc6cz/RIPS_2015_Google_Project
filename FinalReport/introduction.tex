\chapter{Introduction}\label{Ch:Introduction}

Google, one of the most widely recognized companies in the world and well-known 
for its search and Gmail services, produces a variety of products. A
top priority for Google is the constant improvement of user experience.
Our project is concerned with personalized recommendations, which can improve a user's
time spent searching online by sorting through and prioritizing the vast amount of information available on the web.

\textbf{The proposed problem.}   Personalized recommendations, such as those
offered by YouTube, Google Play Music, and Google News, rely on the accurate
prediction of consumer preferences. \emph{Recommender systems} often employ a
technique called \emph{collaborative filtering}, in which a user's interests are
predicted based on the behavior of other users \cite{Koren2009MatrixSystems}.
To better understand these systems, the team implemented and evaluated existing models that take into account the unique features of the \emph{Yelp Challenge Dataset}. These unique features include review text and geographical diversity which  provided interesting insights when applied to collaborative filtering.

\textbf{Our team's approach.} Yelp's current recommendation algorithm takes into account only the quality, reliability, and web activity of a given user \cite{YelpOfficialBlog2013}. Keeping this rudimentary system in mind, the team built upon existing recommender systems to gain insight into how the unique qualities of the \emph{Yelp Challenge Dataset} can be used to improve such systems. We are now able to offer suggestions for a superior model that is able to handle a vast and diverse dataset efficiently and accurately.

First, we built two baseline models to serve as comparisons for more sophisticated recommender systems. Once the baseline models were implemented and evaluated, we built a model that focuses on location-based subsets of the data. After the model was successfully implemented, we evaluated its strengths and weaknesses based on the accuracy of its predicted ratings. We then implemented and evaluated a review-text-based model that takes into account both the entire set of reviews and the ratings that correspond to them.

\textbf{Overview of the report.} \Cref{ch:datasummary} details the components of the Yelp Challenge Dataset.

\Cref{ch:baseline} provides a summary of our baseline recommender models, the basic offsets model and the latent factors model. This section explains how the two models were implemented and what purpose they serve in our project.

\Cref{ch:lbpa} outlines the \emph{Location-Based Preference-Aware (LBPA)} model. It describes the importance of the model and its major
components. The implementation of the model is detailed in the report because of non-trivial deviations from the original model described in the paper \emph{Location-based and Preference-Aware Recommendation Using Sparse Geo-Spacial Networking Data} \cite{Bao2012}.

\Cref{ch:hft} outlines the \emph{Hidden Factors as Topics (HFT)} model \cite{McAuley2013HiddenText} . This section includes the major components and algorithm of the model.

\Cref{ch:results} lists results from the models and includes tables and
graphs showing the prediction errors of each model.

\Cref{ch:cross-city} includes an analysis of the diverse attributes of the Yelp Challenge Dataset and results involving geographic subsets of the data.

\Cref{ch:conclusion} is the conclusion of the report. This chapter summarizes
the results of the models, as well as the team's final interpretations and analysis. It discusses the ``big picture questions'' that our group set out to answer and the major takeaways that we can deliver to Google. Additionally, it describes the issues and difficulties the team encountered and in what respects the problem we wished to solve was completed. We will also offer additional directions for further research.

Appendix A contains a subset of the category tree used in the LBPA model.

Appendix B contains sample output from a run of HFT on a chain restaurant.

\Cref{ch:glossary} contains a glossary of terms used in the paper.

The Bibliography contains references to the literature we have used for our work thus far.

\endinput
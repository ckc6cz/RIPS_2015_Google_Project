\chapter{Hidden Factors as Topics} \label{ch:hft}

The \emph{Hidden Factors as Topics (HFT)} model predicts ratings by discovering implicit user tastes and business attributes. For example, it might predict a user's likely enjoyment of Taco Bell by identifying the business as a Mexican fast-food restaurant, as well as finding the user's level of interest in this type of business. This model makes use of both the past ratings and written reviews of the user and the business. This model combines the latent factors model with a topic modeling algorithm, Latent Dirichlet Allocation (LDA). This model has outperformed others because of its ability to uncover hidden dimensions in user ratings and reviews \cite{McAuley2013HiddenText}. 

The HFT model claims to accomplish four things: explain the variation in ratings and reviews, improve on models that take ratings or reviews into consideration independently, discover genres, and scale to large datasets. 

Since the latent factors model is previously explained in  \Cref{ch:baseline}, here we will only cover the LDA model. LDA uncovers hidden dimensions in review text. Each document $d$ is associated with a K-dimensional topic distribution $\theta_d$ which encodes the fraction of words in d that discuss each of the K topics. Each topic $k$ is associated with a word distribution $\phi_k$ which encodes the probability that a word will be used in that particular topic. The LDA model fits the word distributions for each topic, topic distributions for each document, and topic assignments for each word, $z_{d,j}$. The likelihood of a text corpus $\tau$ given $\phi$ and $z$ is the likelihood of seeing the topics multiplied by the likelihood of seeing the particular words for the topic over each document in the corpus and all words in that document. 

\begin{equation}
	p(\tau | \theta, \phi, z) = \prod_{d \in \tau} \prod_{j=1}^{N_d} \theta_{z_{d,j}} 		\phi_{z_{d,j},w_{d,j}}
\end{equation}

The HFT model correlates topics with the latent factors of businesses and users, $\gamma_i$ and $\gamma_u$. Since review text tends to describe the properties of businesses rather than the properties of the users themselves, the model considers the text of all reviews of a business $i$ as a document $d_i$ (rather than the set of all reviews made by a user $u$). The model links business attributes with user preferences in hopes that a high $\gamma_{i,k}$, a property being present, will correspond to a high $\theta_{i,k}$, a particular topic being discussed. To link the $\gamma_1$ and the $\theta_i$, we have the transformation

\begin{equation}
	\theta_{i,k} = \dfrac{\textrm{exp}(\kappa \gamma_{i,k})}{\sum_{\kappa'} \textrm{exp}(\kappa \gamma_{i, \kappa'})}
\end{equation}

where $\kappa$ controls the `peakiness' of the transformation. The final model is as follows:

\begin{equation}
f( \tau | \Theta, \Phi, \kappa, z) = \sum_{r_{u,i} \in \tau} (rec(u,i) - 		r_{u,i})^2 - \mu l(\tau | \theta, \phi, z)
\end{equation}

which is the sum over the text corpus of the error of predicted ratings minus the likelihood of the review corpus. In order for the model to predict accurate ratings, the parameters $\Theta = \{\alpha, \beta_u, \beta_i, \gamma_u, \gamma_i \}$ and $\Phi = \{\theta, \phi\}$ need to be optimized. Given the review corpus $\tau$ the objective is to find 

\begin{equation}
	\textrm{argmin}_{\Theta, \Phi, \kappa, z} f(\tau | \Theta, \Phi, \kappa, z).
\end{equation}

The parameters are fit by an alternation between gradient descent and Gibbs sampling. The procedure is composed of two steps, outlined as follows:

\begin{equation}
\textrm{update } \Theta ^{(t)}, \Phi ^{(t)}, \kappa ^{(t)} = \textrm{argmin}_{\Theta, \Phi, \kappa, z^{(t-1)}}
\end{equation}

\begin{equation}
\textrm{sample }  z_{d,j}^{(t)} \textrm{ with probability } p(z_{d,j}^{(t)} = k) = \phi_{k,w_{d,j}}^{(t)}
\end{equation}

In the first step, $z$ is fixed and the remaining terms, $\Theta, \Phi,$ and $\kappa$ are fit by gradient descent. The second step iterates through documents $d$ and all word positions $j$ updating their topic assignments. Each word is assigned to a topic randomly by its probability of appearing in the particular topic. To ensure that the word distribution for topic $k$ is a stochastic vector (meaning that its entries add up to 1), we define 

\begin{equation}
\phi_{k,w} = \dfrac{\textrm{exp}(\upsilon_{k,w})}{\sum_{w'} \textrm{exp}(\upsilon_{k,w'})}
\end{equation}

where the new variable $\upsilon$ is introduced, acting as a parameter for the multinomial $\phi_{k} \in \Delta ^D$ with $D$ being the number of distinct words in the corpus. This procedure is analogous to LDA's topic assignments. The only difference present in the HFT fitting is that topic proportions $\theta$ are not sampled from a Dirichlet distribution, but instead are based on the value of $\Theta ^t$ from the update step. The update and sample steps are repeated until the change in $\Theta$ and $\Phi$ between iterations is sufficiently small.

The C++ code to implement this model is publicly available on the author's
website. For our initial evaluation of the model, we ran it in its original form. We then incorporated various tweaks in order to gain insights as
to how the model performs in different circumstances, as described in
\Cref{sec:textanalysis} and \Cref{ch:chainrestaurants}.

\section{Alternative Offsets Models}

We tested several offset-based models to see how much of HFT's performance
could be captured without the use of review text. Because many HFT topics
appear to correspond to Yelp's business categories, we created a
\emph{category offsets model} that replaced the average $\mu$ of
\Cref{sec:basicoffsets} with $\mu_c$, the average rating for businesses in
category $c$.

Typically, businesses are associated with multiple categories. We tested several ways of incorporating these multiple offsets. Let $C$ be the set of categories associated with business $b$, and let $D(c_i)$ be the depth of $c_i$ in the category hierarchy. We tried replacing $\mu$ by
 \begin{itemize}
 	\item $\overline{\mu_c}$  for an entire category $c$
	\item $\mu_c$ for the highest-level category associated with $b$
 	\item $\mu_c$ for the deepest category associated with $b$
 	\item Linear weight by depth: $\sum_{c_i \in C} D(c_i) \mu_{c_i}$
 	\item Inverse linear weight by depth: $\sum_{c_i \in C} \del{4 - D(c_i)} \mu_{c_i}$
 	\item Exponential weight by depth: $\sum_{c_i \in C} \exp\del{D(c_i) \mu_{c_i} }$
 	\item Inverse exponential weight by depth: $\sum_{c_i \in C} \exp\del{{4 - D(c_i)} \mu_{c_i}}$
\end{itemize}
Of these, the average $\overline{\mu_c}$ performed the best.

We also implemented offset models for a business's price range and for its city. However, since neither of these performed better than the baseline, they are not included in our results.
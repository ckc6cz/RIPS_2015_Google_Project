# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 14:59:00 2015

@author: cchandler
"""

import numpy as np
from sklearn.cross_validation import KFold
from collections import defaultdict
from pymongo import MongoClient

# connect to relevant collections
rev = MongoClient().yelpdb.rev
bus = MongoClient().yelpdb.bus


def make_location_rev_dictionary():
    """ make {city: {(uid, bid): stars}} """

    city_rev = defaultdict(dict)
    for r in rev.find():
        bid = r['business_id']
        uid = r['user_id']
        star = r['stars']
        city = bus.find_one({'business_id': bid})['true_city']
        city_rev[city].update({(uid, bid): star})

    return city_rev


def basic_offset_prediction(data):
    """
    args:
        data (dict): {cat: {(uid, bid): rating}}

    returns:
        dict: {(uid, bid): rating} filled by basic offset method
    """

    # prepare to set-up training and test sets
    pred = defaultdict(list)

    for city, city_data in data.iteritems():
        uid_bid_keys = np.array(city_data.keys())
        kf = KFold(len(city_data), n_folds=5, shuffle=True)
        for train_index, test_index in kf:
            train, test = uid_bid_keys[train_index], uid_bid_keys[test_index]
            uid_ratings, bid_ratings = defaultdict(list), defaultdict(list)

            # prepare for offset calculations
            for (uid, bid) in train:
                uid_ratings[uid].append(city_data[(uid, bid)])
                bid_ratings[bid].append(city_data[(uid, bid)])

            # calculate offsets
            dolla_mu = np.mean([city_data[(uid, bid)] for (uid, bid) in train])
            uid_offset, bid_offset = defaultdict(int), defaultdict(int)
            uid_offset.update({
                uid: np.mean(uid_ratings[uid]) - dolla_mu
                for uid in uid_ratings
            })
            bid_offset.update({
                bid: np.mean(bid_ratings[bid]) - dolla_mu
                for bid in bid_ratings
            })
            # make predictions!
            for (uid, bid) in test:
                pred[(uid, bid)].append(
                    dolla_mu + uid_offset[uid] + bid_offset[bid])

    for (uid, bid), predictions in pred.iteritems():
        pred[(uid, bid)] = np.mean(predictions)

    return pred


if __name__ == '__main__':
    print 'formatting the dolla dolla bills y\'all...'
    data = make_location_rev_dictionary()

    print 'generating predictions...'
    pred = basic_offset_prediction(data)

    print 'comparing for results...'
    flatdata = {
        (uid, bid): rating
        for cat in data.values() for (uid, bid), rating in cat.items()
    }
    delta = {
        (uid, bid): pred[(uid, bid)] - flatdata[(uid, bid)]
        for (uid, bid) in pred
    }

    rmse = np.sqrt(np.mean(np.array(delta.values()) ** 2))
    print 'rmse:', rmse

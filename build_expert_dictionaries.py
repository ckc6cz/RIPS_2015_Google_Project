# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 11:45:43 2015

@author: tlichter
"""
from BasicLocalExpert import expert_dictionary

# THIS IS THE SLOW PART
#get local expert dictionaries
#number 1 has the users who reviewed the most for that category
#number 2 has the users who reviewed the second most for that category (etc.)
exp_dict1 = expert_dictionary(1)
#exp_dict2 = expert_dictionary(2)
#exp_dict3 = expert_dictionary(3)
##example of how to find expert of given category
#fash_expert = exp_dict1['Fashion']
##how many categories are the experts an expert in?
#how_expert = {}
#for expert in lxp_list:
#    count = lxp_list.count(expert)
#    how_expert[expert] = count

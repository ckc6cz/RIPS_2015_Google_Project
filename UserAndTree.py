"""
date: 2015.07.15

author: daniel and tamar
"""


def insert_leaf(tree, cat, uid, ucp, ctp):
    subtree = tree
    if cat in ctp:
        chain = [ctp[cat]]
        while chain[-1] in ctp:
            chain.append(ctp[chain[-1]])
        while chain:
            parent = chain.pop()
            pval = ucp[uid][parent]
            subtree = subtree[(parent, pval)]
    subtree[(cat, ucp[uid][cat])] = {}
    return tree


def make_all_user_pref_trees():
    from UserProfile import create_user_profile
    from HierarchyCreation import make_category_tree
    from user_pref_profile import compute_user_preference
    from tree_helpers import get_cat_depth, tree_crawler, make_child_parent_dict
    import pandas as pd
    user_prof = create_user_profile()
    cat_tree = make_category_tree()
    cat_set = tree_crawler(cat_tree)
    cat_depth = {cat: get_cat_depth(cat, cat_tree) for cat in cat_set}
    cat_to_parent = make_child_parent_dict(cat_tree)
    user_cat_preference = compute_user_preference()
    # make master
    master = {uid: {} for uid in user_prof}

    for uid in master:
        user_cats = user_prof[uid].index
        user_cats_depth = pd.Series([cat_depth[cat] for cat in user_cats],
                                    index=user_cats)
        user_cats_depth.sort()
        for cat, depth in user_cats_depth.iteritems():
            master[uid] = insert_leaf(master[uid], cat, uid,
                                      user_cat_preference, cat_to_parent)

    return master

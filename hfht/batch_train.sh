# use this code to run the hft model (accessed via ./train) on a set of files
# matching a pattern (e.g. here data_*.gz) across a grid of lambda and ntopics
# param combinations

for filename in data_*.gz
do
    for doublefactor in {0..3}
    do
        for lambda in {1..10}
        do
            lambda=$(($lambda / 2.0))
            name=${filename##*/}
            outname=${name%.gz}
            ntopics=$((5*$((2**doublefactor))))
            ./train $filename 0 $lambda $ntopics "model.out" "predictions.out" > "hfht_${outname}_L${lambda}_T${ntopics}.txt"
        done
    done
done
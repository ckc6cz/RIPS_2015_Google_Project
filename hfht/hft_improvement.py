"""
running this file from the command line with the inputs being the files
to search across, the printed result will be a dictionary for each corpus
in the test sets, with a simple summary of the best parameter combinations
and their improvements.

author: daniel

e.g.
>> python hft_improvement.py *.txt

 'charonly.gz': {'improvement absolute': -0.1934,
                 'improvement relative': -0.1492,
                 'params': {'lambda': 5.0, 'ntopics': 40}},
 'charpitt.gz': {'improvement absolute': -0.2156,
                 'improvement relative': -0.1664,
                 'params': {'lambda': 5.0, 'ntopics': 40}},
 'pittonly.gz': {'improvement absolute': -0.2475,
                 'improvement relative': -0.196,
                 'params': {'lambda': 5.0, 'ntopics': 40}}}
"""

import fileinput
from numpy import inf, nan
from pprint import pprint

# initialize so that first final-result will replace this
template = {
    'params': {'lambda': nan,
               'ntopics': nan},
    'improvement absolute': inf,
    'improvement relative': inf
}

best = {}
for line in fileinput.input():
    if line.startswith('corpus = '):
        corpus = line.split()[-1]
        best[corpus] = template.copy()
    elif line.startswith('lambda = '):
        param_lambda = float(line.split()[-1])
    elif line.startswith('K = '):
        param_ntopics = int(line.split()[-1])
    elif line.startswith('Error w/ offset and bias'):
        error_offsets = float(line.split()[7].split('/')[2])
    elif line.startswith('Error (train/valid/test)'):
        train, valid, test = [float(x) for x in line.split()[3].split('/')]
        improvement = test - error_offsets
        if improvement < best[corpus]['improvement absolute']:
            best[corpus][
                'params'
            ] = {'lambda': param_lambda,
                 'ntopics': param_ntopics}
            best[corpus]['improvement absolute'] = improvement
            best[corpus]['improvement relative'] = improvement / error_offsets

# round entries for easier reading
for corpus in best:
    best[corpus]['improvement absolute'] = round(
        best[corpus]['improvement absolute'], 4)
    best[corpus]['improvement relative'] = round(
        best[corpus]['improvement relative'], 4)

pprint(best)

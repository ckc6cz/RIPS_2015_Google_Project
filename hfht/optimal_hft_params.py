"""
running this file from the command line with the inputs being the files
to search across, the printed result will be a dictionary for each corpus
in the test sets, with a detailed summary of the best parameter combinations
and their results.

author: daniel

e.g.
>> python optimal_hft_params.py *.txt
{'charonly.gz': {'error_offsets_w_bias': 1.295858,
                 'param_lambda': 5.0,
                 'param_ntopics': 40,
                 'test': 1.26992,
                 'train': 0.823249,
                 'valid': 1.24883},
 'charpitt.gz': {'error_offsets_w_bias': 1.296189,
                 'param_lambda': 5.0,
                 'param_ntopics': 40,
                 'test': 1.259067,
                 'train': 0.813632,
                 'valid': 1.231572},
 'pittonly.gz': {'error_offsets_w_bias': 1.262878,
                 'param_lambda': 5.0,
                 'param_ntopics': 40,
                 'test': 1.229236,
                 'train': 0.8567,
                 'valid': 1.234433}}
"""

import fileinput
from numpy import inf, nan
from pprint import pprint

# initialize so that first final-result will replace this
template = {
    'error_offsets_w_bias': nan,
    'param_lambda': nan,
    'param_ntopics': nan,
    'train': nan,
    'valid': nan,
    'test': inf
}

best = {}
for line in fileinput.input():
    if line.startswith('corpus = '):
        corpus = line.split()[-1]
        best[corpus] = template.copy()
    elif line.startswith('lambda = '):
        param_lambda = float(line.split()[-1])
    elif line.startswith('K = '):
        param_ntopics = int(line.split()[-1])
    elif line.startswith('Error w/ offset and bias'):
        error_offsets = float(line.split()[7].split('/')[2])
    elif line.startswith('Error (train/valid/test)'):
        train, valid, test = [float(x) for x in line.split()[3].split('/')]
        if test < best[corpus]['test']:
            best[corpus]['error_offsets_w_bias'] = error_offsets
            best[corpus]['param_lambda'] = param_lambda
            best[corpus]['param_ntopics'] = param_ntopics
            best[corpus]['train'] = train
            best[corpus]['valid'] = valid
            best[corpus]['test'] = test

pprint(best)

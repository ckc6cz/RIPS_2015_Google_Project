""" call me from the command line on char_predictions.out """

import fileinput
import numpy as np
from make_error_histogram import error_report_and_hist
from collections import Counter

pred, data = {}, {}
for line in fileinput.input():
    try:
        uid, bid, test, guess = line.split()
        test, guess = float(test), float(guess)
        pred[(uid, bid)] = guess
        data[(uid, bid)] = test
    except ValueError:
        # empty line case
        continue

keys = pred.keys()
test = np.array([data[key] for key in keys])
guess = np.array([pred[key] for key in keys])
error_report_and_hist(test, guess,
                      fname='char_hfht_all00.pdf',
                      normed=True,
                      ymax=0.75)

uid_count = Counter(uid for (uid, _) in data)
bid_count = Counter(bid for (_, bid) in data)
test = np.array([data[(uid, bid)] for (uid, bid) in keys
                 if uid_count[uid] >= 3 and bid_count[bid] >= 5])
guess = np.array([pred[(uid, bid)] for (uid, bid) in keys
                  if uid_count[uid] >= 3 and bid_count[bid] >= 5])
error_report_and_hist(test, guess,
                      fname='char_hfht_all35.pdf',
                      normed=True,
                      ymax=0.75)

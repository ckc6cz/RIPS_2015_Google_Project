"""
use this file to format the Yelp data for the hft code

author: daniel

example usage:
in python:
    write_for_hft('out')
in bash:
    gzip out
    ./train FILENAME.gz 0 LAMBDA NTOPICS "model.out" "predictions.out" > RESULTSFILENAME.txt
"""

from string import punctuation
from nltk.stem import SnowballStemmer
from sklearn.feature_extraction.text import CountVectorizer
from pymongo import MongoClient

# include the cities and their review collection
rev = MongoClient().yelpdb.rev
bus = MongoClient().yelpdb.bus
bid_city = {b['business_id']: b['true_city'] for b in bus.find()}

stemmer = SnowballStemmer('english')
sws = set(CountVectorizer(min_df=1, stop_words='english').get_stop_words())
punc = set(punctuation)


def removePunc(wordlist):
    out = []
    for word in wordlist:
        out.append(''.join(ch for ch in word if ch not in punc))
    return out


def stemify(wordlist):
    wordlist = removePunc(wordlist)
    for word in wordlist:
        if word in sws:
            wordlist.remove(word)
    stems = map(stemmer.stem, wordlist)
    return stems


def write_for_hft(fname):
    """ run to create a file formatted for hft """
    with open(fname, 'w') as f:
        for r in rev.find():
            bid = r['business_id']
            if bid_city[bid] not in cities:
                continue
            uid = r['user_id']
            stars = str(r['stars'])
            time = '0'
            review = r['text'].encode('ascii', 'ignore').lower().split()
            review = ' '.join(stemify(review))
            review_len = str(len(review.split()))
            line = ' '.join([uid, bid, stars, time, review_len, review,
                             '\n'])
            f.write(line)

if __name__ == '__main__':
    cities = {'Charlotte', 'Edinburgh', 'Las Vegas', 'Madison', 'Waterloo', 'Phoenix', 'Pittsburgh', 'Urbana-Champaign'}
    write_for_hft('data_english.out')

# make a heatmap of review counts for charlotte data
#
# following instructions found at:
# http://www.r-bloggers.com/heatmap-of-toronto-traffic-signals-using-rgooglemaps/
#
# author: daniel
setwd('~/Documents/RIPS_2015_Google_Project/hfht/topicdist/')

library(MASS)
library(RgoogleMaps)
library(RColorBrewer)
source('colorRampPaletteAlpha.R')

# read in the data
data <- read.csv("~/Documents/RIPS_2015_Google_Project/hfht/topicdist/char_latlon_topic1_scaled.csv", header=FALSE, comment.char="#")
colnames(data) = c('lat', 'lon', 'weight')

# duplicate rows in accordance with thier count value
data <- data[rep(seq_len(nrow(data)), data[,3]), ][,-3]
data <- cbind(jitter(data[,1]), jitter(data[,2]))
# data <- cbind(data[,1],data[,2],data[,3])

# Reproduce William's original map
par(bg='black')
plot(data, col="white", pch=20)

# Create heatmap with kde2d and overplot
k <- kde2d(data[,1], data[,2], bandwidth.nrd(data), n=500)
# Intensity from green to red
cols <- rev(colorRampPalette(brewer.pal(8, 'RdYlGn'))(100))
par(bg='white')
image(k, col=cols, xaxt='n', yaxt='n')
points(data, cex=0.1, pch=20)


# Mapping via RgoogleMaps
# Find map center and get map
center <- apply(data, 2, mean)
map <- GetMap(center=center, zoom=11)
# Translate original data
coords <- LatLon2XY.centered(map, data[,1], data[,2], 11)
coords <- data.frame(coords)

# Rerun heatmap
k2 <- kde2d(coords$newX, coords$newY, n=500)

# Create exponential transparency vector and add
alpha <- seq.int(0.5, 0.95, length.out=100)
alpha <- exp(alpha^6-1)
cols2 <- addalpha(cols, alpha)

# Plot
PlotOnStaticMap(map)
image(k2, col=cols2, add=T)
points(coords$newX, coords$newY, pch=20, cex=0.3)






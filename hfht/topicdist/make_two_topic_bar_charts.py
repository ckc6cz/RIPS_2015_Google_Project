# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 19:56:46 2015

@author: tlichter
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 16:44:49 2015

@author: tlichter
"""

def latexify(fig_width=None, fig_height=None, columns=1):
        """Set up matplotlib's RC params for LaTeX plotting.
        Call this before plotting a figure.

        Parameters
        ----------
        fig_width : float, optional, inches
        fig_height : float,  optional, inches
        columns : {1, 2}
        """

        # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

        # Width and max height in inches for IEEE journals taken from
        # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

        import matplotlib
        from math import sqrt

        assert (columns in [1, 2])

        if fig_width is None:
            fig_width = 3.39 if columns == 1 else 6.9  # width in inches

        if fig_height is None:
            golden_mean = (sqrt(5) - 1.0) / 2.0  # Aesthetic ratio
            fig_height = fig_width * golden_mean  # height in inches

        MAX_HEIGHT_INCHES = 18.0
        if fig_height > MAX_HEIGHT_INCHES:
            print("WARNING: fig_height too large:" + fig_height +
                  "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
            fig_height = MAX_HEIGHT_INCHES

        params = {
            'backend': 'ps',
            'text.latex.preamble': ['\usepackage{gensymb}'],
            'axes.labelsize': 8,  # fontsize for x and y labels (was 10)
            'axes.titlesize': 8,
            'font.size': 8,  # was 10
            'legend.fontsize': 6,  # was 10
            'xtick.labelsize': 5,
            'ytick.labelsize': 8,
            'text.usetex': True,
            'figure.figsize': [fig_width, fig_height],
            'font.family': 'serif'
        }

        matplotlib.rcParams.update(params)

def make_double_bar_chart(weights1, weights2, topics):
    """
    Creates a bar chart that shows the distribution of topic weights
    for one particular business
    """
    
    # replace '&' characters with 'and' so they can be read by LaTeX
#    busname = busname.replace('&','and')    
    
    import matplotlib.pyplot as plt
    import numpy as np
    
    plt.clf()
    latexify()
    index = np.arange(len(topics))
#    plt.bar(index, (weights1, weights2), color=('#66c2a5','#e78ac3'))
    
    fig, ax = plt.subplots()
    rects1 = ax.bar(index, weights1, .4, color='#66c2a5')
    rects2 = ax.bar(index+.4, weights2, .4, color='#e78ac3')
    ax.legend((rects1[0],rects2[0]),('Sidelines Bar and Grill', 'Pearl Sushi Lounge and Bomber Bar'))    
    
    plt.ylabel('Normalized topic weight')
    plt.title('Topic Distributions for Two Bars')
    plt.xticks(index+.5, topics, rotation = 15)
#    flatbusname = '_'.join(busname.split())
    # edit the following line to fit with category and color
    filename = 'graphics/bars_L5_T5_double.pdf'
    plt.tight_layout()
    plt.savefig(filename, dpi=300)
    

def make_two_topic_bar_charts():
    """
    makes a double bar chart from the two specified businesses
    """
    
    # Hard code the topic names you want
    topics = ('BBQ, Beer \& Wings', 'Las Vegas', 'Wine, Steak \& Seafood',
              'Irish Pubs', 'Clubs \& Specialty Bars')           
    
#    bid1 = '3BNomgNXuzR-Yfc2LbY6sQ'
#    bid2 = 'RmA5zN-Bs89lsTuY6v8W3Q'
    dist1 = (0.633649, 0.025825, 0.047221, 0.125371, 0.167934)
    dist2 = (0.049402, 0.158976, 0.378293, 0.301721, 0.111608)
    
    make_double_bar_chart(dist1, dist2, topics)


## to test    
#if __name__ == '__main__':
#    make_topic_bar_charts('topics_bars_L5_T5.txt')
#    
#3BNomgNXuzR-Yfc2LbY6sQ
#
#RmA5zN-Bs89lsTuY6v8W3Q
#8TSSHGynPWzilsnTKMMHyw

    
    
    
    
    
    
    
    
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 16:44:49 2015

@author: tlichter
"""

def latexify(fig_width=None, fig_height=None, columns=1):
        """Set up matplotlib's RC params for LaTeX plotting.
        Call this before plotting a figure.

        Parameters
        ----------
        fig_width : float, optional, inches
        fig_height : float,  optional, inches
        columns : {1, 2}
        """

        # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

        # Width and max height in inches for IEEE journals taken from
        # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

        import matplotlib
        from math import sqrt

        assert (columns in [1, 2])

        if fig_width is None:
            fig_width = 3.39 if columns == 1 else 6.9  # width in inches

        if fig_height is None:
            golden_mean = (sqrt(5) - 1.0) / 2.0  # Aesthetic ratio
            fig_height = fig_width * golden_mean  # height in inches

        MAX_HEIGHT_INCHES = 18.0
        if fig_height > MAX_HEIGHT_INCHES:
            print("WARNING: fig_height too large:" + fig_height +
                  "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
            fig_height = MAX_HEIGHT_INCHES

        params = {
            'backend': 'ps',
            'text.latex.preamble': ['\usepackage{gensymb}'],
            'axes.labelsize': 18,  # fontsize for x and y labels (was 10)
            'axes.titlesize': 18,
            'font.size': 18,  # was 10
            'legend.fontsize': 8,  # was 10
            'xtick.labelsize': 12,
            'ytick.labelsize': 12,
            'text.usetex': True,
            'figure.figsize': [fig_width, fig_height],
            'font.family': 'serif'
        }

        matplotlib.rcParams.update(params)

def make_bar_chart(weights, topics, busname):
    """
    Creates a bar chart that shows the distribution of topic weights
    for one particular business
    """
    
    # replace '&' characters with 'and' so they can be read by LaTeX
    busname = busname.replace('&','and')    
    
    import matplotlib.pyplot as plt
    import numpy as np
    
    plt.clf()
    latexify()
    index = np.arange(len(topics))
    plt.bar(index, weights, color='#e78ac3')
    plt.ylabel('Normalized topic weight')
    plt.title(busname)
    plt.xticks(index + 0.5, topics, rotation = 20)
    flatbusname = '_'.join(busname.split())
    # edit the following line to fit with category and color
    filename = ''.join(['graphics/bars_L5_T5_e78ac3_', flatbusname, '.pdf'])
    plt.tight_layout()
    plt.savefig(filename, dpi=300)
    

def make_topic_bar_charts(topicDistFile, business = ''):
    """
    Reads in topic distribution data from file
    If business is specified, makes topic distribution bar chart for that bid
    Otherwise, selects 5 businesses with > 20 reviews,
    and creates bar charts that show the topic distribution for each business
    Saves image for business with name busname to graphics/busname.pdf
    """
    
    # Hard code the topic names you want
    topics = ('BBQ, Beer \& Wings', 'Las Vegas', 'Wine, Steak \& Seafood',
              'Irish Pubs', 'Clubs \& Specialty Bars')           
    
     
    from pymongo import MongoClient  
    bus = MongoClient().yelpdb.bus    
    
    if business:
    #   Just make the chart for the specified business
        with open(topicDistFile, 'r') as tdf:
            for line in tdf:
                data = line.strip().split()
                bid = data[0]
                if business == bid:
                    dist = tuple(float(i) for i in data[1:])
                    break
            cursor = bus.find({'business_id': business})
            for b in cursor:
                make_bar_chart(dist, topics, b['name'])
            print b['name']

    else:
        # Make the chart for the first 5 businesses with > 20 reviews
        chartsmade = 0       
        with open(topicDistFile, 'r') as tdf:
            for line in tdf:
                if chartsmade < 10:
                    data = line.strip().split()
                    bid = data[0]
                    dist = tuple(float(i) for i in data[1:])
                    cursor = bus.find({'business_id': bid})
                    for b in cursor:
                        if b['review_count'] > 20:
                            make_bar_chart(dist, topics, b['name'])
                            print b['name']
                            chartsmade += 1

## to test    
#if __name__ == '__main__':
#    make_topic_bar_charts('topics_bars_L5_T5.txt')
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
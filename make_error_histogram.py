# example usage for stacked csvs
# import numpy as np
# test = np.genfromtxt('test.csv', delimiter = ',')
# guess = np.genfromtxt('guess.csv', delimiter = ',')
# error_report_and_hist(test, guess, stacks = 5, fname = 'All Users and Businesses')


def error_report_and_hist(test, guess,
                          stacks=1,
                          fname='',
                          title='Histogram of Errors',
                          xlab='Error Magnitude',
                          ylab='Frequency',
                          summary=False,
                          normed=False,
                          ymax=0):
    """
    report root-mean-square error for the data provided when left as is and
    when rounded

    params:
        test: array, the true values
        guess: array, the estimated values
        fname: string, the output filename of the histogram
                    if left empty, no histogram will be made
        title: string, the title for the histogram
        xlab: string, the label for the histogram's x axis
        ylab: string, the label for the histogram's y axis
        summary: bool, if true prints summary statistics of errors
    """

    import numpy as np

    def rmse(x, y):
        import numpy as np
        return np.sqrt(np.mean((x - y) ** 2))

    def latexify(fig_width=None, fig_height=None, columns=1):
        """Set up matplotlib's RC params for LaTeX plotting.
        Call this before plotting a figure.

        Parameters
        ----------
        fig_width : float, optional, inches
        fig_height : float,  optional, inches
        columns : {1, 2}
        """

        # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

        # Width and max height in inches for IEEE journals taken from
        # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

        import matplotlib
        from math import sqrt

        assert (columns in [1, 2])

        if fig_width is None:
            fig_width = 3.39 if columns == 1 else 6.9  # width in inches

        if fig_height is None:
            golden_mean = (sqrt(5) - 1.0) / 2.0  # Aesthetic ratio
            fig_height = fig_width * golden_mean  # height in inches

        MAX_HEIGHT_INCHES = 8.0
        if fig_height > MAX_HEIGHT_INCHES:
            print("WARNING: fig_height too large:" + fig_height +
                  "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
            fig_height = MAX_HEIGHT_INCHES

        params = {
            'backend': 'ps',
            'text.latex.preamble': ['\usepackage{gensymb}'],
            'axes.labelsize': 8,  # fontsize for x and y labels (was 10)
            'axes.titlesize': 8,
            'font.size': 8,  # was 10
            'legend.fontsize': 8,  # was 10
            'xtick.labelsize': 8,
            'ytick.labelsize': 8,
            'text.usetex': True,
            'figure.figsize': [fig_width, fig_height],
            'font.family': 'serif'
        }

        matplotlib.rcParams.update(params)

    if stacks > 1:
        breaks = np.linspace(0, len(test), stacks + 1).astype(int)
        index_pairs = [(breaks[i], breaks[i + 1])
                       for i in range(len(breaks) - 1)]
        test_set = [test[break_pair[0]:break_pair[1]]
                    for break_pair in index_pairs]
        guess_set = [guess[break_pair[0]:break_pair[1]]
                     for break_pair in index_pairs]

        e_straight = []
        e_round = []

        for i in range(len(test_set)):
            e_straight.append(rmse(test_set[i], guess_set[i]))

            guess_round = np.round(guess_set[i])
            guess_round[guess_round > 5] = 5
            guess_round[guess_round < 1] = 1
            e_round.append(rmse(test_set[i], guess_round))

        print "avg rmse: ", round(np.mean(e_straight), 4), \
            "+-", round(np.std(e_straight) / np.sqrt(stacks), 4)
        print "avg rounded: ", round(np.mean(e_round), 4), \
            "+-", round(np.std(e_round) / np.sqrt(stacks), 4)

        test = test_set[0]
        guess = guess_set[0]

    else:
        e_straight = rmse(test, guess)

        guess_round = np.round(guess)
        guess_round[guess_round > 5] = 5
        guess_round[guess_round < 1] = 1
        e_round = rmse(test, guess_round)

        print "rmse: ", round(e_straight, 4)
        print "rounded rmse: ", round(e_round, 4)

    if summary:
        import pandas as pd
        print pd.DataFrame(np.abs(guess - test)).describe().transpose()

    if fname:
        import matplotlib.pyplot as plt
        import matplotlib
        latexify()
        plt.hist(np.abs(test - guess), bins=np.arange(0, 5, 1), normed=normed)
        plt.title(title)
        plt.xlabel(xlab)
        plt.ylabel(ylab)
        if ymax:
            plt.ylim((0, ymax))
        plt.tight_layout()
        plt.savefig(fname, dpi=300)
        plt.clf()

"""
provides a function compute_user_preference() which creates a nested dictionary
from uid to cat to user-preference-weight as needed for LBPA

authors: daniel and tamar
"""


def make_uid_to_bid():
    """
    make a uid to bid dictionary
    """
    from pymongo import MongoClient
    from collections import defaultdict
    crev = MongoClient().char.rev
    prev = MongoClient().pitt.rev

    out = defaultdict(list)
    for r in crev.find():
        out[r['user_id']].append(r['business_id'])
    for r in prev.find():
        out[r['user_id']].append(r['business_id'])

    return out


def make_bid_to_cat():
    """
    make a bid to category dictionary
    """
    from pymongo import MongoClient
    cbus = MongoClient().char.bus
    pbus = MongoClient().pitt.bus

    out = {}
    for b in cbus.find():
        out[b['business_id']] = b['categories']
    for b in pbus.find():
        out[b['business_id']] = b['categories']

    return out


def make_uid_to_cat_to_count():
    """
    make a uid to category to number of reviews in a category dictionary
    """
    from collections import defaultdict
    from tree_helpers import make_cat_ancestry_dict

    uid_to_bid = make_uid_to_bid()
    bid_to_cat = make_bid_to_cat()
    cat_ancestry = make_cat_ancestry_dict()

    out = defaultdict(lambda: defaultdict(int))
    for uid, bids in uid_to_bid.iteritems():
        for bid in bids:
            set_to_inc = set(bid_to_cat[bid])
            for cat in set_to_inc:
                set_to_inc = set_to_inc.union(cat_ancestry[cat])
            for cat in set_to_inc:
                out[uid][cat] += 1

    return out


def compute_user_preference():
    """
    make a uid to category to user-preference-weight dictionary
    """
    from collections import defaultdict
    import numpy

    uid_to_bid = make_uid_to_bid()
    user_counts = make_uid_to_cat_to_count()
    user_profiles = make_uid_to_cat_to_count()

    user_to_num_rev = {uid: len(uid_to_bid[uid]) for uid in uid_to_bid}
    num_users = len(user_counts)

    cat_to_count = defaultdict(int)
    for uid in user_counts:
        for cat in user_counts[uid]:
            cat_to_count[cat] += 1

    for uid in user_profiles:
        for cat in user_profiles[uid]:
            user_profiles[uid][cat] = \
                user_counts[uid][cat] / float(user_to_num_rev[uid]) \
                * numpy.log2(num_users / float(cat_to_count[cat]))

    return user_profiles

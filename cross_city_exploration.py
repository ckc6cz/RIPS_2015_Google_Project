from pymongo import MongoClient
from collections import defaultdict, Counter
from itertools import combinations, chain
from pprint import pprint

allrev = MongoClient().yelpdb.rev
allbus = MongoClient().yelpdb.bus

bid_city = {b['business_id']: b['true_city'] for b in allbus.find()}

uid_city = defaultdict(set)
for r in allrev.find():
    uid_city[r['user_id']].add(bid_city[r['business_id']])

# run this to see the breakdown of how many users have reviews in n cities
print Counter(map(len, uid_city.values()))

# figure out which city pairings are most common
print dict(Counter(map(lambda x: tuple(sorted(x)), uid_city.values())))

citysets = Counter(map(lambda x: tuple(sorted(x)), uid_city.values()))


def cityset_cmp(x, y):
    """
    x, y look like {(city1, city2, ..., cityN): number of occurances}
    """
    if len(x[0]) > len(y[0]):
        return -1
    if len(x[0]) < len(y[0]):
        return 1
    if x[1] > y[1]:
        return -1
    if x[1] < y[1]:
        return 1
    return 0


pprint(sorted(citysets.items(), cmp=cityset_cmp))


def citypair_counter(uid_city):
    citysets = map(sorted, uid_city.values())
    citypairs = filter(lambda x: len(x) == 2, citysets)
    citymultiples = filter(lambda x: len(x) > 2, citysets)
    pairified = [list(combinations(cityset, 2)) for cityset in citymultiples]
    pairified = list(chain(*pairified))
    altogether = Counter(map(tuple, chain(citypairs, pairified)))
    return sorted(altogether.items(), cmp=cityset_cmp)


pprint(citypair_counter(uid_city))

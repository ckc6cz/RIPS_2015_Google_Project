= RIPS-GOOGLE 2015

Authors:

* Chelsea Chandler (Project Manager), University of Virginia, ckc6cz@virginia.edu

* Tamar Lichter, Queens College-CUNY

* Daniel Metz, Pomona College

* Monica Ribero, Universidad de los Andes

Supported by Google LA and NSF Grant DMS-0931852

== Abstract:

As consumers are offered information and options at an unprecedented scale, many
businesses have implemented systems that predict user preferences and make
personalized recommendations. Such systems are designed to enhance the user
experience and ensure user satisfaction. Google LA has asked the RIPS team to
use data from the Yelp Challenge Dataset to compare several recommendation
systems and analyze their performance given geographically diverse data.
Versions of the following recommender models have been implemented and evaluated
by the team: basic offsets, latent factors, Location-Based Preference-Aware
(LBPA), Hidden Factors as Topics (HFT), and category offsets. It was found that
the HFT model significantly outperforms all others when trained and tested on a
single city.


== Models:

=== Offsets Models:

Implements a model where

    $\hat{r_{ij}} = \mu + \alpha_i + \beta_j$
    where
        $\alpha_i =$ mean(reviews for $user_i) - \mu$
        and
        $\beta_j =$ mean(reviews for $bus_j) - \mu$

==== Files:

    * +basic_offset_redone.py+:
        implements the basic offsets model
    * +basic_category_offseets.py+:
        implements the category offsets model
    * +basic_dolla_offsets.py+:
        implements the price offsets model
    * +basic_location_offsets.py+:
        implements the city offsets model

=== Latent Factors Model:

A model that factorizes a ratings matrix into latent user-factors and business-
factors by

    $\arg\min_{p,q} \sum_{ij} (p_i q_j - r_{ij}) + \lambda \Omega(P, Q)$

where $\Omega$ is a penalization term, frequently $\Omega(P,Q) = ||P|| + ||Q||$


==== Files:

    * +baseline.R+:
        implementation in R using +recosystem+ package

=== Location-Based Preference-Aware Model:

A model where predictions are made by matching users based on their preference
profiles

==== Files:

    * +lbpa_nonparallel.py+:
        implements LBPA in a non-parallelized fashion (thereby memory-cheaper)
    * +lbpa_parallel.py+:
        implements LBPA in a parallelized fashion (thereby faster if enough
        memory)
    * +HierarchyCreation+:
        constructs the category hierarchy tree for the LBPA model. depends on
        +categories.json+

=== Hidden Factors as Topics Model:

A model that incorporates the Latent Factors matrix-factorization technique
alongside Latent Dirichlet Allocation so as to incorporate review-text

Many outputs, visualizations, and dependencies from running the model may be
found in

    * +WORDCLOUDS/+
    * +hfht/+
    * +McDonalds.py+
    * +WordCloud.py+


== Assorted Others:

=== Files used for data-manipulation

    * +all_make_dbs.py+:
        creates the various MongoDB bits on disk
    * +all_asign_true_city.py+:
        maps businesses to their "true_city" based on latitude and longitude
        to force businesses to be assigned to one of the 10 cities included
        listed in the dataset (by K-Means)
    * +all_asign_true_city_db.py+:
        utilizes the above two scripts to update the business table to
        incorporate these true_city assignments

=== Files used for generating various results and figures

    * +latexify.py+: for vectorized text in matplotlib plots
    * +gen_report_results.py+: for a standardized method of producing plots

    * +basic_offset_intersection_corrected.py+
    * +basic_offset_intersection_hometown.py+

=== Future Work Related

    * +multimethod_lincomb.py+:
        explores possible linear combinations of the predictions of two models
        as a basic ensemble model might do


== Write-Ups

    * +FinalReport/+: a full report-style write-up
    * +FinalPresentation/+: slides for a 30-minute presentation for Projects Day
    * +FinalPresGoogle/+: slides for a 45-minute presentation at Google LA
    * +FinalReport/Graphics/+: folder including many visualizations




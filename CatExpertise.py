# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 16:55:23 2015

@author: cchandler
"""

from pymongo import MongoClient
from collections import defaultdict
import pickle
import pandas as pd


def category_experts():
    """
    returns a dictionary that maps a category name to a pandas series that relates
    a user id to the number of reviews they have made for that category
    """

    #open users to businesses dictionary
    with open('charpitt_uid_bid_dict.pickle', 'r') as f:
        user_to_businesses = pickle.load(f)

    #get database info
    client = MongoClient()
    cbus = client.char.bus
    pbus = client.pitt.bus

    #create a dictionary that relates businesses to their categories
    bid_to_cat = {}
    for b in cbus.find():
        bid_to_cat[b['business_id']] = b['categories']
    for b in pbus.find():
        bid_to_cat[b['business_id']] = b['categories']

    #create a dictionary that relates users to categories they review for
    user_to_cat = defaultdict(list)
    for b in cbus.find():
        for u, bu in user_to_businesses.iteritems():
            if b['business_id'] in bu:
                user_to_cat[u].append(b['categories'])
    for b in pbus.find():
        for u, bu in user_to_businesses.iteritems():
            if b['business_id'] in bu:
                user_to_cat[u].append(b['categories'])

    #make users' categories into a single list
    for u, c in user_to_cat.iteritems():
        newcat = reduce(lambda x, y: x + y, c)
        user_to_cat[u] = newcat

    #dictionary that relates user to a dictionary of their categories with counts of each
    for key, value in user_to_cat.iteritems():
        dict_of_counts = {}
        for v in value:
            if v in dict_of_counts:
                dict_of_counts[v] += 1
            else:
                dict_of_counts[v] = 1
        user_to_cat[key] = dict_of_counts

    #dictionary with key: category and value: vector that associates user id with category count
    cats = list(set(cbus.distinct('categories')).union(set(pbus.distinct(
        'categories'))))
    cat_vects = {}
    for cat1 in cats:
        counts = []
        index = []
        for user, diict in user_to_cat.iteritems():
            for cat, num in diict.iteritems():
                if cat == cat1:
                    counts.append(num)
                    index.append(user)
                    cat_vects[cat] = pd.Series(counts, index=index)

    return cat_vects

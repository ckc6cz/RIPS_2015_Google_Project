import numpy as np
from collections import defaultdict, Counter
from pymongo import MongoClient
from lbpa_nonparallel import bid_city
import cPickle as pickle


def basic_offset_prediction(train, test):
    """
    args:
        train (dict): {(uid, bid): rating}
        test (dict): {(uid, bid): rating}

    returns:
        dict: {(uid, bid): rating} filled by basic offset method
    """

    # prepare to set-up training and test sets
    pred = defaultdict(dict)

    uid_ratings, bid_ratings = defaultdict(list), defaultdict(list)

    # prepare for offset calculations
    for (uid, bid), stars in train.iteritems():
        uid_ratings[uid].append(stars)
        bid_ratings[bid].append(stars)

    # calculate offsets
    mu = np.mean([stars for _, stars in train.iteritems()])
    uid_offset, bid_offset = defaultdict(int), defaultdict(int)
    uid_offset.update(
        {uid: np.mean(uid_ratings[uid]) - mu
         for uid in uid_ratings})
    bid_offset.update(
        {bid: np.mean(bid_ratings[bid]) - mu
         for bid in bid_ratings})

    # make predictions!
    pred.update({
        (uid, bid): mu + uid_offset[uid] + bid_offset[bid]
        for (uid, bid) in test
    })

    return pred


if __name__ == '__main__':
    rev = MongoClient().yelpdb.rev
    bus = MongoClient().yelpdb.bus

    print 'assigning home cities for each user'
    uid_citycount = defaultdict(Counter)
    for r in rev.find():
        uid_citycount[r['user_id']][bid_city[r['business_id']]] += 1

    uid_homecity = {
        uid: citycount.most_common(1)[0][0]
        for uid, citycount in uid_citycount.iteritems()
    }

    print 'assembling the test and training data'
    test, train = defaultdict(dict), {}
    uid_bid, bid_uid = defaultdict(list), defaultdict(set)
    for r in rev.find():
        uid, bid, stars = r['user_id'], r['business_id'], r['stars']
        bcity, uhomecity = bid_city[bid], uid_homecity[uid]
        if bcity == 'Las Vegas' and uhomecity == 'Las Vegas':
            train[(uid, bid)] = stars
            uid_bid[uid].append(bid)
            bid_uid[bid].add(uid)
        elif bcity == 'Las Vegas' and uhomecity != 'Las Vegas':
            test[uhomecity][(uid, bid)] = stars
            uid_bid[uid].append(bid)
            bid_uid[bid].add(uid)

    print 'generating predictions...'
    pred, delta = defaultdict(dict), defaultdict(dict)
    for city in test:
        if city not in {'Charlotte', 'Montreal', 'Phoenix', 'Pittsburgh'}:
            continue
        print '  for {}'.format(city)
        pred[city] = basic_offset_prediction(train, test[city])
        delta[city] = [pred[city][(uid, bid)] - test[city][(uid, bid)]
                       for (uid, bid) in test[city]]
        delta[city] = np.array(delta[city])
        rmse = np.sqrt(np.mean(delta[city] ** 2))
        print '  rmse for {}: {}\n\n'.format(city, rmse)

    save = raw_input(
        'Would you like to save the results? (y/[n])\n').lower() == 'y'
    if save:
        suggested_fname = 'basic_offset_intersection.pickle'
        fname = raw_input(
            'Where would you like to save the results? [{}]\n'.format(
                suggested_fname))
        fname = fname if fname else suggested_fname
        print 'saving the deltas to {}'.format(fname)
        with open(fname, 'w') as f:
            pickle.dump(delta, f)

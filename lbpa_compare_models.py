# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 09:36:57 2015

@author: tlichter

Makes three kinds of rating predictions: Original LBPA, LBPA with all experts
weighted uniformly, and LBPA with Yelo elites as the experts. 
Not parallelized.
"""

#from UserAndTree import make_all_user_pref_trees
#from char_make_bid_uid_dict import char_make_bid_uid_dict


def computeRating(user, experts, ratings):
    """
    computes a rating for a user by weighting similarity

    args:
        user (dict): preference tree from the user we try to predict
        experts (dict): {local experts for 'user': their preference trees}
        ratings (dict): {local expert: rating for particular business}

    returns:
        float: predicted rating via weighted linear combination
    """
    from lbpa_compute_similarity import similarity

    # construct a dictionary of expert: similarity-to-user
    sim = {expert: similarity(user, experts[expert]) for expert in experts}

    # calculate total in order to normalize similarities
    total = sum(sim.values())

    # case where there are no experts
    if not total:
        return 0

    # return the two ratings if there is a rating
    return sum([sim[i] * ratings[i] / total for i in sim])


def localExperts2(user, expertise, potential_expert):
    """
    finds the local experts for a given user pre-limited to a particular
    business

    args:
        user (dict): {(cat (string), pref: float): <recursive-tree-here>}
        expertise (dict):
            {cat (string): pd.Series(expertise (float), index = uid (string)}
        potential_expert (list): uids who have reviewed the business in question

    returns:
        list: expert uids
    """
    import numpy as np
    from lbpa_compute_similarity import get_user_depth, get_dict_at_level

    experts = []
    for level in range(get_user_depth(user)):
        _, cat_scores = get_dict_at_level(user, level)
        #least_pref_weight = min(map(lambda x: x[1], cat_scores.values()))
        least_pref_weight = min(cat_scores.values())
        for cat in cat_scores:
            # case for which the category is team-made (not Yelp-provided)
            if cat.islower():
                continue

            # case when the category is indeed Yelp-provided
            k = int(np.ceil(abs(cat_scores[cat] / least_pref_weight)))
            experts.extend(
                expertise[cat].filter(potential_expert).nlargest(k).index)

    return list(set(experts))


def lbpa_elts(uid, bid, preferences, train, bid_to_uid, train_uids, elt_list):
    """
    helper function to lbpa using Yelp elites as experts

    returns the predicted rating for (uid, bid)
    """
    #Create the user preference tree
    pref_tree = preferences[uid]
    # find elites who have reviewed the business and are elites
    eids = filter(lambda eid: (eid, bid) in train.keys(),
                  bid_to_uid[bid].intersection(elt_list))

    # make sure a user cannot be their own expert
    if uid in eids:
        eids.remove(uid)

    if not eids:
        return 0
    else:
        #find preferences trees for the experts
        experts = {eid: preferences[eid] for eid in eids}
        #find the ratings
        ratings = {eid: train[(eid, bid)] for eid in eids}
        #compute predictions
        pE = computeRating(pref_tree, experts, ratings)
    return pE


def lbpa_single(uid, bid, preferences, expertise, train, bid_to_uid, train_uids
                ):
    """
    helper function to lbpa

    returns the predicted rating for (uid, bid)
    """

    #Create the user preference tree
    pref_tree = preferences[uid]

    # find local experts for the users
    potential_experts = filter(lambda eid: (eid, bid) in train.keys(),
                               bid_to_uid[bid])

    eids = localExperts2(pref_tree, expertise, potential_experts)

    # make sure a user cannot be their own expert
    if uid in eids:
        eids.remove(uid)

    # if there are no experts, predict 0
    if not eids:
        return 0, 0

    else:
        # some experts at this point might be included because they
        # have a review in both the test and training set, despite
        # not having reviewed the business for which we care
        # we remove them here

        #find preferences trees for the experts
        experts = {eid: preferences[eid] for eid in eids}
        #find the ratings
        ratings = {eid: train[(eid, bid)] for eid in eids}
        #compute predictions
        p = computeRating(pref_tree, experts, ratings)
        nexp = float(len(eids))
        if not nexp:
            nexp = 1
        pU = sum([ratings[i] for i in eids]) / nexp
        return p, pU


def lbpa(test, preferences, expertise, train, bid_to_uid, elt_list):
    """
    args:
        test (dict): {(uid (string), bid (string)): true rating (int)}
            the test set
        train (dict): {(uid (string), bid (string)): true rating (int)}
            the training set
        preferences (dict): {uid (string): preference tree}
            the tree containing all user preference trees
        expertise (dict): {cat (string): {uid: expertise}}
            relates categories to users and their expertise
        bid_to_uid (dict): {bid (string): uids (set)}
            relates bids to the uids who've reviewed the bid
        elt_list (list): uid (string) of users who are Yelp Elites

    returns:
        pred (dict): {(uid, bid): rating}: 
            rating prediction based on a weighted linear combination of experts
        predUnif (dict): {(uid, bid): rating}:
            rating prediction based on the average of experts' ratings
        predElt (dict): {(uid, bid): rating}:
            rating prediction based on weighted l.c. of elites' ratings
    """

    train_uids = [key[0] for key in train]
    pred = dict()
    predUnif = dict()
    predElt = dict()
    for uid, bid in train:
        p, pU = lbpa_single(uid, bid, preferences, expertise, train,
                            bid_to_uid, train_uids)
        pE = lbpa_elts(uid, bid, preferences, train, bid_to_uid, train_uids,
                       elt_list)
        pred[(uid, bid)] = p
        predUnif[(uid, bid)] = pU
        predElt[(uid, bid)] = pE
    return pred, predUnif, predElt


def crossVal(data, preferences, expertise, bid_uid_dict, elt_list):
    from sklearn import cross_validation
    import numpy as np

    #Create folds for crossVal
    kf = cross_validation.KFold(len(data), n_folds=5, shuffle=True)

    # prepare the outputs
    totalRes = {}
    totalResU = {}
    totalResE = {}

    # for each fold, record predictions
    folds_complete = 0
    for train_index, test_index in kf:
        print 'starting on fold', folds_complete
        train_keys = np.array(data.keys())[train_index]
        test_keys = np.array(data.keys())[test_index]
        train = {tuple(key): data[tuple(key)] for key in train_keys}
        test = {tuple(key): data[tuple(key)] for key in test_keys}
        res, resU, resE = \
            lbpa(test, preferences, expertise, train, bid_uid_dict, elt_list)
        totalRes.update(res)
        totalResU.update(resU)
        totalResE.update(resE)
        folds_complete += 1

    return totalRes, totalResU, totalResE


def testrun(limit=0, cv=False):
    """ see if lbpa works"""
    import cPickle as pickle

    # connect to mongo and generate our dictionary of ratings
    # from pymongo import MongoClient
    # rev = MongoClient().char.rev
    # ratings = {}
    # for review in rev.find():
    #     ratings[(review['user_id'], review['business_id'])] = review['stars']
    data = pickle.load(open('char_ratings.pickle', 'r'))

    #p references = make_all_user_pref_trees()
    preferences = pickle.load(open('preferences.pickle', 'r'))

    # expertise = category_experts()
    expertise = pickle.load(open('cat_experts.pickle', 'r'))

    # bid to list of users who have reviewed the business
    bid_uid_dict = pickle.load(open('bid_to_uid.pickle', 'r'))

    #    get list of elite ids
    #    can use make_elite_list.py
    elt_list = pickle.load(open('char_elt_list.pickle', 'r'))

    if limit:
        data = {key: data[key] for key in data.keys()[:limit]}

    if cv:
        pred, predUnif, predElt = \
            crossVal(data,preferences,expertise,bid_uid_dict,elt_list)
    else:
        split = len(data) / 5
        test = {v: data[v] for v in data.keys()[:split]}
        train = {v: data[v] for v in data.keys()[split:]}
        pred, predUnif, predElt = lbpa(test, preferences, expertise, train,
                                       bid_uid_dict, elt_list)

    return pred, predUnif, predElt

    ## compare where two have different results
    #for pair in pred.keys():
    #    if pred[pair] - predElt[pair]:
    #        print pair, ': ', pred[pair], predElt[pair]


def rmse(data, pred):
    """
    calculate the root mean square error of a set of predictions
    for each pair (uid, bid) in the ratings data
    """
    import numpy as np
    delta = [pred[pair] - data[pair] for pair in pred]
    delta = np.array(delta)
    return np.sqrt(np.mean(delta ** 2))
#   
#def restrict_to_nonzero(data, pred, predElt):
#    smalldata = {}    
#    for pair in pred:
#        if pred[pair] > 0 and predElt[pair] > 0:
#            smalldata[pair] = data[pair]
#    return smalldata
#     
#def numZeros(pred):
#    """
#    find out how many predictions made were 0
#    """
#    count = 0
#    for pair in pred.keys():
#        if pred[pair] == 0:
#            count += 1
#    return count

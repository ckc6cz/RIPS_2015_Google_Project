"""
sister file to lbpa_intersection
provides comparison data on how well lbpa performs for users tested in
lbpa_intersection in the context of their hometown
"""

import numpy as np
import cPickle as pickle
from pymongo import MongoClient
from collections import defaultdict, Counter
from itertools import chain
from lbpa_nonparallel import (cat_hierarchy, cat_ancestry, bid_city, bid_cat,
                              make_all_user_pref_trees, category_experts, lbpa)

rev = MongoClient().yelpdb.rev
bus = MongoClient().yelpdb.bus

if __name__ == '__main__':
    rev = MongoClient().yelpdb.rev
    bus = MongoClient().yelpdb.bus

    print 'assigning home cities for each user'
    uid_citycount = defaultdict(Counter)
    for r in rev.find():
        uid_citycount[r['user_id']][bid_city[r['business_id']]] += 1

    uid_homecity = {
        uid: citycount.most_common(1)[0][0]
        for uid, citycount in uid_citycount.iteritems()
    }

    print 'assembling the test and training data'
    test_uids = {
        r['user_id']
        for r in rev.find() if bid_city[r['business_id']] == 'Las Vegas' and
        uid_homecity[r['user_id']] != 'Las Vegas'
    }

    test, train = defaultdict(dict), defaultdict(dict)
    uid_bid = defaultdict(list)
    for r in rev.find():
        uid = r['user_id']
        bid = r['business_id']
        bcity = bid_city[bid]
        stars = r['stars']
        if uid in test_uids:
            uhomecity = uid_homecity[uid]
            if uhomecity == bcity:
                test[bcity][(uid, bid)] = stars
                uid_bid[uid].append(bid)
        else:
            train[bcity][(uid, bid)] = stars
            uid_bid[uid].append(bid)

    print 'generating preference trees...'
    preferences = make_all_user_pref_trees(uid_bid, bid_cat, cat_ancestry,
                                           cat_hierarchy)

    print 'generating category experts...'
    expertise = category_experts(uid_bid, bid_cat)

    print 'generating a business_id to user_id dictionary...'
    bid_uid = {
        city: defaultdict(set)
        for city in chain(train.keys(), test.keys())
    }
    for city in bid_uid:
        for (uid, bid) in chain(test[city], train[city]):
            bid_uid[city][bid].add(uid)

    print 'generating predictions...'
    pred = {}
    for city in test:
        print '  for {}'.format(city)
        pred[city] = lbpa(test[city], preferences, expertise, train[city],
                          bid_uid[city])
        delta = [pred[city][(uid, bid)] - test[city][(uid, bid)]
                 for (uid, bid) in pred[city]]
        delta = np.array(delta)
        rmse = np.sqrt(np.mean(delta ** 2))
        print '\n  rmse for {}: {}'.format(city, rmse)

    save = raw_input(
        'Would you like to save the results? (y/[n])\n').lower() == 'y'
    if save:
        suggested_fname = 'lbpa-pred.pickle'
        fname = raw_input(
            'Where would you like to save the results? [{}]\n'.format(
                suggested_fname))
        fname = fname if fname else suggested_fname
        print 'saving the predictions to {}'.format(fname)
        with open(fname, 'w') as f:
            pickle.dump(pred, f)

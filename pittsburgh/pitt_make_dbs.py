"""
make the pittsburgh databases (user, bus, rev)


city: pittsburgh 

author: daniel
"""

# load businesses
from import_businesses import *
businesses = import_businesses()

# assign true_city
from assign_true_cities import *
assign_true_cities(businesses)

# convert to pandas df
import pandas as pd
bus_df = pd.DataFrame(businesses)

# get only Pittsburgh
pitt_bus = bus_df[bus_df['true_city'] == 'Pittsburgh']

# import that into a database
from pymongo import MongoClient
client = MongoClient()
db = client.pitt
db.bus.insert_many([row[1].to_dict() for row in pitt_bus.iterrows()])

# great, so now we have the relevant businesses, now for reviews and users
# lets start with reviews, since they have user_ids and business_ids
pitt_bus_ids = set(db.bus.distinct('business_id'))

import ujson
with open('../data/yelp_academic_dataset_review.json') as f:
    review_list = [ujson.loads(line) for line in f]
    review_df = pd.DataFrame(review_list)

all_bus_ids = review_df['business_id'].ravel()
tf_list = []
for b_id in all_bus_ids:
    tf_list.append(b_id in pitt_bus_ids)

db.rev.insert_many([row[1].to_dict() for row in review_df[tf_list].iterrows()])

# now for users
pitt_user_ids = set(db.rev.distinct('user_id'))
with open('../data/yelp_academic_dataset_user.json') as f:
    user_list = [ujson.loads(line) for line in f]
    user_df = pd.DataFrame(user_list)

all_user_ids = user_df['user_id'].ravel()
tf_list = []
for u_id in all_user_ids:
    tf_list.append(u_id in pitt_user_ids)

db.user.insert_many([row[1].to_dict() for row in user_df[tf_list].iterrows()])

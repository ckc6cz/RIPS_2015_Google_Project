# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 09:52:45 2015

@author: tlichter
"""

from itertools import chain
from tree_helpers import make_cat_ancestry_dict

cat_ancestry = make_cat_ancestry_dict()

from string import punctuation
from nltk.stem import SnowballStemmer
from sklearn.feature_extraction.text import CountVectorizer
from stop_words import get_stop_words


def get_ancestry_set(catlist):
    """ includes catlist"""
    ancestors = set(chain(*[cat_ancestry[cat] for cat in catlist
                            if cat in cat_ancestry]))
    return ancestors.union(catlist)


def make_bid_to_all_cat():
    """
    Creates a dictionary that maps every business to a list of categories.
    The categories are the ones already applied to the business,
    plus any parent categories in the category hierarchy.
    """
    from pymongo import MongoClient
    bus = MongoClient().yelpdb.bus

    bus_cat = {b['business_id']: b['categories'] for b in bus.find()}

    out = {bid: get_ancestry_set(bus_cat[bid]) for bid in bus_cat}

    return out


def make_rev_dict(catset):
    from pymongo import MongoClient
    # change the db selected below to restrict by location
    rev = MongoClient().yelpdb.rev

    bid_allcat = make_bid_to_all_cat()

    out = {
        (r['user_id'], r['business_id']): (r['stars'], r['text'])
        for r in rev.find()
        if bid_allcat[r['business_id']].intersection(catset)
    }

    return out


stemmer = SnowballStemmer('english')

sws = set(chain(
    CountVectorizer(min_df=1,
                    stop_words='english').get_stop_words(),
    get_stop_words('french'), get_stop_words('german')))
punc = set(punctuation)


def removePunc(wordlist):
    out = []
    for word in wordlist:
        out.append(''.join(ch for ch in word if ch not in punc))
    return out


def stemify(wordlist):
    wordlist = removePunc(wordlist)
    for word in wordlist:
        if word in sws:
            wordlist.remove(word)
    stems = map(stemmer.stem, wordlist)
    return stems


def write_for_hft(fname, catlist):
    """ run to create a file formatted for hft """
    data = make_rev_dict(catlist)
    with open(fname, 'w') as f:
        for (uid, bid) in data:
            stars, review = data[(uid, bid)]
            time = '0'
            review = review.encode('ascii', 'ignore').lower().split()
            review = ' '.join(stemify(review))
            review_len = str(len(review.split()))
            line = ' '.join([uid, bid, str(stars), time, review_len, review,
                             '\n'])
            f.write(line)

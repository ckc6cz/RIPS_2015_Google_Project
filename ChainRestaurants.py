# -*- coding: utf-8 -*-
"""
some methods that find the chain restaurants and plays with them a bit

@author: cchandler
"""


def chains():
    """
    returns a dictionary of chain restaurants that links restaurant name to a set
    of all of the cities it is reviewed in
    """
    from pymongo import MongoClient
    bus = MongoClient().yelpdb.bus

    #get dictionary of all business names mapped to cities it appears in
    merged = {}
    for b in bus.find():
        if b['name'] not in merged.keys():
            merged[b['name']] = [b['true_city']]
        else:
            merged[b['name']].append(b['true_city'])

    #filter out the businesses that are only in one location
    chain_dict = {}
    for k, v in merged.iteritems():
        if len(set(v)) > 1:
            chain_dict[k] = set(v)

    return chain_dict


def some_chains(chain_dict, number):
    """
    given the chain dictionary and a city count between 2 and 10, this function
    returns the businesses that have reviews in that number of cities
    """

    in_number = {}
    for k, v in chains.iteritems():
        if len(v) == number:
            in_number[k] = v

    return in_number


def get_bids(name):
    """
    given a business name, return a list of its business_ids
    helper function for get_total_reviews()
    """
    from pymongo import MongoClient
    bus = MongoClient().yelpdb.bus

    bids = []
    for entry in bus.find({'name': name}):
        bids.append(entry['business_id'])

    return bids


def get_total_reviews(name):
    """
    given a business name, the function calls get_bids() to find out what its different
    bids are. then this function computes how many reviews it has in the entire dataset
    """
    from pymongo import MongoClient
    rev = MongoClient().yelpdb.rev
    bids = get_bids(name)

    count = 0
    for bid in bids:
        for r in rev.find({'business_id': bid}):
            count += 1

    return count

#playing with the data
chain_dict = chains()

#print dictionary of businesses reviewed in 9 cities
print some_chains(chain_dict, 7)

#find out how many reviews a specific business has
print get_total_reviews('The Home Depot')

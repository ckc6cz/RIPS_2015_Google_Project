# -*- coding: utf-8 -*-
"""
create a category tree
@author: cchandler
"""

import json


def make_category_tree():
    """ 
    Using the category.json file from https://www.yelp.com/developers/documentation/v2/all_category_list
    this function creates a nested dictionary that represents the hierarchy of 
    categories. There are 12 major categories, some having more categories branching
    from it and some just containing the Yelp categories (the leaves of the 'tree')      
    """

    with open('categories.json') as data_file:
        data = json.load(data_file)

    parents = []
    titles = []
    for entry in data:
        parents.append(entry['parents'])
        titles.append(entry['title'])

    pset = set()
    for par in parents:
        for p in par:
            pset.add(p)

    dictionary = dict(zip(titles, parents))

    #dict 2 is the parent to children dictionary
    dict2 = {}
    for parent in pset:
        titles = []
        for key, value in dictionary.iteritems():
            if parent in value:
                titles.append(key)
                dict2[parent] = titles

    values = []
    for key, value in dict2.iteritems():
        for v in value:
            values.append(v)

    overlap = []
    for p in pset:
        for v in values:
            if p == v.lower():
                overlap.append(p)

    disjoint = pset.difference(set(overlap))

    #remove: None : [u'Restaurants', u'Automotive', u'Home Services', u'Health & Medical', u'Active Life', u'Beauty & Spas', u'Mass Media', u'Professional Services', u'Food', u'Pets', u'Local Services', u'Arts & Entertainment', u'Public Services & Government', u'Education', u'Real Estate', u'Bicycles', u'Shopping', u'Religious Organizations', u'Local Flavor', u'Hotels & Travel', u'Event Planning & Services', u'Nightlife', u'Financial Services']
    disjoint.remove(None)

    #remove the categories that will become sub-categories below
    disjoint.remove('medcenters')
    disjoint.remove('dentalhygienists')
    disjoint.remove('c_and_mh')
    disjoint.remove('diagnosticservices')
    disjoint.remove('physicians')
    disjoint.remove('latin')
    disjoint.remove('mideastern')
    disjoint.remove('localflavor')
    disjoint.remove('gourmet')
    disjoint.remove('fitness')
    disjoint.remove('sportgoods')
    disjoint.remove('bicycles')
    disjoint.remove('petservices')
    disjoint.remove('itservices')
    disjoint.remove('financialservices')
    disjoint.remove('publicservicesgovt')
    disjoint.remove('realestate')
    disjoint.remove('localservices')
    disjoint.remove('homeservices')
    disjoint.remove('homeandgarden')
    disjoint.remove('eventservices')
    disjoint.remove('artsandcrafts')
    disjoint.remove('media')
    disjoint.remove('shopping')
    disjoint.remove('musicinstrumentservices')
    disjoint.remove('massmedia')
    disjoint.remove('specialtyschools')
    disjoint.remove('hair')
    disjoint.remove('hairremoval')
    disjoint.remove('flowers')
    disjoint.remove('auto')
    disjoint.remove('transport')

    #create nested dictionary with the leaves having blank dictionaries
    hierarchy = {}
    for d in disjoint:
        hierarchy[d] = {}
        for n in dict2[d]:
            hierarchy[d][n] = {}
            for x in overlap:
                if x.capitalize() in dict2[d]:
                    hierarchy[d][x] = {}
    for key, value in hierarchy.iteritems():
        for k, v in value.iteritems():
            if k in dict2.keys():
                for x in dict2[k]:
                    hierarchy[key][k][x] = {}

    #add medcenters dentalhygienists c_and_mg diagnosticservices and physicians to health category
    hierarchy['health']['medcenters'] = {}
    for x in dict2['medcenters']:
        hierarchy['health']['medcenters'][x] = {}
    hierarchy['health']['dentalhygienists'] = {}
    for y in dict2['dentalhygienists']:
        hierarchy['health']['dentalhygienists'][y] = {}
    hierarchy['health']['c_and_mh'] = {}
    for z in dict2['c_and_mh']:
        hierarchy['health']['c_and_mh'][z] = {}
    hierarchy['health']['diagnosticservices'] = {}
    for zz in dict2['diagnosticservices']:
        hierarchy['health']['diagnosticservices'][zz] = {}
    hierarchy['health']['physicians'] = {}
    for a in dict2['physicians']:
        hierarchy['health']['physicians'][a] = {}

    #add latin localflavor and mideastern to restaurants
    hierarchy['restaurants']['mideastern'] = {}
    for b in dict2['mideastern']:
        hierarchy['restaurants']['mideastern'][b] = {}
    hierarchy['restaurants']['latin'] = {}
    for c in dict2['latin']:
        hierarchy['restaurants']['latin'][c] = {}
    hierarchy['restaurants']['localflavor'] = {}
    for cc in dict2['localflavor']:
        hierarchy['restaurants']['localflavor'][cc] = {}

    #add gourmet and restaurants to food
    hierarchy['food']['gourmet'] = {}
    for d in dict2['gourmet']:
        hierarchy['food']['gourmet'][d] = {}
    hierarchy['food']['restaurants'] = {}
    for key, value in hierarchy['restaurants'].iteritems():
        hierarchy['food']['restaurants'][key] = {}
        if key in dict2.keys():
            for e in dict2[key]:
                hierarchy['food']['restaurants'][key][e] = {}

    #remove restaurants entry
    hierarchy.pop('restaurants')

    #add fitness sportgoods and bicycles to active
    hierarchy['active']['fitness'] = {}
    for f in dict2['fitness']:
        hierarchy['active']['fitness'][f] = {}
    hierarchy['active']['sportgoods'] = {}
    for g in dict2['sportgoods']:
        hierarchy['active']['sportgoods'][g] = {}
    hierarchy['active']['bicycles'] = {}
    for h in dict2['bicycles']:
        hierarchy['active']['bicycles'][h] = {}

    #add petservices to pets
    hierarchy['pets']['petservices'] = {}
    for i in dict2['petservices']:
        hierarchy['pets']['petservices'][i] = {}

    #add itservices, fincancialservices, publicservicesgovt, localservices eventservices homeservices homeandgarden and realestate to professional
    hierarchy['professional']['itservices'] = {}
    for j in dict2['itservices']:
        hierarchy['professional']['itservices'][j] = {}
    hierarchy['professional']['financialservices'] = {}
    for k in dict2['financialservices']:
        hierarchy['professional']['financialservices'][k] = {}
    hierarchy['professional']['publicservicesgovt'] = {}
    for l in dict2['publicservicesgovt']:
        hierarchy['professional']['publicservicesgovt'][l] = {}
    hierarchy['professional']['realestate'] = {}
    for m in dict2['realestate']:
        hierarchy['professional']['realestate'][m] = {}
    hierarchy['professional']['localservices'] = {}
    for n in dict2['localservices']:
        hierarchy['professional']['localservices'][n] = {}
    hierarchy['professional']['homeservices'] = {}
    for o in dict2['homeservices']:
        hierarchy['professional']['homeservices'][o] = {}
    hierarchy['professional']['eventservices'] = {}
    for p in dict2['eventservices']:
        hierarchy['professional']['eventservices'][p] = {}
    hierarchy['professional']['homeandgarden'] = {}
    for pp in dict2['homeandgarden']:
        hierarchy['professional']['homeandgarden'][pp] = {}

    #add arstandcrafts media shopping musicinstrumentservices flowers and massmedia to arts
    hierarchy['arts']['artsandcrafts'] = {}
    for q in dict2['artsandcrafts']:
        hierarchy['arts']['artsandcrafts'][q] = {}
    hierarchy['arts']['media'] = {}
    for r in dict2['media']:
        hierarchy['arts']['media'][r] = {}
    hierarchy['arts']['shopping'] = {}
    for s in dict2['shopping']:
        hierarchy['arts']['shopping'][s] = {}
    hierarchy['arts']['musicinstrumentservices'] = {}
    for t in dict2['musicinstrumentservices']:
        hierarchy['arts']['musicinstrumentservices'][t] = {}
    hierarchy['arts']['massmedia'] = {}
    for u in dict2['massmedia']:
        hierarchy['arts']['massmedia'][u] = {}
    hierarchy['arts']['flowers'] = {}
    for u in dict2['flowers']:
        hierarchy['arts']['flowers'][u] = {}

    #add specialtyschools to education
    hierarchy['education']['specialtyschools'] = {}
    for v in dict2['specialtyschools']:
        hierarchy['education']['specialtyschools'][v] = {}

    #adde hair and hairremoval to beautysvc
    hierarchy['beautysvc']['hair'] = {}
    for w in dict2['hair']:
        hierarchy['beautysvc']['hair'][w] = {}
    hierarchy['beautysvc']['hairremoval'] = {}
    for ww in dict2['hairremoval']:
        hierarchy['beautysvc']['hairremoval'][ww] = {}

    #add transport and auto to hotelstravel
    hierarchy['hotelstravel']['transport'] = {}
    for x in dict2['transport']:
        hierarchy['hotelstravel']['transport'][x] = {}
    hierarchy['hotelstravel']['auto'] = {}
    for y in dict2['auto']:
        hierarchy['hotelstravel']['auto'][y] = {}

    return hierarchy

#for key, value in hierarchy.iteritems():
#    print key
#    print value

#with open('parents_to_cats.csv','wb') as f:
#   w = csv.writer(f)
#   w.writerows(dict2.items())

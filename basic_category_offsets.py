"""
basic_category_offsets

pred = mean([catmu+beta_user+beta_bus for cat in bus_to_cat[bus]])
"""
import numpy as np
from sklearn.cross_validation import KFold
from collections import defaultdict, Counter
from itertools import chain
from pymongo import MongoClient
from HierarchyCreation import make_category_tree
from tree_helpers import make_cat_ancestry_dict
import sys
import cPickle as pickle

# connect to relevant collections
rev = MongoClient().yelpdb.rev
bus = MongoClient().yelpdb.bus

cat_ancestry = make_cat_ancestry_dict()
cat_tree = make_category_tree()


def get_ancestry_set(catlist):
    """ includes catlist """
    ancestors = set(chain(*[cat_ancestry[cat] for cat in catlist
                            if cat in cat_ancestry]))
    return ancestors.union(catlist)


def make_cat_rev_dictionary(revs, bus_db):
    """ make {cat: {(uid, bid): stars}} """
    bid_allcat = {
        b['business_id']: get_ancestry_set(b['categories'])
        for b in bus_db.find()
    }

    cat_rev = defaultdict(dict)
    for (uid, bid), stars in revs.iteritems():
        for cat in bid_allcat[bid]:
            cat_rev[cat].update({(uid, bid): stars})

    return cat_rev


def format_data(cities=None):
    """ returns dict: {(uid, bid): rating}  """
    if not cities:
        cities = set(bus.distinct('true_city'))

    bid_city = {b['business_id']: b['true_city'] for b in bus.find()}
    allrevs = {
        (r['user_id'], r['business_id']): r['stars']
        for r in rev.find() if bid_city[r['business_id']] in cities
    }
    return make_cat_rev_dictionary(allrevs, bus)


def basic_cat_offset_prediction(data):
    """
    args:
        data (dict): {cat: {(uid, bid): rating}}

    returns:
        dict: {(uid, bid): rating} filled by basic offset method
    """
    flatdata = {
        (uid, bid): rating
        for cat in data.values() for (uid, bid), rating in cat.items()
    }
    uid_count = Counter(uid for (uid, _) in flatdata)
    bid_count = Counter(bid for (_, bid) in flatdata)

    # prepare to set-up training and test sets
    pred = defaultdict(list)

    allmu = np.mean(list(chain(*[x.values() for _, x in data.iteritems()])))

    for cat, catdata in data.iteritems():
        uid_bid_keys = np.array([(uid, bid) for (uid, bid) in catdata.keys()
                                 if uid_count[uid] >= 3 and bid_count[bid] >= 5
                                 ])
        try:
            kf = KFold(len(uid_bid_keys), n_folds=5, shuffle=True)
            for train_index, test_index in kf:
                train, test = uid_bid_keys[train_index
                                           ], uid_bid_keys[test_index]
                uid_ratings, bid_ratings = defaultdict(list), defaultdict(list)

                # prepare for offset calculations
                for (uid, bid) in train:
                    uid_ratings[uid].append(catdata[(uid, bid)])
                    bid_ratings[bid].append(catdata[(uid, bid)])

                # calculate offsets
                cat_mu = np.mean([catdata[(uid, bid)] for (uid, bid) in train])
                uid_offset, bid_offset = defaultdict(int), defaultdict(int)
                uid_offset.update({
                    uid: np.mean(uid_ratings[uid]) - cat_mu
                    for uid in uid_ratings
                })
                bid_offset.update({
                    bid: np.mean(bid_ratings[bid]) - cat_mu
                    for bid in bid_ratings
                })
                # make predictions!
                for (uid, bid) in test:
                    pred[(uid, bid)].append(
                        cat_mu + uid_offset[uid] + bid_offset[bid])

        except ValueError:
            # case when the category is too small to use folds
            # note that every business that lists such a category also lists
            # a more popular category so we don't have to worry about a
            # business failing to have any ratings
            continue

    for (uid, bid), predictions in pred.iteritems():
        pred[(uid, bid)] = np.mean(predictions)

    return pred


if __name__ == '__main__':
    args = set(sys.argv[1:])
    if not args:
        args = raw_input(
            'Choose input cities (e.g. \'Charlotte Pittsburgh\')\n')
        args = {arg.capitalize() for arg in args.split()}
    if 'English' in args:
        cities = {'Charlotte', 'Edinburgh', 'Las Vegas', 'Madison', 'Waterloo',
            'Phoenix', 'Pittsburgh', 'Urbana-Champaign'}
    cities = args.intersection(bus.distinct('true_city'))

    print 'formatting the data...'
    data = format_data(cities)

    print 'generating predictions...'
    pred = basic_cat_offset_prediction(data)

    print 'comparing for results...'
    flatdata = {
        (uid, bid): rating
        for cat in data.values() for (uid, bid), rating in cat.items()
    }
    delta = {
        (uid, bid): pred[(uid, bid)] - flatdata[(uid, bid)]
        for (uid, bid) in pred
    }

    rmse = np.sqrt(np.mean(np.array(delta.values()) ** 2))
    print 'rmse: {:f}'.format(rmse)

    fname = raw_input('To save the data, please give a filename\n')
    if fname:
        with open(fname, 'w') as f:
            pickle.dump(delta, f)

    from gen_report_results import report
    report(delta)

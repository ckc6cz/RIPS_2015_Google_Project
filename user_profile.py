# -*- coding: utf-8 -*-
"""
author: CHELSEA
Creates a user to category visits dictionary 
"""

from pymongo import MongoClient
from collections import defaultdict
import pickle
import pandas as pd
import operator

#open users to businesses dictionary
with open(
    '/home/cchandler/anaconda/RIPS_2015_Google_Project/char_uid_bid_dict.pickle',
    'r') as f:
    user_to_businesses = pickle.load(f)

#get database info
client = MongoClient()
bus = client.char.bus
user = client.char.user

#create a dictionary that relates businesses to their categories
bid_to_cat = {}
for b in bus.find():
    bid_to_cat[b['business_id']] = b['categories']

for key, value in bid_to_cat.iteritems():
    print key
    print value

#create a dictionary that relates users to categories they review for
user_to_cat = defaultdict(list)
for b in bus.find():
    for u, bu in user_to_businesses.iteritems():
        if b['business_id'] in bu:
            user_to_cat[u].append(b['categories'])

#make users' categories into a single list
for u, c in user_to_cat.iteritems():
    newcat = reduce(lambda x, y: x + y, c)
    user_to_cat[u] = newcat

    #dictionary that relates user to a dictionary of their categories with counts of each
for key, value in user_to_cat.iteritems():
    dict_of_counts = {}
    for v in value:
        if v in dict_of_counts:
            dict_of_counts[v] += 1
        else:
            dict_of_counts[v] = 1
    user_to_cat[key] = dict_of_counts

#address individual category counts with "user_to_cat[user id][category name]"

#dictionary with key: category and value: vector that associates user id with category count
cats = bus.distinct('categories')
cat_vects = {}
for cat1 in cats:
    counts = []
    index = []
    for user, diict in user_to_cat.iteritems():
        for cat, num in diict.iteritems():
            if cat == cat1:
                counts.append(num)
                index.append(user)
                cat_vects[cat] = pd.Series(counts, index=index)
for key, value in cat_vects.iteritems():
    value.sort()

#create a total number count per category to see most popular categories
cat_to_count = {}
for key, value in cat_vects.iteritems():
    total = value.sum()
    cat_to_count[key] = total
sorted_c = sorted(cat_to_count.items(), key=operator.itemgetter(1))
for n in sorted_c:
    print n

    #dictionary with key: user id and value: vector that counts category visits
user_prof = {}
for user, cats in user_to_cat.iteritems():
    cats1 = []
    nums = []
    for cat, num in cats.iteritems():
        cats1.append(cat)
        nums.append(num)
        user_prof[user] = pd.Series(nums, index=cats1)
for key, value in user_prof.iteritems():
    value.sort()
for key, value in user_prof.iteritems():
    print key
    print value

#user with the highest visits to a categor ~PROBS NOT HOW WE WANT TO IMPLEMENT THIS AT ALL~
local_experts = {}
for key, value in cat_vects.iteritems():
    highest_val = value.index[-1]
    local_experts[key] = highest_val

print len(local_experts)
first5 = {k: local_experts[k] for k in local_experts.keys()[:5]}
print first5

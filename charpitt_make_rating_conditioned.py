"""
INCOMPLETE

provides a call-able function to create a reduced ratings matrix for charlotte
based on a defined minimum review count for users and businesses

author: daniel
"""

from pymongo import MongoClient
import numpy as np
import pandas as pd


def make_rating_matrix_conditioned(fname='out.csv',
                                   min_user_rev_count=3,
                                   min_bus_rev_count=5):
    """
    creates a rating matrix for charlotte conditioned such that all users
    and businesses in the matrix have a number of reviews as specified by
    the parameters and that the counted reviews are contained in the resultant
    matrix. matrix is written to csv with filename fname
    """
    # connect to Mongo and alias the rev table
    client = MongoClient()
    crev = client.char.rev
    prev = client.pitt.rev

    # get uids and bids
    uids = list(set(crev.distinct('user_id')).union(set(prev.distinct(
        'user_id'))))
    bids = list(set(crev.distinct('business_id')).union(set(prev.distinct(
        'business_id'))))
    uids.sort()
    bids.sort()

    # prepare the empty rating matrix
    rating = np.empty((len(uids), len(bids)))
    rating.fill(np.nan)

    for r in crev.find():
        row = uids.index(r['user_id'])
        col = bids.index(r['business_id'])
        rating[row, col] = r['stars']
    for r in prev.find():
        row = uids.index(r['user_id'])
        col = bids.index(r['business_id'])
        rating[row, col] = r['stars']

    # eliminate users are businesses that are too sparse
    while any((rating > 0).sum(axis = 0) < min_bus_rev_count) or \
        any((rating > 0).sum(axis = 1) < min_user_rev_count):
        rating = np.delete(rating, np.where(
            (rating > 0).sum(axis=1) < min_user_rev_count),
                           axis=0)
        rating = np.delete(rating, np.where(
            (rating > 0).sum(axis=0) < min_bus_rev_count),
                           axis=1)
        # check against the empty array edge case
        if not rating.any(): break

    np.savetxt(fname, rating, delimiter=',')

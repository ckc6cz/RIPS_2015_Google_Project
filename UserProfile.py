# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 09:05:16 2015

@author: cchandler
"""


def create_user_profile():
    from pymongo import MongoClient
    from collections import defaultdict
    from tree_helpers import make_cat_ancestry_dict
    import pandas as pd

    #get database info
    client = MongoClient()
    cbus = client.char.bus
    crev = client.char.rev
    pbus = client.pitt.bus
    prev = client.pitt.rev

    #make users to businesses dictionary
    user_to_businesses = defaultdict(list)
    for r in crev.find():
        user_to_businesses[r['user_id']].append(r['business_id'])
    for r in prev.find():
        user_to_businesses[r['user_id']].append(r['business_id'])

    #create a dictionary that relates businesses to their categories
    bid_to_cat = {}
    for b in cbus.find():
        bid_to_cat[b['business_id']] = b['categories']
    for b in pbus.find():
        bid_to_cat[b['business_id']] = b['categories']

    #make cat_ancestry
    cat_ancestry = make_cat_ancestry_dict()

    #create a dictionary that relates users to categories they review for
    user_to_cat = defaultdict(list)
    for uid, bids in user_to_businesses.iteritems():
        for bid in bids:
            set_to_inc = set(bid_to_cat[bid])
            for cat in set_to_inc:
                set_to_inc = set_to_inc.union(cat_ancestry[cat])
            user_to_cat[uid] += set_to_inc

    #dictionary that relates user to a dictionary of their categories with counts of each
    for uid, cats in user_to_cat.iteritems():
        dict_of_counts = defaultdict(int)
        for cat in cats:
            dict_of_counts[cat] += 1
        user_to_cat[uid] = dict_of_counts

    #dictionary with key: user id and value: vector that counts category visits
    user_prof = {}
    for user, cat_to_counts in user_to_cat.iteritems():
        user_prof[user] = pd.Series(cat_to_counts)
        user_prof[user].sort()

    return user_prof

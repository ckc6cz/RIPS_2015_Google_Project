# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 09:23:56 2015

@author: cchandler
"""


def filtered_hierarchy(business):
    """
    this function takes the category expertise tree and filters out the users
    who have not reviewed the given business. For instance, if you enter business
    id 'zzgXEteMduykqN8JAUG4CA', it will return a tree of the categories the users
    who have reviewed that business have reviewed overall, mapped to a series
    of particular users to review count.
    """

    from pymongo import MongoClient
    #   from CatExpertise import category_experts
    import pickle

    #get database info
    client = MongoClient()
    bus = client.char.bus
    reviews = client.char.rev

    #make business to users who have reviewed the business dictionary
    bus_set = set()
    for b in bus.find():
        bus_set.add(b['business_id'])
    bus_to_users = {}
    for b in bus_set:
        for r in reviews.find({'business_id': b}):
            if b in bus_to_users.keys():
                bus_to_users[b].append(r['user_id'])
            else:
                bus_to_users[b] = [r['user_id']]

    #import hierarchy to be filtered    
    #cat_exps = category_experts()

    with open('cat_experts.pickle', 'r') as f:
        cat_exps = pickle.load(f)

#remove users that have never reviewed for that business
    for category, series in cat_exps.iteritems():
        for user, count in series.iteritems():
            if user not in bus_to_users[business]:
                deleteme = series.pop(user)

    #remove the categories that are now blank
    deleteme = []
    for cat, series in cat_exps.iteritems():
        if series.empty:
            deleteme.append(cat)
    for d in deleteme:
        del cat_exps[d]

    return cat_exps

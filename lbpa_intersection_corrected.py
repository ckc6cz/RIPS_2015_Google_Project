"""
sister file to lbpa_intersection
provides comparison data on how well lbpa performs for users tested in
lbpa_intersection in the context of their hometown
"""

import numpy as np
import cPickle as pickle
from pymongo import MongoClient
from collections import defaultdict, Counter
from itertools import chain
from lbpa_nonparallel import (cat_hierarchy, cat_ancestry, bid_city, bid_cat,
                              make_all_user_pref_trees, category_experts, lbpa)

if __name__ == '__main__':
    rev = MongoClient().yelpdb.rev
    bus = MongoClient().yelpdb.bus

    print 'assigning home cities for each user'
    uid_citycount = defaultdict(Counter)
    for r in rev.find():
        uid_citycount[r['user_id']][bid_city[r['business_id']]] += 1

    uid_homecity = {
        uid: citycount.most_common(1)[0][0]
        for uid, citycount in uid_citycount.iteritems()
    }

    print 'assembling the test and training data'
    test, train = defaultdict(dict), {}
    uid_bid, bid_uid = defaultdict(list), defaultdict(set)
    for r in rev.find():
        uid, bid, stars = r['user_id'], r['business_id'], r['stars']
        bcity, uhomecity = bid_city[bid], uid_homecity[uid]
        if bcity == 'Las Vegas' and uhomecity == 'Las Vegas':
            train[(uid, bid)] = stars
            uid_bid[uid].append(bid)
            bid_uid[bid].add(uid)
        elif bcity == 'Las Vegas' and uhomecity != 'Las Vegas':
            test[uhomecity][(uid, bid)] = stars
            uid_bid[uid].append(bid)
            bid_uid[bid].add(uid)

    print 'generating preference trees...'
    preferences = make_all_user_pref_trees(uid_bid, bid_cat, cat_ancestry,
                                           cat_hierarchy)

    print 'generating category experts...'
    expertise = category_experts(uid_bid, bid_cat)

    print 'generating predictions...'
    pred = {}
    for city in test:
        if city not in {'Charlotte', 'Montreal', 'Phoenix', 'Pittsburgh'}:
            continue
        print '  for {}'.format(city)
        pred[city] = lbpa(test[city], preferences, expertise, train, bid_uid)
        delta = [pred[city][(uid, bid)] - test[city][(uid, bid)]
                 for (uid, bid) in test[city]]
        delta = np.array(delta)
        rmse = np.sqrt(np.mean(delta ** 2))
        print '  rmse for {}: {}\n\n'.format(city, rmse)

    save = raw_input(
        'Would you like to save the results? (y/[n])\n').lower() == 'y'
    if save:
        suggested_fname = 'lbpa-pred.pickle'
        fname = raw_input(
            'Where would you like to save the results? [{}]\n'.format(
                suggested_fname))
        fname = fname if fname else suggested_fname
        print 'saving the predictions to {}'.format(fname)
        with open(fname, 'w') as f:
            pickle.dump(pred, f)

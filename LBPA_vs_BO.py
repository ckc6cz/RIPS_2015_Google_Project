# -*- coding: utf-8 -*-
"""
LBPA vs Basic Offsets error plotting

@author: cchandler
"""


def make_scatter(LBPA_offsets, BO_offsets):
    #given LBPA_offsets = {(uid,bid): LBPA offset}
    #      BO_offsets = {(uid, bid): BO offset}
    #want {(uid,bid): (LBPA offset, BO offset)}

    #compare the two in a dictionary
    LBPA_BO_offsets = {}
    for tup1 in LBPA_offsets.keys():
        for tup2 in BO_offsets.keys():
            if tup1 == tup2:
                LBPA_BO_offsets[tup1] = (LBPA_offsets[tup1], BO_offsets[tup2])

    #plot errors
    import matplotlib.pyplot as plt

    x_val = [x[0] for x in LBPA_BO_offsets.values()]
    y_val = [x[1] for x in LBPA_BO_offsets.values()]

    plt.scatter(x_val, y_val)
    plt.xlabel("LBPA offsets")
    plt.ylabel("BO offsets")
    plt.show()


def make_scatter2(LBPA_offsets, BO_offsets):
    #given LBPA_offsets = {(uid,bid): LBPA offset}
    #      BO_offsets = {(uid, bid): BO offset}
    #want {(uid,bid): (LBPA offset, BO offset)}

    #compare the two in a dictionary
    offsetsL = {
        (uid, bid): (LBPA_offsets[(uid, bid)], BO_offsets[(uid, bid)])
        for (uid, bid) in LBPA_offsets
    }
    offsetsB = {
        (uid, bid): (LBPA_offsets[(uid, bid)], BO_offsets[(uid, bid)])
        for (uid, bid) in BO_offsets
    }
    assert offsetsL == offsetsB

    #plot errors
    import matplotlib.pyplot as plt

    offsets = np.array(offsetsL.values())
    x_val, y_val = offsets[:, 0], offsets[:, 1]

    plt.scatter(x_val, y_val)
    plt.xlabel("LBPA offsets")
    plt.ylabel("BO offsets")
    plt.show()

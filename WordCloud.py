# -*- coding: utf-8 -*-
"""
Making wordclouds using review text alone
@author: cchandler
"""
from sklearn.feature_extraction.text import CountVectorizer
import matplotlib.pyplot as plt
from wordcloud import WordCloud
from pymongo import MongoClient

#generate word cloud for one overall city
city_reviews = MongoClient().urbana.rev

reviews = ""
for r in city_reviews.find():
    review = r['text'].encode('ascii', 'ignore').lower().split()
    rev = ' '.join(review)
    reviews += rev

#take out stop words
sws = set(CountVectorizer(min_df=1, stop_words='english').get_stop_words())
words = reviews.split()
take_out = ""
for word in words:
    if word not in sws:
        take_out += word + " "

#remove the words that they all have
take_out_more = ""
words = take_out.split()
for word in words:
    if word != "place":
        take_out_more += word + " "

wordcloud = WordCloud().generate(take_out_more)
plt.imshow(wordcloud)
plt.axis("off")
plt.show()

words = "dive " * 478
words += "band " * 446
words += "karaoke " * 437
words += "hookah " * 425
words += "irish " * 424
words += "piano " * 417
words += "edinburgh " * 405
words += "german " * 402
words += "pub " * 399
words += "jukebox " * 395
words += "tiki " * 393
words += "dart " * 387
words += "arcade " * 370
words += "essence " * 368
words += "sing " * 360
words += "der " * 357
words += "hofbrauhaus " * 344
words += "ich " * 341
words += "comedy " * 340
words += "duel " * 339
words += "juke " * 337
words += "divey " * 332
words += "das " * 329
words += "video " * 323
words += "whisky " * 321
words += "student " * 317
words += "saloon " * 317
words += "nicht " * 315
words += "song " * 315
words += "pint " * 311
words += "film " * 311
words += "shuffleboard " * 304
words += "spank " * 301
words += "musician " * 300
words += "schnitzel " * 299
words += "gay " * 299
words += "insert " * 299
words += "center " * 298
words += "ireland " * 297
words += "punk " * 295
words += "stage " * 293
words += "paddle " * 293
words += "theater " * 293

from os import path
from scipy.misc import imread
import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS

d = path.dirname(
    '/home/cchandler/anaconda/RIPS_2015_Google_Project/WORDCLOUDS/pics_and_txt/')

# Read the whole text.
#text = open(path.join(d, 'HomeDepot.txt')).read()

# read the mask image
human_mask = imread(path.join(d, "bars.jpg"))

wc = WordCloud(width=1600,
               height=800,
               background_color="white",
               max_words=75,
               stopwords=STOPWORDS.add("one"),
               mask=human_mask).generate(words)

# generate word cloud
#wc.generate(string)

# store to file
wc.to_file(path.join(d, "bar_topics.png"))

# show
plt.figure(figsize=(20, 10), facecolor='k')
plt.imshow(wc)
plt.axis("off")
plt.figure()
plt.imshow(human_mask, cmap=plt.cm.gray)
plt.axis("off")
plt.show()
#mob (5.063295) crime (3.967968) 51 (3.913557) weapon (3.769803) mobster (3.737562) mafia (3.724982) interact (3.547310) courtroom (3.502292) courthous (3.497410) read (3.476505)

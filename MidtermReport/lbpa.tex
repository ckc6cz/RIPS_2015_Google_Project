\chapter{Location-Based Preference-Aware Model} \label{ch:lbpa}

The \emph{Location-Based Preference-Aware (LBPA)} recommender model makes
recommendations based on a user's preferences for various types of venues
\cite{Bao2012}. This system pre-computes and extracts local experts for each
venue category in a city. When recommending a venue, the system takes into
account the preferences of the user and the ratings of similar local experts.

The original LBPA model uses location history data from \emph{FourSquare}
to derive a location-based social network and location category
hierarchy \cite{Bao2012}. Since our data comes from Yelp, we don't know how many times a user
has visited a particular business and thus had to generalize the model to
count total number of reviews in a particular category. To begin our analysis,
we created a Yelp category hierarchy similar to that of the FourSquare-based
model. Yelp provides a loosely organized file of venue categories. Using this, we
placed all Yelp categories into a tree-like data structure. This data structure is able
to capture the varying specificity inherent to business categories. The tree
is comprised of 14 parent categories, each with a depth between zero and
three. For example, the parent category ``Food'' increases specificity as it
goes from ``Restaurants'' to ``Brazilian'' to ``Central Brazilian''. This
granularity is key in accurately measuring similarity between users, with
deeper nodes of the category tree receiving a greater weight in the computation.

To find the set of users that the model employs as \emph{local experts}, we had to
first find each user's areas of expertise, a function of each individual user's
total number of visits to a particular category relative to the total number of
visits to that category from all users. This differs from the original
implementation of LBPA. The original system uses an iterative algorithm in which
a user's expertise is based on the number of high quality venues they visit in a
particular category, and in turn venue quality is based on the number of expert
users who visit it.

The next step is to project a user's history into the category structure and
compute a preference profile for each user. For each category $c$ in which a
user $u$ has a review, the preference weight $w(u,c)$ represents the fraction of user $u$'s reviews that
relate to category $c$, scaled by the rarity of category $c$ among all users'
reviews as shown in equation \ref{weightPR}.
\begin{equation}
\label{weightPR}
    w(u,c) = \frac{n(u,c)}{t(u)} \times \lg \frac{U}{v(c)}
\end{equation}

Here $n(u,c)$ is the number of reviews user $u$ has written in category $c$,
$t(u)$ is the total number of reviews $u$ has written, $U$ is the total number
of users, and $v(c)$ is the total number of users who have reviewed in
category $c$. A given user's preference profile consists of a tree like that of the
main category hierarchy, where each node additionally contains the user's
preference weight for that category.

Once a preference profile is generated for each user, the similarity between any
two users can be determined. Similarity is first computed on each distinct level
$\ell$ of the category tree. The ``level similarity'' between two users on
$\ell$ is measured by the total overlap between their preferences on that level:

\begin{equation}
    LevelSim(u,u',\ell) = \sum_{c \in C^\ell} \min \del{w(u,c),w(u',c)}
\end{equation}
where $w(u,c)$ and $w(u',c)$ represent the two users' preference weights of an
overlapped node $c$. The next similarity comparison is the entropy of each
level, which captures the diversity of a user's preferences. For a given user
$u$ and tree level $\ell$, the entropy is calculated by:
\begin{equation}
    H(u,\ell) = - \sum_{c \in C^\ell} P(u,c) \times \log_2 P(u,c)
\end{equation}
where $P(u,c)$ represents the probability that user $u$ has visited category
$c$. We measure $P(u,c)$ as the user's preference weight for $c$ divided by the
sum of all of $u$'s preference weights. The entropy is important to evaluate how
important certain categories are to a user. For example, we must distinguish a
user who has a focused interest in an area from a user who has interest in the a
wide variety of other areas.

The model calculates the overall user similarity using the entropy and the level
similarity. The similarity between users $u$ and $u'$ is:
\begin{equation}
    Sim(u,u') = \sum_{\ell=0}^{\abs{\ell}} \beta(\ell) \times
        \frac{LevelSim(u,u',\ell)}{1+\abs{H(u,\ell)-H(u',\ell)}}
\end{equation}
where $\beta(\ell)$ weights deeper levels of the category hierarchy more
heavily. Choices of $\beta$ determine the relative significance of level-
difference specificity. Here, we choose to use $\beta(\ell) = 2^{\ell + 1}$ as
recommended in \cite{Bao2012}. The similarity between two users is high if they
share category nodes with high preference ratings, located in lower levels of their
weighted category hierarchy tree, and they have similar entropy on each level.

Finally, the predicted rating $r(u,b)$ of a user $u$ to a business $b$ is
estimated by a linear combination of local experts' ratings for that business,
where the coefficients are the normalized similarity of each expert to user $u$,
that is,
\begin{equation}
    r(u,b) = \sum_{u' \in E} s(u,u') \cdot r(u',b)
\end{equation}
where $E$ is the set of local experts for user $u$ and $s(u,u')$ is the
normalized similarity between users $u$ and $u'$. This rating inference has the
limitation of restricting the local experts to those users who have actually
reviewed business $b$. This may result in local experts that are not as similar
to $u$ as we would like.

\chapter{Results} \label{ch:results}

For the present work the data used was restricted to the reviews for one city:
Charlotte, NC. Although restricting the data to one city will take away from
the geographic diverstiy of the dataset, we prototype and test more quickly
and more easily when dealing with a smaller amount of ratings. Charlotte was
chosen in particular because its review count, about 95,000, was small enough
to handle, but not so small as to inhibit the effectiveness of the models. In
comparison, the two largest cities comprise 1.2 million of the 1.6 million
total reviews in the dataset. Using this data, the considered models were
tested under four conditions:

\begin{enumerate}[a), noitemsep]
\item all users and all businesses
\item users with at least 3 reviews and businesses with at least 5
    reviews
\item all users, and only businesses that listed ``Food'' or ``Restaurant''
    among their categories
\item users with at least 3 reviews, and businesses with at least
    5 reviews that also listed ``Food'' or ``Restaurant'' among their categories
\end{enumerate}

For each condition we calculate the root-mean-square error. This is an
estimate of the model accuracy based on cross-validation with 80\% of the data
used for training and 20\% for testing. We also calculate the root-mean-square
error when predictions are rounded and bounded to integer values between 1 and
5, i.e. the nearest possible star rating.

\section{Basic Offsets}

In \Cref{fig:base-error-hists} we can see that the baseline model tends to
make predictions within one star of the true value.
\Cref{tab:base-error} shows that our prediction errors are smaller
when dealing only with users and businesses with at least three or five reviews
respectively. This leads us to believe that these users and businesses contain
more signal which causes them to be more predictable in the context of the model.

\begin{table}[H] \begin{center}
    \begin{tabular}{lcc} \toprule
        Condition & RMSE & Rounded \& Bounded RMSE \\ \midrule
        All Users \& Businesses & $1.234 \pm 0.002$ & $1.250 \pm 0.003$ \\
        Review Count Limited & $1.109 \pm 0.004$ & $1.136 \pm 0.004$ \\
        Category Limited & $1.209 \pm 0.001$ & $1.228 \pm 0.001$ \\
        Category \& Review Count Limited & $1.107 \pm 0.002$ & $1.139 \pm 0.003$ \\ \bottomrule
    \end{tabular}
    \caption{Root-Mean-Square Errors for the Basic Offsets Model}
    \label{tab:base-error}
\end{center} \end{table}

\begin{figure}[H] \begin{center}
    \begin{subfigure}{0.45\textwidth}
        \caption{All Users and Businesses}
        \includegraphics[height=1.5in]{Graphics/char_baseline_hist_all00.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/char_baseline_hist_all35.pdf}
    \end{subfigure} \\
    \begin{subfigure}{0.45\textwidth}
        \caption{Category Limited}
        \includegraphics[height=1.5in]{Graphics/char_baseline_hist_rest00.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Category and Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/char_baseline_hist_rest35.pdf}
    \end{subfigure}
    \caption{Histogram of Basic Offsets Model Errors}
    \label{fig:base-error-hists}
\end{center} \end{figure}

\section{Latent Factors}

The latent factors model performs significantly better when the data is
restricted to users and businesses with at least three or five reviews
respectively. \Cref{tab:latent-error} shows that the root mean square error is
far smaller when review count is limited. \Cref{fig:latent-error-hists} shows
that the ``tail'' of these predictions is narrower in the restricted model.

This is probably due to what is known as the \emph{cold start} problem. Matrix
factorization techniques are generally ill-equipped to make predictions when
data is very sparse \cite{Koren2009MatrixSystems}. That is, when a user or
business has few reviews, it is difficult for a latent factors model to make
predictions about that user or business.

% Currently the restricted latent factors model performs at about the same level
% as the restricted basic offsets model. However, our latent factors model's performance
% is on track with previous application of latent factors to Yelp data
% \cite{McAuley2013HiddenText}.

Currently the restricted latent factors model performs at about the same level
as the restricted basic offsets model. This was unexpected, as
latent factors models generally perform better than baseline models. It is
possible that if we further restrict the data, we will see an improvement to
the latent factors model not matched by the baseline.

\begin{table}[H] \begin{center}
    \begin{tabular}{lcc} \toprule
        Condition & RMSE & Rounded \& Bounded RMSE \\ \midrule
        All Users \& Businesses & $1.562 \pm 0.005$ & $1.578 \pm 0.005$ \\
        Review Count Limited & $1.125 \pm 0.002$ & $1.154 \pm 0.003$ \\
        Category Limited & $1.566 \pm 0.005$ & $1.578 \pm 0.006$ \\
        Category \& Review Count Limited & $1.122 \pm 0.002$ & $1.153 \pm 0.002$ \\ \bottomrule
    \end{tabular}
    \caption{Root-Mean-Square Errors for the Latent Factors Model}
    \label{tab:latent-error}
\end{center} \end{table}

\begin{figure}[H] \begin{center}
    \begin{subfigure}{0.45\textwidth}
        \caption{All Users and Businesses}
        \includegraphics[height=1.5in]{Graphics/char_latent_hist_all00.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/char_latent_hist_all35.pdf}
    \end{subfigure} \\
    \begin{subfigure}{0.45\textwidth}
        \caption{Category Limited}
        \includegraphics[height=1.5in]{Graphics/char_latent_hist_rest00.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Category and Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/char_latent_hist_rest35.pdf}
    \end{subfigure}
    \caption{Histogram of Latent Factors Model Errors}
    \label{fig:latent-error-hists}
\end{center} \end{figure}

\section{Location-Based Preference Aware Model}

The Location-Based Preference Aware model has similar results to the baseline model. \Cref{tab:lbpa-error} and \Cref{fig:lbpa-error-hists} show results for models both restricted and not restricted by review count. We do not restrict by category because this model incorporates venue type into its rating predictions.

Though this model does not improve on the baseline results for Charlotte data, we expect that it will do better than both the baseline and latent factors models in making predictions for cross-city users.

\begin{table}[h] \centering
    \begin{tabular}{ccc} \toprule
    Condition & RMSE & Rounded and Bounded RMSE \\ \midrule
    All Users \& Businesses & $1.289 \pm 0.003$ & $1.293 \pm 0.002$ \\
    Review Count Limited & $1.120 \pm 0.002$ & $1.154 \pm 0.001$ \\ \bottomrule
    \end{tabular}
    \caption{Root-Mean-Square Errors for the
        Location-Based Preference Aware Model}
    \label{tab:lbpa-error}
\end{table}

\begin{figure}[h] \centering
    \begin{subfigure}{0.45\textwidth}
        \caption{All Users and Businesses}
        \includegraphics[height=1.5in]{Graphics/char_lbpa_hist_all00.pdf}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \caption{Review Count Limited}
        \includegraphics[height=1.5in]{Graphics/char_lbpa_hist_all35.pdf}
    \end{subfigure}
    \caption{Histogram of Location-Based Preference Aware Model Errors}
    \label{fig:lbpa-error-hists}
\end{figure}


\section{Basic Offsets, Latent Factors, and LBPA Compared}

The root-mean-square error results for the basic offset, latent factors, and
LBPA models under the All Users \& All Businesses condition and under the
Review Count Limited condition are collected in \Cref{tab:models-compared}. We
see that the basic offsets model slightly outperforms the LBPA model, which in
turn significantly outperforms the latent factors model, when all users and
all businesses are considered.

As earlier mentioned, this comes as little suprise; the basic offsets model is
conservative in its estimates, LBPA assigns big weights to users with greater
numbers of reviews, and the latent factors model is known to have difficulty
with the cold start problem. When we limit consideration so as only to include
users with three or more reviews and businesses with five or more reviews, all
models perform comparably; the basic offsets model improves slightly, the
latent factors model warms up, and LBPA shows a slight improvement as
well.

It is when we go forward to include multiple locations that we expect LBPA to
shine. Within the context that we have so far tested, where the reviews are
limited to only those in Charlotte, the LBPA model uses location no more than
either of the two baseline models do. As soon as we add our second city to our
testing, we expect that the LBPA model will take advantage of geographic data
to improve predictions.

\begin{table}[h] \centering
    \begin{tabular}{ccc} \toprule
    Model & RMSE (All Users \& All Businesses) & RMSE (Review Count Limited) \\ \midrule
    Basic Offsets & $1.234 \pm 0.002$ & $1.109 \pm 0.004$ \\
    Latent Factors & $1.562 \pm 0.005$ & $1.125 \pm 0.002$ \\
    LBPA & $1.289 \pm 0.003$ & $1.120 \pm 0.002$ \\ \bottomrule
    \end{tabular}
    \caption{Basic Offsets, Latent Factors, and LBPA Compared}
    \label{tab:models-compared}
\end{table}

\Cref{fig:error-mag-plots} shows the error magnitude both of users and businesses with respect to the number of reviews they have. As we would expect, the average error is significantly smaller for users and businesses with more reviews.

These graphs also illustrate the cold start problem. Specially, the latent factors model performs poorly when dealing with users and businesses with few reviews, but it recovers and performs almost equally to the offsets model when dealing with users and businesses with more reviews.

\begin{figure}[h]
    \begin{subfigure}{.5\textwidth}
        \caption{Basic Offsets, Businesses}
        \includegraphics[height=2in]{Graphics/error_trend_BO_bus}
    \end{subfigure}
    \begin{subfigure}{.4\textwidth}
        \caption{Basic Offsets, Users}
        \includegraphics[height=2in]{Graphics/error_trend_BO_user}
    \end{subfigure} \\
    \begin{subfigure}{.5\textwidth}
        \caption{Latent Factors, Businesses}
        \includegraphics[height=2in]{Graphics/error_trend_LF_bus}
    \end{subfigure}
    \begin{subfigure}{.4\textwidth}
        \caption{Latent Factors, Users}
        \includegraphics[height=2in]{Graphics/error_trend_LF_user}
    \end{subfigure}
    \caption{Error magnitude per number of reviews.}
    \label{fig:error-mag-plots}
\end{figure}
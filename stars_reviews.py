# -*- coding: utf-8 -*-
"""
author: Chelsea Chandler

date: 2015.06.23

description: 
This program loads JSON data from the Yelp! Challenge Dataset. It analyzes the 
user JSON file and creates a dictionary that relates a user's average stars to
their total number of reviews written. 
**IN PROGRESS**
"""

import json
import itertools
import matplotlib.pyplot as plt

user_file = '/home/cchandler/Documents/yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_user.json'

data = []
with open(user_file) as data_file:
    for line in data_file:
        data.append(json.loads(line))

#Get a list of average stars left per user
av_stars = []
for s in data:
    av_stars.append(s['average_stars'])

    #Get a list of the total number of reviews per user
num_reviews = []
for r in data:
    num_reviews.append(r['review_count'])

#Create a dictionary that relates stars to reviews
stars_reviews = dict(itertools.izip(av_stars, num_reviews))

#Plot average stars vs number of reviews eliminating one outlier with 8843 reviews
plt.xlim(min(av_stars), max(av_stars))
plt.ylim(0, 5000)
plt.plot(av_stars, num_reviews, '.')
plt.show()

#Get list of years elite
elite_years = []
for e in data:
    elite_years.append(e['elite'])

num_elite_years = []
for x in elite_years:
    num_elite_years.append(len(x))

    #Create a dictionary that relates stars to elite
stars_elite_yrs = dict(itertools.izip(av_stars, num_elite_years))

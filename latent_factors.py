# -*- coding: utf-8 -*-
"""
latent factors method using bfgs 
"""
from scipy import optimize
import Queue, threading
import numpy as np


def matrix_fact(R, P, Q, lam):
    Y = np.empty(R.shape)  #create empty matrix of the same dimensions of R
    for u in range(P.shape[1]):  #loop through number of columns of P (aka rows in R)
        for b in range(Q.shape[1]):  #loop through number of columns of Q (aka cols in R)
            #set each space of the new matrix Y to be (that formula)
            Y[u][b] = pow(R[u][b] - np.dot(Q[:,b],P[:,u]),2) + \
                lam * (np.dot(Q[:,b],Q[:,b]) + np.dot(P[:,u],P[:,u]))
    return Y.sum()


    # helper function to parallelize BFGS execution
    # create a new thread that executes target for each argument in args
def parallelize(target, args):

    #create a queue
    result = Queue.Queue()

    #fill queue with tasks
    def fill_queue(*args):
        result.put(target(*args))

    #create a list of threads corresponding to the task list
    threads = [threading.Thread(target=fill_queue, args=arg) for arg in args]

    for t in threads:
        t.start()
        t.join()

    return result


if __name__ == "__main__":
    R = [
        [5, 3, 0, 1],
        [4, 0, 0, 1],
        [1, 1, 0, 5],
        [1, 0, 0, 4],
        [0, 1, 5, 4],
    ]
    R = np.array(R)
    N = len(R)
    M = len(R[0])
    K = 2
    P = np.random.rand(K, N)
    Q = np.random.rand(K, M)
    lam = 1
    print matrix_fact(R, P, Q, lam)

    #how do we use the optimization with the function that we have?
    optimize.fmin_l_bfgs_b(matrix_fact(R, P, Q, lam))
#optimize.fmin_bfgs(matrix_fact(R,P,Q,lam))

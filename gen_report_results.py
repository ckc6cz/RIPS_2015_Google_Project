import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
from itertools import chain, product
import shelve
from latexify import latexify

# import pandas as pd


def get_errors(options, deltas=None, test=None, train=None):
    """ helper to error_report_and_hist """
    u_revmin, b_revmin = options['revmin'] if 'revmin' in options else (0, 0)
    if u_revmin and b_revmin:
        print 'review limiting to {}, {}'.format(u_revmin, b_revmin)

    if deltas:
        # input was merely the delta pickle
        uid_count = Counter(uid for (uid, _) in deltas)
        bid_count = Counter(bid for (_, bid) in deltas)
    elif test and train:
        uid_count = Counter(uid
                            for (uid, _) in chain(test.keys(), train.keys()))
        bid_count = Counter(bid
                            for (_, bid) in chain(test.keys(), train.keys()))
        deltas = {
            (uid, bid): guess - train[(uid, bid)]
            for (uid, bid), guess in test.iteritems()
        }
    else:
        raise TypeError(None, 'missing the data')

    revlimited = {
        (uid, bid): star
        for (uid, bid), star in deltas.iteritems()
        if uid_count[uid] >= u_revmin and bid_count[bid] >= b_revmin
    }
    errors = np.array([err for err in revlimited.itervalues()])
    rmse = np.sqrt(np.mean(errors ** 2))
    print 'rmse: {:f}'.format(rmse)

    # if summary:
    #     print pd.DataFrame(np.abs(errors)).describe().transpose()

    return errors


def make_hist(errors, options=None):
    # initialize some sane defaults
    defaults = {
        'fname': None,
        'title': 'Histogram of Errors',
        'xlab': 'Error Magnitude',
        'ylab': 'Frequency',
        'summary': False,
        'normed': True,
        'ymax': None,
        'binwidth': 0.5,
        'color': 'c',
        'fontsize': 18
    }

    # overwrite defaults if specified in options
    if options:
        for kw, val in options.iteritems():
            defaults[kw] = val

    if not defaults['fname']:
        defaults['fname'] = raw_input(
            'No filename specified. '
            'If you wish to save a figure, specify a filename.\n')

    if defaults['fname']:
        plt.clf()
        latexify(fontsize=defaults['fontsize'])
        plt.hist(np.abs(errors),
                 bins=np.arange(0, 5, defaults['binwidth']),
                 normed=defaults['normed'],
                 color=defaults['color'])
        plt.title(defaults['title'])
        plt.xlabel(defaults['xlab'])
        plt.ylabel(defaults['ylab'])
        if defaults['ymax']:
            plt.ylim((0, defaults['ymax']))
        plt.yticks(np.arange(0,1.1,0.2))
        plt.xticks(range(6))
        plt.tight_layout()
        print 'saving figure to {}'.format(defaults['fname'])
        plt.savefig(defaults['fname'], dpi=300)
        plt.clf()


def report(*args, **kwargs):
    """
    report root-mean-square error for the data provided
    params:
        train: array, the true values
        test: array, the estimated values
        deltas: test-train
        fname: string, the output filename of the histogram
                    if left empty, no histogram will be made
        title: string, the title for the histogram
        xlab: string, the label for the histogram's x axis
        ylab: string, the label for the histogram's y axis
        summary: bool, if true prints summary statistics of errors
    """
    if len(args) == 1:
        deltas, = args
        errors = get_errors(kwargs, deltas=deltas)
    elif len(args) == 2:
        test, train = args
        errors = get_errors(kwargs, test=test, train=train)
    else:
        raise TypeError(None, 'missing the data')

    # check to see if the input was merely the delta pickle
    make_hist(errors, kwargs)

    if 'revmin' not in kwargs:
        print 'this time with review limiting...'
        kwargs['revmin'] = (3, 5)
        if len(args) == 1:
            errors = get_errors(kwargs, deltas=deltas)
        else:
            errors = get_errors(kwargs, test=test, train=train)
        kwargs['fname'] = kwargs['fname'].replace('00', '35')
        make_hist(errors, kwargs)


def make_comp_hist(errors, options=None):
    # initialize some sane defaults
    defaults = {
        'fname': None,
        'title': 'Histogram of Errors',
        'xlab': 'Error Magnitude',
        'ylab': 'Frequency',
        'summary': False,
        'normed': True,
        'ymax': None,
        'binwidth': 0.5,
        'color': None,
        'fontsize': 36
    }

    # overwrite defaults if specified in options
    if options:
        for kw, val in options.iteritems():
            defaults[kw] = val

    if not defaults['fname']:
        defaults['fname'] = raw_input(
            'No filename specified. '
            'If you wish to save a figure, specify a filename.\n')

    if defaults['fname']:
        plt.clf()
        latexify(fontsize=defaults['fontsize'])
        errors = [np.abs(error) for error in errors]
        plt.hist(errors,
                 bins=np.arange(0, 5, defaults['binwidth']),
                 normed=defaults['normed'],
                 color=defaults['color'])
        if defaults['legend']:
            plt.legend(defaults['legend'])
        plt.title(defaults['title'])
        plt.xlabel(defaults['xlab'])
        plt.ylabel(defaults['ylab'])
        if defaults['ymax']:
            plt.ylim((0, defaults['ymax']))
        plt.yticks(np.arange(0,1.1,0.2))
        plt.xticks(range(6))
        plt.tight_layout()
        print 'saving figure to {}'.format(defaults['fname'])
        plt.savefig(defaults['fname'], dpi=300)
        plt.clf()


def comp_report(*args, **kwargs):
    """
    report root-mean-square error for the data provided
    params:
        train: array, the true values
        test: array, the estimated values
        deltas: test-train
        fname: string, the output filename of the histogram
                    if left empty, no histogram will be made
        title: string, the title for the histogram
        xlab: string, the label for the histogram's x axis
        ylab: string, the label for the histogram's y axis
        summary: bool, if true prints summary statistics of errors
    """
    errors = [get_errors(kwargs, deltas=arg) for arg in args]
    make_comp_hist(errors, kwargs)

    if 'revmin' not in kwargs:
        print 'this time with review limiting...'
        kwargs['revmin'] = (3, 5)
        errors = [get_errors(kwargs, deltas=arg) for arg in args]
        kwargs['fname'] = kwargs['fname'].replace('00', '35')
        make_comp_hist(errors, kwargs)


if __name__ == '__main__':
    results = shelve.open('results.shelf')
    design_vars = shelve.open('design_vars.shelf')
    model_colors = design_vars['color']
    models = results.keys()

    for (model1, model2) in product(models, models):
        legend = (model1, model2)
        color = (model_colors[model1], model_colors[model2])
        model1_fname = '_'.join([s.lower() for s in model1.split()])
        model2_fname = '_'.join([s.lower() for s in model2.split()])

        if model1 == model2:
            print 'Generating a report for {}'.format(model1)
            model_fname = '_'.join([s.lower() for s in model1.split()])
            fname = 'FinalReport/Graphics/hist_{}_char00_BIG.pdf'.format(
                model_fname)
            report(results[model1], fname=fname, legend=model1, color=color[0])
        else:
            print 'Generating a report for {} and {}'.format(model1, model2)
            fname = 'FinalReport/Graphics/hist_comp_{}_{}_char00_BIG.pdf'.format(
                model1_fname, model2_fname)
            comp_report(results[model1], results[model2],
                        fname=fname,
                        legend=legend,
                        color=color)

    model1, model2, model3 = 'Basic Offsets', 'Category Offsets', 'HFT'
    legend = (model1, model2, model3)
    color = (model_colors[model1], model_colors[model2], model_colors[model3])
    model1_fname = '_'.join([s.lower() for s in model1.split()])
    model2_fname = '_'.join([s.lower() for s in model2.split()])
    model3_fname = '_'.join([s.lower() for s in model3.split()])
    fname = 'FinalReport/Graphics/hist_comp_{}_{}_{}_char00_BIG.pdf'.format(
        model1_fname, model2_fname, model3_fname)
    comp_report(results[model1], results[model2], results[model3],
                fname=fname,
                legend=legend,
                color=color)

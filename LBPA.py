# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 08:54:10 2015

@author: mribero

LBPA complete. Given a test set (a nx3 matrix where n is the number of tests and the columns are (u_id - b_id - realRating ))
it predicts the rating for those users to businesses.

"""
import numpy as np
import pandas as pd
from pymongo import MongoClient
import pickle
import pdb
from ratingInference import computeRating
from PrefAwareCandidates import localExperts, localExperts2
from UserAndTree import make_all_user_pref_trees
from char_make_bid_uid_dict import char_make_bid_uid_dict

#import csv

##LBPA
#Input:
#test: test set. Dictionary where the keys are tuples(user id, business id) and the value is the real rating.
#train: test set. Dictionary where the keys are tuples(user id, business id) and the value is the real rating.
#preferences: dictionary where the keys are the users ids and the values their preference trees
#expertise: dictionary where the keys are the categories and the values a dictionary with each user's expertise in that category

#returns the predicted rating weighted by similarity and the predicted rating using the most similar user


def falsa(test, preferences, expertise, train, dictionary):
    pdb.set_trace()
    res = lbpa(test, preferences, expertise, train, dictionary)
    return res


def lbpa(test, preferences, expertise, train, dictionary):
    #Create dictionaries for predictions
    predicted = {}
    predictedMax = {}
    train_users = [key[0] for key in train.keys()]
    #for each rating
    for (u, b) in test.keys():
        #Create the user preference tree
        uTree = preferences[u]
        #find local experts for the users
        #pdb.set_trace()
        potExperts = dictionary[b].intersection(train_users)
        #pdb.set_trace()
        expS = localExperts2(uTree, expertise, potExperts)
        if u in expS:
            expS.remove(u)
        else:
            continue
        if len(expS) == 0:
            predicted[(u, b)] = 0
            predictedMax[(u, b)] = 0
        else:
            #find preferences trees for the experts
            expe = {v: preferences[v] for v in expS}
            #find the ratings
            ratings = {v: train[(v, b)] for v in expS}
            #compute predictions
            predicted[(u, b)], predictedMax[(u, b)] = computeRating(
                uTree, expe, ratings)
    return predicted, predictedMax

#################################3
#u'1R6wDvXIzmofopYllK_fpQ', u'va8gX25HcEcnHnLgy1j_tA'

###TEST####
client = MongoClient()
db = client.char

char_rev = db.rev
data = {}
for r in char_rev.find():
    u = r['user_id']
    b = r['business_id']
    data[(u, b)] = r['stars']

#preferences = make_all_user_pref_trees()
with open('preferences.pickle', 'r') as f:
    preferences = pickle.load(f)

#expertise = category_experts()
with open('cat_experts.pickle', 'r') as f:
    expertise = pickle.load(f)

bid_uid_dict = char_make_bid_uid_dict()

test = {v: data[v] for v in data.keys()[:142]}
train = {v: data[v] for v in data.keys()[142:]}
pred, predM = lbpa(test, preferences, expertise, train, bid_uid_dict)


def crossVal(data, preferences, expertise, bid_uid_dict):
    from sklearn import cross_validation
    import numpy as np

    n = len(data)
    #Create folds for crossVal
    kf = cross_validation.KFold(n, n_folds=5)
    pdb.set_trace()
    totalRes = {}
    totalResM = {}
    for train_index, test_index in kf:
        train_keys = np.array(data.keys())[train_index]
        test_keys = np.array(data.keys())[test_index]
        train = {tuple(key): data[tuple(key)] for key in train_keys}
        test = {tuple(key): data[tuple(key)] for key in test_keys}
        results, resultsMax = lbpa(test, preferences, expertise, train,
                                   bid_uid_dict)
        totalRes.update(results)
        totalResM.update(resultsMax)
    return totalRes, totalResM


pred, predM = crossVal(data, preferences, expertise, bid_uid_dict)

###############################
#Write results

results = [[data[u], pred[u], predM[u]] for u in data.keys()]

with open('resultsCharlotteLBPA.csv', 'wb') as f:
    w = csv.writer(f, quoting=csv.QUOTE_ALL)
    w.writerow(results)

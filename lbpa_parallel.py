""" lbpa module """

import parmap  # to parallelize lbpa
import numpy as np
import pandas as pd
import sys  # to process command line arguments
import cPickle as pickle  # to save results

from pymongo import MongoClient  # to access the database
from collections import defaultdict, Counter
from operator import add
from itertools import chain
from sklearn import cross_validation

from HierarchyCreation import make_category_tree


def get_user_depth(user):
    """
    :param user: a (category, category score) tree for user
    :type user: {(cat, score): {<recursive>}}

    :return: the height of a user-tree
    :rtype: int
    """
    if user:
        return 1 + max(get_user_depth(val) for _, val in user.iteritems())
    return 0


def get_dict_at_level(user, level):
    """
    compresses the nodes at a level of user's tree into a dictionary

    :param user: tree for user
    :type user: {(cat, score): {<recursive>}}

    :param level: the requested depth level
    :type level: int

    :return: set of categories for user at depth-level level,
            dictionary of categories to scores for user at depth-level level
    :rtype: set, {cat: cat-score}
    """
    # case where the level being asked is deeper than the user's tree
    if level >= get_user_depth(user):
        return set(), {}

    # top-level case
    if not level:
        cats = {cat for cat, score in user.keys()}
        cat_scores = {cat: score for cat, score in user.keys()}
        return cats, cat_scores

    # recursive case
    cats = set()
    cat_scores = {}
    for key in user:
        cat, cat_s = get_dict_at_level(user[key], level - 1)
        cats = cats.union(cat)
        cat_scores.update(cat_s)

    return cats, cat_scores


def level_similarity(userA, userB):
    '''
    computes the similarity of two users at each level of a category hierarchy.
    to be used in computing the total similarity between two users.

    :param userA: nested dictionary of user preferences
    :param userB: nested dictionary of user preferences
    :type userA: {(cat, score): {<recursive>}}
    :type userB: {(cat, score): {<recursive>}}

    :returns: similarity values ordered from top to bottom
    :rtype: list
    '''
    similarity = np.zeros(max(get_user_depth(userA), get_user_depth(userB)))
    for level in xrange(len(similarity)):
        A_cat, A_cat_scores = get_dict_at_level(userA, level)
        _, B_cat_scores = get_dict_at_level(userB, level)
        for cat in A_cat:
            try:
                similarity[level] += min(A_cat_scores[cat], B_cat_scores[cat])
            except KeyError:
                # case where cat in A_cat but not in B_cat
                continue
    return similarity


def level_entropy(user):
    '''
    Computes the entropy of a user's preferences at each level of a category
    hierarchy. To be used in computing the total similarity between two users.

    :param user: nested dictionary of user preferences
    :type user: {(cat, score): {<recursive>}}

    :returns: entropy values, ordered from top to bottom
    :rtype: list
    '''
    entropy = np.zeros(get_user_depth(user))
    for level in xrange(len(entropy)):
        _, scores = get_dict_at_level(user, level)
        score_total = np.sum(scores.values())
        prob_list = [score / score_total for score in scores.values()]
        entropy[level] -= np.sum(prob_list * np.log2(prob_list))
    return entropy


def user_similarity(userA, userB):
    '''
    :param userA: nested dictionary of user preferences
    :param userB: nested dictionary of user preferences
    :type userA: {(cat, score): {<recursive>}}
    :type userB: {(cat, score): {<recursive>}}

    :returns: total similarity between two users
    :rtype: float
    '''
    # get similarity at each level as a list
    sim_at_level = level_similarity(userA, userB)

    # get entropy at each level as a list
    entropyA = level_entropy(userA)
    entropyB = level_entropy(userB)

    # compute total similarity (see Eq. 8 in LBPA paper)
    sim = sum(
        2 ** (1 + level) * (
            sim_at_level[level] / (1 + abs(entropyA[level] - entropyB[level])))
        for level in xrange(min(get_user_depth(userA), get_user_depth(userB))))
    return sim


def insert_leaf(tree, cat, uid, ucp, ctp):
    """
    :param tree: user-profile tree
    :type tree: {(cat, score): {<recursive>}}
    :param cat: category to be inserted
    :type cat: str
    :param uid: user_id
    :type uid: str
    :param ucp: category preferences dictionary
    :type ucp: {uid: {category: score}}
    :param ctp: category to parent dictionary
    :type ctp: {child category: parent category}

    :returns: tree for a user with cat properly inserted
    :rtype: {(cat, score): {<recursive>}}
    """
    subtree = tree
    # if the category has a parent category
    if cat in ctp:
        # initialize its ancestry_chain to begin with cat
        ancestry_chain = [ctp[cat]]

        # so long as the a_chain's final member has a parent, append its parent
        while ancestry_chain[-1] in ctp:
            ancestry_chain.append(ctp[ancestry_chain[-1]])

        # so long as the ancestry_chain has members, properly tree-ify them
        while ancestry_chain:
            parent = ancestry_chain.pop()
            pval = ucp[uid][parent]
            subtree = subtree[(parent, pval)]

    # finally add the category with its score inserted
    subtree[(cat, ucp[uid][cat])] = {}

    return tree


def create_user_profile(uid_bid, bid_cat, cat_ancestry):
    """
    relate users to the categories for which they review

    :param uid_bid: uid to bid dictionary
    :type uid_bid: {uid: [bids]}
    :param bid_cat: bid to cat dictionary
    :type bid_cat: {bid: [categories]}
    :param cat_ancestry: cat to ancestors dictionary
    :type cat_ancestry: {cat: set(cats)}

    :returns: user to category counts Series
    :rtype: {user: pd.Series(counts, index = cat)}
    """
    uid_cat = defaultdict(list)
    for uid, bids in uid_bid.iteritems():
        for bid in bids:
            set_to_inc = set(bid_cat[bid])
            for cat in set_to_inc:
                set_to_inc = set_to_inc.union(cat_ancestry[cat])
            uid_cat[uid] += set_to_inc

    #dictionary that relates user to a dictionary of their categories with counts of each
    for uid, cats in uid_cat.iteritems():
        uid_cat[uid] = Counter(cat for cat in cats)

    #dictionary with key: user id and value: vector that counts category visits
    user_prof = {}
    for user, cat_to_counts in uid_cat.iteritems():
        user_prof[user] = pd.Series(cat_to_counts)
        user_prof[user].sort()

    return user_prof


def make_uid_to_cat_to_count(uid_bid, bid_cat):
    """
    make a uid to category to number of reviews in a category dictionary

    :param uid_bid: uid to bid dictionary
    :type uid_bid: {uid: [bids]}
    :param bid_cat: bid to cat dictionary
    :type bid_cat: {bid: [categories]}

    :rtype: {uid: {cat: count}}
    """
    out = defaultdict(Counter)
    for uid, bids in uid_bid.iteritems():
        for bid in bids:
            set_to_inc = set(bid_cat[bid])
            set_to_inc = set_to_inc.union(set(chain.from_iterable(
                cat_ancestry[cat] for cat in set_to_inc)))
            out[uid] += Counter(cat for cat in set_to_inc)

    return out


def compute_user_preference(uid_bid, bid_cat):
    """
    :param uid_bid: uid to bid dictionary
    :type uid_bid: {uid: [bids]}
    :param bid_cat: bid to cat dictionary
    :type bid_cat: {bid: [categories]}

    :returns: user category preference profile
    :rtype: {user: {cat: preference}}
    """
    user_counts = make_uid_to_cat_to_count(uid_bid, bid_cat)
    user_profiles = make_uid_to_cat_to_count(uid_bid, bid_cat)

    user_to_num_rev = {uid: len(uid_bid[uid]) for uid in uid_bid}
    num_users = len(user_counts)

    cat_to_count = defaultdict(int)
    for uid in user_counts:
        for cat in user_counts[uid]:
            cat_to_count[cat] += 1

    for uid in user_profiles:
        for cat in user_profiles[uid]:
            user_profiles[uid][cat] = \
                user_counts[uid][cat] / float(user_to_num_rev[uid]) \
                * np.log2(num_users / float(cat_to_count[cat]))

    return user_profiles


def get_cat_depth(cat, hierarchy):
    """
    finds the depth of cat in hierarchy

    :param cat: category to be found
    :type cat: str
    :param hierarchy: tree in which to find cat
    :type hierarchy: {parent_cat: {child_cat: <recursive>}}

    :returns: the depth in the hierarchy at which cat appears
        0 if toplevel, np.nan if hierarchy doesn't contain cat
    :rtype int/float
    """
    if not hierarchy:
        return np.nan
    if cat in hierarchy:
        return 0
    return 1 + np.nanmin([get_cat_depth(cat, hierarchy[root]) \
        for root in hierarchy])


def tree_crawler(tree):
    """
    :param tree: tree to be crawled
    :type tree: {key: {value: <recursive>}}

    :returns: set containing the keys of all levels of tree
    :rtype: set
    """
    if not tree:
        return set()

    out = set(tree.keys())
    return out.union(*[tree_crawler(tree[key]) for key in tree])


def make_child_parent_dict(tree):
    """
    :param tree: tree for which children are to be traced to parents
    :type tree: {parent: {child: <recursive>}}

    :returns: child to parent dictionary
    :rtype: {child: parent}
    """
    out = {}
    for parent, child_tree in tree.iteritems():
        out.update({child: parent for child in child_tree})
        out.update(make_child_parent_dict(child_tree))
    return out


def make_cat_ancestry_dict(cat_hierarchy):
    """
    :returns: dictionary of categories to a set of parent categories
    :rtype: {cat: {parent-cats}}
    """
    cat_set = tree_crawler(cat_hierarchy)
    cat_to_parent = make_child_parent_dict(cat_hierarchy)

    out = {cat: set() for cat in cat_set}
    for cat in out:
        if cat in cat_to_parent:
            lineage = [cat_to_parent[cat]]
            while lineage[-1] in cat_to_parent:
                lineage.append(cat_to_parent[lineage[-1]])
            for ancestor in lineage:
                out[cat].add(ancestor)
    return out


def make_all_user_pref_trees(uid_bid, bid_cat, cat_ancestry, cat_hierarchy):
    """
    :returns: dictionary of users to their preference trees
    :rtype: {uid: {(cat, score): <recursive>}}
    """
    user_prof = create_user_profile(uid_bid, bid_cat, cat_ancestry)
    cat_set = tree_crawler(cat_hierarchy)
    cat_depth = {cat: get_cat_depth(cat, cat_hierarchy) for cat in cat_set}
    cat_to_parent = make_child_parent_dict(cat_hierarchy)
    user_cat_preference = compute_user_preference(uid_bid, bid_cat)

    master = {uid: {} for uid in user_prof}
    for uid in master:
        user_cats = user_prof[uid].index
        user_cats_depth = pd.Series([cat_depth[cat] for cat in user_cats],
                                    index=user_cats)
        user_cats_depth.sort()
        for cat in user_cats_depth.index:
            master[uid] = insert_leaf(master[uid], cat, uid,
                                      user_cat_preference, cat_to_parent)

    return master


def category_experts(uid_bid, bid_cat):
    """
    relates a user id to the number of reviews they have made for that category

    :param uid_bid: uid to bid dictionary
    :type uid_bid: {uid: [bids]}
    :param bid_cat: bid to cat dictionary
    :type bid_cat: {bid: [categories]}

    :returns: category name to experts Series
    :rtype: {cat: pd.Series(counts, index=uids)}
    """
    #create a dictionary that relates users to categories they review for
    uid_catcount = {
        uid: reduce(add, [Counter(bid_cat[bid]) for bid in bids])
        for uid, bids in uid_bid.iteritems()
    }

    # set of all categories
    catset = {cat for bid in bid_cat for cat in bid_cat[bid]}

    cat_vecs = {}
    for cat in catset:
        counts = [catcounter[cat] for _, catcounter in uid_catcount.iteritems()
                  if cat in catcounter]
        uids = [uid for uid, catcounter in uid_catcount.iteritems()
                if cat in catcounter]
        cat_vecs[cat] = pd.Series(counts, index=uids)

    return cat_vecs


def compute_rating(user, experts, ratings):
    """
    computes a rating for a user by weighting similarity

    :param user: preference tree from the user we try to predict
    :type user: {(cat: score): <recursive>}
    :param experts: dictionary of local experts to their preference trees
    :type experts: {uid: {(cat: score): <recursive>}}
    :param ratings: dictionary of local expert to their rating for the
        particular business
    :type ratings: {uid: float}

    :returns: predicted rating via weighted linear combination
    :rtype: float
    """

    # construct a dictionary of expert: similarity-to-user
    sim = {
        expert: user_similarity(user, experts[expert])
        for expert in experts
    }

    # calculate total in order to normalize similarities
    total = sum(sim.values())

    # case where there are no experts
    if not total:
        return 0

    # return the two ratings if there is a rating
    return sum([sim[i] * ratings[i] / total for i in sim])


def local_experts(user, expertise, potential_experts):
    """
    finds the local experts for a given user pre-limited to a particular
    business

    :param user: user preference tree
    :type user: {(cat: float): <recursive>}
    :param expertise: dictionary of categories to a pd.Series of the experts for
        the category and their category expertise
    :type expertise: {cat: pd.Series(float, index = uid)}
    :param potential_experts: list of uids who have reviewed the business in
        question
    :type potential_experts: list

    :returns: expert uids
    :rtype: list
    """

    experts = []
    for level in xrange(get_user_depth(user)):
        _, cat_scores = get_dict_at_level(user, level)
        least_pref_weight = min(cat_scores.values())
        for cat in cat_scores:
            # case for which the category is team-made (not Yelp-provided)
            if cat.islower():
                continue

            # case when the category is indeed Yelp-provided
            k = int(np.ceil(abs(cat_scores[cat] / least_pref_weight)))
            experts.extend(
                expertise[cat].filter(potential_experts).nlargest(k).index)

    return list(set(experts))


def lbpa_single(uid, bid, preferences, expertise, train, bid_uid, train_uids):
    """
    helper function to lbpa to allow for easy parallelization
    returns the predicted rating for (uid, bid)
    """
    #Create the user preference tree
    pref_tree = preferences[uid]
    # find local experts for the users
    potential_experts = bid_uid[bid].intersection(train_uids)
    eids = local_experts(pref_tree, expertise, potential_experts)
    # make sure a user cannot be their own expert
    if uid in eids:
        eids.remove(uid)
    # if there are no experts, predict 0
    if not eids:
        return {(uid, bid): 0}
    else:
        # some experts at this point might be included because they
        # have a review in both the test and training set, despite
        # not having reviewed the business for which we care
        # we remove them here
        eids = [eid for (eid, _) in train
                if eid in eids and (eid, bid) in train]
        #find preferences trees for the experts
        experts = {eid: preferences[eid] for eid in eids}
        #find the ratings
        ratings = {eid: train[(eid, bid)] for eid in eids}
        #compute predictions
        pred = compute_rating(pref_tree, experts, ratings)
        return {(uid, bid): pred}


def lbpa(test, preferences, expertise, train, bid_uid):
    """
    :param test: the test set
    :type test: {(uid, bid): true rating}
    :param train: the training set
    :type train: {(uid, bid): true rating}
    :type preferences: {uid: preference tree}
    :param preferences: the tree containing all user preference trees
    :type expertise: {cat: {uid: expertise}}
    :param expertise: relates categories to users and their expertise
    :type bid_uid: {bid: set(uids)}
    :param bid_uid: relates bids to the uids who've reviewed the bid

    :returns: rating prediction based on a weighted linear combination of experts
    :rtype: {(uid, bid): rating}
    """

    train_uids = [uid for (uid, _) in train]
    preds = parmap.starmap(lbpa_single, test, preferences, expertise, train,
                           bid_uid, train_uids)

    return {
        (uid, bid): guess
        for pred in preds for (uid, bid), guess in pred.iteritems()
    }


def cross_val(data, preferences, expertise, bid_uid):
    """
    :param data: the data
    :type data: {(uid, bid): rating}
    :type preferences: {uid: preference tree}
    :param preferences: the tree containing all user preference trees
    :type expertise: {cat: {uid: expertise}}
    :param expertise: relates categories to users and their expertise
    :type bid_uid: {bid: set(uids)}
    :param bid_uid: relates bids to the uids who've reviewed the bid

    :returns: predictions for every entry of the input data
    :rtype: {(uid, bid): rating}
    """
    #Create folds for cross_val
    kf = cross_validation.KFold(len(data), n_folds=5, shuffle=True)

    # prepare the outputs
    totalRes = {}

    # for each fold, record predictions
    fold_in_progress = 1
    for train_index, test_index in kf:
        print 'starting on fold', fold_in_progress
        train_keys = np.array(data.keys())[train_index]
        test_keys = np.array(data.keys())[test_index]
        train = {tuple(key): data[tuple(key)] for key in train_keys}
        test = {tuple(key): data[tuple(key)] for key in test_keys}
        results = lbpa(test, preferences, expertise, train, bid_uid)
        totalRes.update(results)
        fold_in_progress += 1

    return totalRes


cat_hierarchy = make_category_tree()
cat_ancestry = make_cat_ancestry_dict(cat_hierarchy)

rev = MongoClient().yelpdb.rev
bus = MongoClient().yelpdb.bus

bid_city = {b['business_id']: b['true_city'] for b in bus.find()}
bid_cat = {b['business_id']: b['categories'] for b in bus.find()}

if __name__ == '__main__':
    # runs lbpa on the cities given as command line arguments
    # if no city is given, Charlotte is assumed

    # get list of valid options
    city_options = set(bus.distinct('true_city'))
    args = set(sys.argv[1:])
    if not args:
        args = raw_input(
            'Choose input cities (e.g. \'Charlotte Pittsburgh\')\n')
        args = {str.capitalize() for str in args.split()}
    cities = args.intersection(city_options)
    cities = cities if cities else city_options

    # check for invalid options
    bad_args = sorted(list(args.difference(city_options)))
    if bad_args:
        print 'the following given arguments are not accepted city choices:'
        print bad_args

    # print list of included cities
    print 'this module will run lbpa with the following cities included:'
    print sorted(cities)

    # gather the data
    print 'gathering the data...'
    data = {
        (r['user_id'], r['business_id']): r['stars']
        for r in rev.find() if bid_city[r['business_id']] in cities
    }
    uid_bid = defaultdict(list)
    for (uid, bid) in data:
        uid_bid[uid].append(bid)
    print '    {} reviews were included'.format(len(data))

    # generate preference trees
    print 'generating preference trees...'
    preferences = make_all_user_pref_trees(uid_bid, bid_cat, cat_ancestry,
                                           cat_hierarchy)

    print 'generating category experts...'
    expertise = category_experts(uid_bid, bid_cat)

    print 'generating a business_id to user_id dictionary...'
    bid_uid = defaultdict(set)
    for (uid, bid) in data:
        bid_uid[bid].add(uid)

    print 'generating predictions...'
    pred = cross_val(data, preferences, expertise, bid_uid)

    print 'calculating rmse'
    deltas = {
        (uid, bid): pred[(uid, bid)] - data[(uid, bid)]
        for (uid, bid) in pred
    }
    errors = np.array(deltas.values())
    rmse = np.sqrt(np.mean(errors ** 2))

    print '\nrmse: {}'.format(rmse)

    save = raw_input(
        'Would you like to save the results? (y/[n])\n').lower() == 'y'
    if save:
        suggested_fname = 'lbpa-pred.pickle'
        fname = raw_input(
            'Where would you like to save the results? [{}]\n'.format(
                suggested_fname))
        print 'saving the deltas to {}'.format(fname)
        fname = fname if fname else suggested_fname
        with open(fname, 'w') as f:
            pickle.dump(deltas, f)

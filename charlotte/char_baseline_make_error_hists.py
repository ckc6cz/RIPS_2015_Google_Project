"""
run this script to produce the histograms for the baseline section of the report
"""

from char_baseline import *
import numpy as np

from char_make_rating_conditioned import *
make_char_rating_matrix_conditioned('all00.csv', 0, 0)
make_char_rating_matrix_conditioned('all35.csv', 3, 5)
make_char_rating_matrix_conditioned('rest00.csv', 0, 0, ['Food', 'Restaurants'
                                                         ])
make_char_rating_matrix_conditioned('rest35.csv', 3, 5, ['Food', 'Restaurants'
                                                         ])

all00 = np.genfromtxt('all00.csv', delimiter=',')
all35 = np.genfromtxt('all35.csv', delimiter=',')
rest00 = np.genfromtxt('rest00.csv', delimiter=',')
rest35 = np.genfromtxt('rest35.csv', delimiter=',')

test(all00, 1, '../MidtermReport/Graphics/char_baseline_hist_all00.pdf',
     normed=True,
     ymax=0.7)
test(all35, 1, '../MidtermReport/Graphics/char_baseline_hist_all35.pdf',
     normed=True,
     ymax=0.7)
test(rest00, 1, '../MidtermReport/Graphics/char_baseline_hist_rest00.pdf',
     normed=True,
     ymax=0.7)
test(rest35, 1, '../MidtermReport/Graphics/char_baseline_hist_rest35.pdf',
     normed=True,
     ymax=0.7)

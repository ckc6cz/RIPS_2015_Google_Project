from pymongo import MongoClient
import numpy as np

# connect to Mongo and alias the rev table
client = MongoClient()
rev = client.char.rev

# get a sorted list of uids and bids
uids = rev.distinct('user_id')
uids.sort()
bids = rev.distinct('business_id')
bids.sort()

rating = np.empty((len(uids), len(bids)))
rating.fill(np.nan)

for r in rev.find():
    row = uids.index(r['user_id'])
    col = bids.index(r['business_id'])
    rating[row, col] = r['stars']

np.savetxt('char_rating_all.csv', rating, delimiter=',')

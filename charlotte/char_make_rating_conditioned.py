"""
INCOMPLETE

provides a call-able function to create a reduced ratings matrix for charlotte
based on a defined minimum review count for users and businesses

author: daniel
"""

from pymongo import MongoClient
import numpy as np
import pandas as pd


def make_char_rating_matrix_conditioned(fname='out.csv',
                                        min_user_rev_count=3,
                                        min_bus_rev_count=5,
                                        cats_to_include=[]):
    """
    creates a rating matrix for charlotte conditioned such that all users
    and businesses in the matrix have a number of reviews as specified by
    the parameters and that the counted reviews are contained in the resultant
    matrix. matrix is written to csv with filename fname
    """
    # connect to Mongo and alias the rev table
    client = MongoClient()
    rev = client.char.rev
    bus = client.char.bus

    # get uids and bids
    uids = rev.distinct('user_id')
    uids.sort()
    bids = rev.distinct('business_id')
    bids.sort()

    # prepare the empty rating matrix
    rating = np.empty((len(uids), len(bids)))
    rating.fill(np.nan)

    if not cats_to_include:  # fill it in without any limitations
        for r in rev.find():
            row = uids.index(r['user_id'])
            col = bids.index(r['business_id'])
            rating[row, col] = r['stars']
    else:  # fill it in including only businesses with cats in cats_to_include
        cats_to_include = set(cats_to_include)
        bid_to_cat = {b['business_id']: b['categories'] for b in bus.find()}
        for r in rev.find():
            if cats_to_include.intersection(bid_to_cat[r['business_id']]):
                row = uids.index(r['user_id'])
                col = bids.index(r['business_id'])
                rating[row, col] = r['stars']

    # eliminate users are businesses that are too sparse
    while any((rating > 0).sum(axis = 0) < min_bus_rev_count) or \
        any((rating > 0).sum(axis = 1) < min_user_rev_count):
        rating = np.delete(rating, np.where(
            (rating > 0).sum(axis=1) < min_user_rev_count),
                           axis=0)
        rating = np.delete(rating, np.where(
            (rating > 0).sum(axis=0) < min_bus_rev_count),
                           axis=1)
        # check against the empty array edge case
        if not rating.any(): break

    np.savetxt(fname, rating, delimiter=',')

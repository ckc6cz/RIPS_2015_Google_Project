"""
make a dictionary for reviews in charlotte
    keys are user_ids
    values are business_ids that the corresponding user has reviewed

City: Charlotte

Author: Daniel
"""

from pymongo import MongoClient
from collections import defaultdict
import pickle

# connect to the Mongo database
client = MongoClient()
# alias the user, bus, and rev tables
user = client.char.user
bus = client.char.bus
rev = client.char.rev

# create a dictionary whose keys are user_id s and whose values are lists of
# businesses that the given user has reviewed
out = defaultdict(list)
for r in rev.find():
    out[r['user_id']].append(r['business_id'])

# export the dictionary in pickle format
with open('char_uid_bid_dict.pickle', 'w') as f:
    pickle.dump(out, f)

# to open it back up, run:
# with open('char_uid_bid_dict.pickle', 'r') as f:
#   <var_name> = pickle.load(f)

"""
predictor model that uses only offsets (absolute avg, user avg, bus avg)
"""

# make some data
# from char_make_rating_conditioned import *
# make_char_rating_matrix_conditioned()

# read in the data
# data = np.genfromtxt('out.csv', delimiter = ',')

# Alternatively, use this:
# import numpy as np
# from char_make_rating_conditioned import *
# make_char_rating_matrix_conditioned('all00.csv', 0, 0)
# make_char_rating_matrix_conditioned('all35.csv', 3, 5)
# make_char_rating_matrix_conditioned('rest00.csv', 0, 0, ['Food', 'Restaurants'])
# make_char_rating_matrix_conditioned('rest35.csv', 3, 5, ['Food', 'Restaurants'])
#
# all00 = np.genfromtxt('all00.csv', delimiter = ',')
# all35 = np.genfromtxt('all35.csv', delimiter = ',')
# rest00 = np.genfromtxt('rest00.csv', delimiter = ',')
# rest35 = np.genfromtxt('rest35.csv', delimiter = ',')

# test(all00, summary=True)
# test(all35, summary=True)
# test(rest00, summary=True)
# test(rest35, summary=True)


def baseline_model(data):
    import numpy as np

    # find the non-nan entries
    rev_loci = np.where(np.isfinite(data))
    rev_loci = np.array(rev_loci).swapaxes(0, 1)

    # choose 20% of them to be part of the test set
    choices = np.random.choice(range(rev_loci.shape[0]), rev_loci.shape[0] / 5)

    # make the test set, tracking their coordinates and value
    test_loci = rev_loci[choices, :]
    test_vals = np.array([data[row, col] for row, col in test_loci])

    # prepare the training set
    train = np.array(data)
    for row, col in test_loci:
        train[row, col] = np.nan

    # impute back the values
    guess = []
    avg = np.nanmean(train)
    for row, col in test_loci:
        off_u = np.nanmean(train[row, :]) - avg
        if np.isnan(off_u): off_u = 0
        off_b = np.nanmean(train[:, col]) - avg
        if np.isnan(off_b): off_b = 0
        guess.append(avg + off_u + off_b)

    guess = np.array(guess)

    return test_vals, guess


def rmse(x, y):
    import numpy as np
    return np.sqrt(np.mean((x - y) ** 2))


def test(data,
         trials=1,
         fname='',
         title='Histogram of Errors',
         xlab='Error Magnitude',
         ylab='Frequency',
         summary=False,
         normed=False,
         ymax=0):
    import numpy as np
    e_straight = []
    e_round = []
    for i in range(trials):
        test, guess = baseline_model(data)
        e_straight.append(rmse(test, guess))
        guess_round = np.round(guess)
        guess_round[guess_round > 5] = 5
        guess_round[guess_round < 1] = 1
        e_round.append(rmse(test, guess_round))
    print "avg rmse: ", round(np.mean(e_straight), 4), \
        "+-", round(np.std(e_straight) / np.sqrt(trials), 4)
    print "avg rounded: ", round(np.mean(e_round), 4), \
        "+-", round(np.std(e_round) / np.sqrt(trials), 4)
    if fname:
        import matplotlib.pyplot as plt
        import matplotlib
        from latexify import latexify
        latexify()
        plt.hist(np.abs(test - guess),
                 bins=np.arange(0, 5, 1.0),
                 normed=normed)
        plt.title(title)
        plt.xlabel(xlab)
        plt.ylabel(ylab)
        if ymax:
            plt.ylim((0, ymax))
        plt.tight_layout()
        plt.savefig(fname, dpi=300)
        plt.clf()
    if summary:
        import pandas as pd
        print pd.DataFrame(np.abs(guess - test)).describe().transpose()

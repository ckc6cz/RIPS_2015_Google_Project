"""
Create a Pandas DataFrame whose rows and columns are business_id s and whose
cells are defined as the distance in miles between the listed geo-coordinates
of the row and column businesses

City: Charlotte

Author: Daniel
"""

from pymongo import MongoClient
import numpy as np
import pandas as pd
from geopy.distance import vincenty

client = MongoClient()
bus = client.char.bus

bids = bus.distinct('business_id')
dist = pd.DataFrame(index=bids, columns=bids)

for b1 in bus.find():
    for b2 in bus.find():
        if not np.isnan(dist.ix[b1['business_id'], b2['business_id']]):
            continue
        b1_coor = (b1['latitude'], b1['longitude'])
        b2_coor = (b2['latitude'], b2['longitude'])
        b1b2_dist = vincenty(b1_coor, b2_coor).meters
        dist.ix[b1['business_id'], b2['business_id']] = b1b2_dist
        dist.ix[b2['business_id'], b1['business_id']] = b1b2_dist

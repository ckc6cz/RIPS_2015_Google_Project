"""
get some information concerning business-neighborhood assignments

city: charlotte

author: daniel
"""

import fiona
import shapely.geometry as geo
from pymongo import MongoClient
from collections import defaultdict
import numpy as np

# intialize a dictionary whose keys are neighborhood names and whose values
# are some form of polygons
nbs = {}

# fill in said dictionary
with fiona.open("../data/ZillowNeighborhoods-NC.shp") as fiona_collection:
    for record in fiona_collection:
        shape = geo.asShape(record['geometry'])
        name = record['properties']['NAME']
        nbs[name] = shape

# connect to mongo and alias the bus database
client = MongoClient()
bus = client.char.bus

# get a list of coordinate tuples
b_coords = []
for b in bus.find():
    b_coords.append((b['latitude'], b['longitude']))

# initialize a dictionary of neighborhood counts
nb_counts = defaultdict(int)
trials = 0

# iterate through the businesses and mark neighborhood hits
for coord in b_coords:
    trials += 1
    point = geo.Point(coord[1], coord[0])
    for name in nbs:
        if nbs[name].contains(point):
            nb_counts[name] += 1
            continue
    print np.sum(nb_counts.values()), "/", trials

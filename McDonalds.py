# -*- coding: utf-8 -*-
"""
Created on Wed Jul 29 16:00:07 2015

@author: cchandler
"""
from collections import defaultdict
from pymongo import MongoClient

rev = MongoClient().yelpdb.rev

#since reviews only have business id, get list of bids that correspond to McD's
bus = MongoClient().yelpdb.bus
bids = []
for entry in bus.find({'name': 'McDonald\'s'}):
    bids.append(entry['business_id'])

    #get dictionary of bid -> reviews
mcd_revs = defaultdict(list)
for bid in bids:
    reviews = [r['text'] for r in rev.find({'business_id': bid})]
    mcd_revs[bid] = reviews

#which bids go to which city
city_to_bids = defaultdict(list)
for bid in bids:
    for b in bus.find({'business_id': bid}):
        if b['true_city'] in city_to_bids.keys():
            city_to_bids[b['true_city']].append(bid)
        else:
            city_to_bids[b['true_city']] = [bid]

#create a dictionay that maps city name to all the reviews for mcdonalds in that city
city_to_reviews = defaultdict(list)
for city, bids in city_to_bids.iteritems():
    for bidd, reviews in mcd_revs.iteritems():
        if bidd in bids:
            city_to_reviews[city].append(reviews)

# -*- coding: utf-8 -*-
"""
Created on Wed Jun 24 11:52:17 2015

@author: Tamar Lichter

description:
Ideally we'd like to find the fraction  of elite users in each city, but we
can't do that yet. This does two separate things: find the general fraction of
users that are elites, and create a list of businesses in a selected city. It's
not usable for anything right now.

"""

import json
import itertools
import matplotlib.pyplot as plt
import assign_true_cities as atc

# Find the fraction of elite users

user_file = '/home/tlichter/Documents/yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_user.json'

user_data = []
with open(user_file) as user_data_file:
    for line in user_data_file:
        user_data.append(json.loads(line))

num_elites = 0
num_users = len(user_data)

for user in user_data:
    if user['elite'] != []:
        num_elites += 1

print float(num_elites) / float(num_users)

# Create a list of businesses in a selected city.

business_file = '/home/tlichter/Documents/yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_business.json'

business_data = []
with open(business_file) as business_data_file:
    for line in business_data_file:
        business_data.append(json.loads(line))

atc.assign_true_cities(business_data)

selected_city = 'Karlsruhe'

selected_city_list = []
for bus in business_data:
    if bus['true_city'] == selected_city:
        selected_city_list.append(bus)

# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 09:59:30 2015

@author: mribero

Create test and trainig sets
"""

import pandas as pd
from pymongo import MongoClient
from sklearn import cross_validation
from LBPA import lpba
#Create data for charlotte
client = MongoClient()
db = client.char

char_rev = db.rev

data = {}
for r in char_rev.find():
    u = r['user_id']
    b = r['business_id']
    data[(u, b)] = r['stars']


def crossVal(data):
    n = len(data)
    #Create folds for crossVal
    kf = cross_validation.Kfold(n, n_folds=5)
    for train_index, test_index in kf:
        train, test = data.keys()[train_index], data.keys()[test_index]
        results, resultsMax = lbpa(test, preferences, expertise, train)
        #CONCATEMAR

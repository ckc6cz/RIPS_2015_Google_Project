"""
authors: daniel and tamar

contains various helper files for working with trees
"""


def get_cat_depth(cat, supertree):
    import numpy as np
    if not supertree:
        return np.nan
    if cat in supertree.keys():
        return 0
    return 1 + np.nanmin([get_cat_depth(cat, supertree[root]) \
        for root in supertree.keys()])


def tree_crawler(tree):
    if not tree:
        return set()
    out = set(tree.keys())
    return out.union(*[tree_crawler(tree[key]) for key in tree])


def make_child_parent_dict(tree):
    out = {}
    for key in tree:
        for value in tree[key].keys():
            out[value] = key
        out.update(make_child_parent_dict(tree[key]))
    return out


def make_cat_ancestry_dict():
    """
    make a dictionary for cats to a set of parent cats
    """
    from HierarchyCreation import make_category_tree
    cat_tree = make_category_tree()
    cat_set = tree_crawler(cat_tree)
    cat_to_parent = make_child_parent_dict(cat_tree)

    out = {cat: set() for cat in cat_set}
    for cat in out:
        if cat in cat_to_parent:
            lineage = [cat_to_parent[cat]]
            while lineage[-1] in cat_to_parent:
                lineage.append(cat_to_parent[lineage[-1]])
            [out[cat].add(ancestor) for ancestor in lineage]
    return out

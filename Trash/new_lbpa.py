from UserAndTree import make_all_user_pref_trees
from char_make_bid_uid_dict import char_make_bid_uid_dict


def computeRating(user, experts, ratings):
    """
    computes a rating for a user by weighting similarity

    args:
        user (dict): preference tree from the user we try to predict
        experts (dict): {local experts for 'user': their preference trees}
        ratings (dict): {local expert: rating for particular business}

    returns:
        float: predicted rating via weighted linear combination
        float: predicted ratings via only most similar expert
    """
    from lbpa_compute_similarity import similarity

    # construct a dictionary of expert: similarity-to-user
    sim = {expert: similarity(user, experts[expert]) for expert in experts}

    # calculate total in order to normalize similarities
    total = sum(sim.values())

    # case where there are no experts
    if not total:
        return 0, 0

    # find the most-similar
    mostSim = max(sim, key=sim.get)

    # return the two ratings if there is a rating
    return sum([sim[i] * ratings[i] / total for i in sim]), ratings[mostSim]


def localExperts2(user, expertise, potential_expert):
    """
    finds the local experts for a given user pre-limited to a particular
    business

    args:
        user (dict): {(cat (string), pref: float): <recursive-tree-here>}
        expertise (dict):
            {cat (string): pd.Series(expertise (float), index = uid (string)}
        potential_expert (list): uids who have reviewed the business in question

    returns:
        list: expert uids
    """
    import numpy as np
    from lbpa_compute_similarity import get_user_depth, get_dict_at_level

    experts = []
    for level in range(get_user_depth(user)):
        _, cat_scores = get_dict_at_level(user, level)
        #least_pref_weight = min(map(lambda x: x[1], cat_scores.values()))
        least_pref_weight = min(cat_scores.values())
        for cat in cat_scores:
            # case for which the category is team-made (not Yelp-provided)
            if cat.islower():
                continue

            # case when the category is indeed Yelp-provided
            k = int(np.ceil(abs(cat_scores[cat] / least_pref_weight)))
            experts.extend(
                expertise[cat].filter(potential_expert).nlargest(k).index)

    return list(set(experts))


def lbpa(test, preferences, expertise, train, bid_to_uid):
    """
    args:
        test (dict): {(uid (string), bid (string)): true rating (int)}
            the test set
        train (dict): {(uid (string), bid (string)): true rating (int)}
            the training set
        preferences (dict): {uid (string): preference tree}
            the tree containing all user preference trees
        expertise (dict): {cat (string): {uid: expertise}}
            relates categories to users and their expertise
        bid_to_uid (dict): {bid (string): uids (set)}
            relates bids to the uids who've reviewed the bid

    returns:
        dict {(uid, bid): rating}: rating prediction based on a weighted linear
            combination of experts
        dict {(uid, bid): rating}: rating based upon only the most similar
            expert
    """
    # get the uids of the users in the training set
    train_uids = [key[0] for key in train]

    # prepare the output
    predicted = {}
    predictedMax = {}

    # for each rating to predict
    for (uid, bid) in test:
        #Create the user preference tree
        pref_tree = preferences[uid]

        # find local experts for the users
        potential_experts = bid_to_uid[bid].intersection(train_uids)
        eids = localExperts2(pref_tree, expertise, potential_experts)

        # make sure a user cannot be their own expert
        if uid in eids:
            eids.remove(uid)

        # if there are no experts, predict 0
        if not eids:
            predicted[(uid, bid)] = 0
            predictedMax[(uid, bid)] = 0

        else:
            # some experts at this point might be included because they
            # have a review in both the test and training set, despite
            # not having reviewed the business for which we care
            # we remove them here
            eids = filter(lambda eid: (eid, bid) in train.keys(), eids)

            #find preferences trees for the experts
            experts = {eid: preferences[eid] for eid in eids}
            #find the ratings
            ratings = {eid: train[(eid, bid)] for eid in eids}
            #compute predictions
            predicted[(uid, bid)], predictedMax[(uid, bid)] = \
                computeRating(pref_tree, experts, ratings)

    return predicted, predictedMax


def crossVal(data, preferences, expertise, bid_uid_dict):
    from sklearn import cross_validation
    import numpy as np

    #Create folds for crossVal
    kf = cross_validation.KFold(len(data), n_folds=5)

    # prepare the outputs
    totalRes = {}
    totalResM = {}

    # for each fold, record predictions
    for train_index, test_index in kf:
        train_keys = np.array(data.keys())[train_index]
        test_keys = np.array(data.keys())[test_index]
        train = {tuple(key): data[tuple(key)] for key in train_keys}
        test = {tuple(key): data[tuple(key)] for key in test_keys}
        results, resultsMax = \
            lbpa(test, preferences, expertise, train, bid_uid_dict)
        totalRes.update(results)
        totalResM.update(resultsMax)

    return totalRes, totalResM


def testComputeRating():
    """
    run this code merely to make sure that computeRating runs without runtime
    errors
    """
    import numpy as np
    expertise = {
        'Food': {'userA': 0.9,
                 'userB': 0.2,
                 'userC': 0.5},
        'Rest': {'userA': 0.8,
                 'userD': 0.3,
                 'userB': 0.5},
        'Asian': {'userA': 0.2,
                  'userB': 0.5},
        'Italian': {'userA': 0.3,
                    'userD': 0.3,
                    'userC': 0.1},
        'French': {'userB': 0.8,
                   'userA': 0.3},
        'Cafe': {'userA': 0.3,
                 'userB': 0.5},
        'Entertainment': {'userA': 0.8,
                          'userB': 0.3,
                          'userC': 0.5}
    }
    userA = {
        ('Food', 1.0): {
            ('Rest', .5):
            {('Asian', .3): {},
             ('Italian', .3): {},
             ('French', .1): {}},
            ('Cafe', .3): {}
        }
    }
    userB = {
        ('Food', 0.8): {
            ('Rest', .5): {('Asian', .1): {},
                           ('Italian', .4): {}},
            ('Cafe', .3): {}
        },
        ('Entertainment', .2): {}
    }
    userC = {
        ('Food', 1.0): {
            ('Rest', .5): {
                ('Asian', np.random.uniform()): {},
                ('Italian', .3): {},
                ('French', .1): {}
            },
            ('Cafe', np.random.uniform()): {}
        }
    }
    userD = {
        ('Food', np.random.uniform()): {
            ('Rest', np.random.uniform()): {
                ('Asian', np.random.uniform()): {},
                ('Italian', .4): {}
            },
            ('Cafe', np.random.uniform()): {}
        },
        ('Entertainment', .2): {}
    }

    experts = {'userC': userC, 'userB': userB}
    ratings = {'userB': 5, 'userC': 4}

    lincomb, maxsim = computeRating(userA, experts, ratings)


def runAllTests():
    testComputeRating()
    testLocalExperts2()


def testrun(limit=0, cv=False):
    """ see if lbpa works"""
    import cPickle as pickle

    # connect to mongo and generate our dictionary of ratings
    # from pymongo import MongoClient
    # rev = MongoClient().char.rev
    # ratings = {}
    # for review in rev.find():
    #     ratings[(review['user_id'], review['business_id'])] = review['stars']
    data = pickle.load(open('char_ratings.pickle', 'r'))

    #preferences = make_all_user_pref_trees()
    preferences = pickle.load(open('preferences.pickle', 'r'))

    #expertise = category_experts()
    expertise = pickle.load(open('cat_experts.pickle', 'r'))

    bid_uid_dict = pickle.load(open('bid_to_uid.pickle', 'r'))

    if limit:
        data = {key: data[key] for key in data.keys()[:limit]}

    if cv:
        pred, predM = crossVal(data, preferences, expertise, bid_uid_dict)
    else:
        split = len(data) / 5
        test = {v: data[v] for v in data.keys()[:split]}
        train = {v: data[v] for v in data.keys()[split:]}
        pred, predM = lbpa(test, preferences, expertise, train, bid_uid_dict)

    return pred, predM

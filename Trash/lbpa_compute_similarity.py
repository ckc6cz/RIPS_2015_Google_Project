# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 09:21:32 2015

@author: tlichter

description:  Measures the similarity between two users based on an existing
weighted category hierarchy. See section 4.2 in "Location-based and
Preference-Aware Recommendation Using Sparse Geo-Social Networking Data."

"""

import numpy as np

# assumes the existence of a business category hierarchy
# in the form of a nested dictionary with three levels
# as well as corresponding dictionaries for each user's preferences
# where each key is a tuple (category, weight)


def get_user_depth(user):
    if user:
        return 1 + max([get_user_depth(user[key]) for key in user.keys()])
    return 0


def get_dict_at_level(user, level):
    if level >= get_user_depth(user):
        return set(), {}
    if not level:
        cats = {cat for cat, score in user.keys()}
        cat_scores = dict(user.keys())
        return cats, cat_scores
    cats = set()
    cat_scores = dict()
    for key in user:
        cat, cat_s = get_dict_at_level(user[key], level - 1)
        cats = cats.union(cat)
        cat_scores.update(cat_s)
    return cats, cat_scores


def level_similarity(userA, userB):
    '''
    Takes in two nested dictionaries of user preferences. Computes the
    similarity of two users at each level of a category hierarchy. To be used in
    computing the total similarity between two users. Returns a list of
    similarity values, ordered from top level to deepest level.
    '''
    similarity = np.zeros(max(get_user_depth(userA), get_user_depth(userB)))
    for level in range(len(similarity)):
        A_cat, A_cat_scores = get_dict_at_level(userA, level)
        B_cat, B_cat_scores = get_dict_at_level(userB, level)
        for cat in A_cat:
            if cat in B_cat:
                similarity[level] += min(A_cat_scores[cat], B_cat_scores[cat])
    return similarity


def level_entropy(user):
    '''
    Takes in one nested dictionary of user preferences.
    Computes the entropy of a user's preferences at each level of a category hierarchy.
    To be used in computing the total similarity between two users.
    Returns a list of entropy values, ordered from top level to deepest level.
    '''
    entropy = np.zeros(get_user_depth(user))
    for level in range(len(entropy)):
        cat, scores = get_dict_at_level(user, level)
        score_total = np.sum(scores.values())
        prob_list = [score / score_total for score in scores.values()]
        entropy[level] -= np.sum(prob_list * np.log2(prob_list))
    return entropy


def similarity(userA, userB):
    '''
    Takes in two dictionaries of user preferences
    Returns the total similarity between the users
    '''
    # get similarity at each level as a list
    sim_at_level = level_similarity(userA, userB)
    # get entropy at each level as a list
    entropyA = level_entropy(userA)
    entropyB = level_entropy(userB)
    # compute total similarity (see Eq. 8 in LBPA paper)
    sim = 0
    for level in range(min(get_user_depth(userA), get_user_depth(userB))):
        sim += 2 ** (1 + level) * (sim_at_level[level] \
                / (1 + abs(entropyA[level] - entropyB[level])))
    return sim

# # test
# userA = {('Food',1.0): {('Rest',.5): {('Asian',.3): {},('Italian',.3):{}, ('French',.1):{}}, ('Cafe',.3): {}}}
# userB = {('Food',0.8): {('Rest',.5): {('Asian',.1): {},('Italian',.4):{}}, ('Cafe',.3): {}}, ('Entertainment',.2):{}}
# print level_similarity(userA,userB)
# print level_entropy(userA)
# print level_entropy(userB)
# print similarity(userA, userB)

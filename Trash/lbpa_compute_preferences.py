# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 10:35:26 2015

@author: tlichter

description: Computes a user's preference weight for each category. See section
3.2 in "Location-based and Preference-Aware Recommendation Using Sparse Geo-
Social Networking Data." Still needs the dictionary described below.

"""

from __future__ import division
import numpy as np
import copy
from pymongo import MongoClient
from collections import defaultdict
import pickle

#open users to businesses dictionary
with open('char_uid_bid_dict.pickle', 'r') as f:
    user_to_businesses = pickle.load(f)

#get database info
client = MongoClient()
bus = client.char.bus
user = client.char.user

#create a dictionary that relates businesses to their categories
bid_to_cat = {}
for b in bus.find():
    bid_to_cat[b['business_id']] = b['categories']

#create a dictionary that relates users to categories they review for
user_to_cat = defaultdict(list)
for b in bus.find():
    for u, bu in user_to_businesses.iteritems():
        if b['business_id'] in bu:
            user_to_cat[u].append(b['categories'])

#make users' categories into a single list
for u, c in user_to_cat.iteritems():
    newcat = reduce(lambda x, y: x + y, c)
    user_to_cat[u] = newcat

    #dictionary that relates user to a dictionary of their categories with counts of each
for key, value in user_to_cat.iteritems():
    dict_of_counts = {}
    for v in value:
        if v in dict_of_counts:
            dict_of_counts[v] += 1
        else:
            dict_of_counts[v] = 1
    user_to_cat[key] = dict_of_counts

# create a new dictionary that maps the same users to the same categories
user_profiles = copy.deepcopy(user_to_cat)

num_users = len(user_to_cat)

# find how many users have visited each category
total_visitors = dict()
for user in user_to_cat:
    for category in user_to_cat[user]:
        if category in total_visitors:
            total_visitors[category] += 1
        else:
            total_visitors[category] = 1

# this is Eq.5 in the LBPA paper.
# user preference for a category = 
#    (his visits to the category / his total visits everywhere) * 
#       log_2 (total num users / num users who have visited the category)
for user in user_to_cat:
    user_visits_total = sum(user_to_cat[user].values())
    for category in user_to_cat[user]:
        user_profiles[user][category] = \
            (user_to_cat[user][category] / user_visits_total) * \
            np.log2(num_users/total_visitors[category])

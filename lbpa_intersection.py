"""
tests lbpa on its intersectional strengths

generates a test set based on reviews in Vegas by users whose hometown is
elsewhere

trains on reviews in Vegas by users whose hometown is Vegas
"""

import numpy as np
import cPickle as pickle
from pymongo import MongoClient
from collections import defaultdict, Counter
from itertools import chain
from lbpa_nonparallel import (cat_hierarchy, cat_ancestry, bid_city, bid_cat,
                              make_all_user_pref_trees, category_experts, lbpa)

rev = MongoClient().yelpdb.rev
bus = MongoClient().yelpdb.bus

if __name__ == '__main__':
    rev = MongoClient().yelpdb.rev
    bus = MongoClient().yelpdb.bus

    print 'assigning home cities for each user'
    uid_citycount = defaultdict(Counter)
    for r in rev.find():
        uid_citycount[r['user_id']][bid_city[r['business_id']]] += 1

    uid_homecity = {
        uid: citycount.most_common(1)[0][0]
        for uid, citycount in uid_citycount.iteritems()
    }

    print 'assembling the test and training data'
    test, train = {}, {}
    uid_bid = defaultdict(list)
    for r in rev.find():
        if bid_city[r['business_id']] == 'Las Vegas' and \
                uid_homecity[r['user_id']] == 'Las Vegas':
            train[(r['user_id'], r['business_id'])] = r['stars']
            uid_bid[r['user_id']].append(r['business_id'])
        elif bid_city[r['business_id']] == 'Las Vegas' and \
                uid_homecity[r['user_id']] != 'Las Vegas':
            test[(r['user_id'], r['business_id'])] = r['stars']
            uid_bid[r['user_id']].append(r['business_id'])

    print 'generating preference trees...'
    preferences = make_all_user_pref_trees(uid_bid, bid_cat, cat_ancestry,
                                           cat_hierarchy)

    print 'generating category experts...'
    expertise = category_experts(uid_bid, bid_cat)

    print 'generating a business_id to user_id dictionary...'
    bid_uid = defaultdict(set)
    for (uid, bid) in chain(test, train):
        bid_uid[bid].add(uid)

    print 'generating predictions...'
    pred = lbpa(test, preferences, expertise, train, bid_uid)

    print 'calculating rmse'
    delta = [pred[(uid, bid)] - test[(uid, bid)] for (uid, bid) in pred]
    delta = np.array(delta)
    rmse = np.sqrt(np.mean(delta ** 2))

    print '\nrmse: {}'.format(rmse)

    save = raw_input(
        'Would you like to save the results? (y/[n])\n').lower() == 'y'
    if save:
        suggested_fname = 'lbpa-pred.pickle'
        fname = raw_input(
            'Where would you like to save the results? [{}]\n'.format(
                suggested_fname))
        fname = fname if fname else suggested_fname
        print 'saving the predictions to {}'.format(fname)
        with open(fname, 'w') as f:
            pickle.dump(pred, f)

# -*- coding: utf-8 -*-
"""
Created on Fri Jul 10 09:01:53 2015

@author: cchandler
"""


def delete_key(dict, key):
    del dict[key]
    return dict


def make_category_tree():

    import json
    """
        Using the category.json file from https://www.yelp.com/developers/documentation/v2/all_category_list
        this function creates a nested dictionary that represents the hierarchy of
        categories. There are 12 major categories, some having more categories branching
        from it and some just containing the Yelp categories (the leaves of the 'tree')
    """

    with open('categories.json') as data_file:
        data = json.load(data_file)

    parents = []
    titles = []
    for entry in data:
        parents.append(entry['parents'])
        titles.append(entry['title'])

    pset = set()
    for par in parents:
        for p in par:
            pset.add(str(p))

    dictionary = dict(zip(titles, parents))

    #dict 2 is the parent to children dictionary
    dict2 = {}
    for parent in pset:
        titles = []
        for key, value in dictionary.iteritems():
            if parent in value:
                #remove: None : [u'Restaurants', u'Automotive', u'Home Services', u'Health & Medical', u'Active Life', u'Beauty & Spas', u'Mass Media', u'Professional Services', u'Food', u'Pets', u'Local Services', u'Arts & Entertainment', u'Public Services & Government', u'Education', u'Real Estate', u'Bicycles', u'Shopping', u'Religious Organizations', u'Local Flavor', u'Hotels & Travel', u'Event Planning & Services', u'Nightlife', u'Financial Services']
                if parent == 'None':
                    continue
                else:
                    titles.append(key)
                    dict2[parent] = titles

    values = []
    for key, value in dict2.iteritems():
        for v in value:
            values.append(v)

    #overlap are the categories that are both a major category and are present in the list of another major category
    overlap = []
    for v in values:
        for p in pset:
            if v.lower() == p:
                overlap.append(v.lower())

    #disjoint is the set of categories that do not have a parent
    disjoint = pset.difference(set(overlap))
    disjoint.discard('None')

    #create nested dictionary with the leaves having blank dictionaries
    hierarchy = {}
    for d in disjoint:
        hierarchy[d] = {}
        for value in dict2[d]:
            hierarchy[d][value] = {}
    #fill in next layer
    for key, value in hierarchy.iteritems():
        if len(value) == 0:
            for k in dict2[key]:
                hierarchy[key][k] = {}
    #fill in next layer
    for key, value in hierarchy.iteritems():
        for k, v in value.iteritems():
            if k.lower() in dict2.keys():
                for x in dict2[k.lower()]:
                    hierarchy[key][k][x] = {}

    #add medcenters dentalhygienists c_and_mg diagnosticservices and physicians to health category
    hierarchy['health']['medcenters'] = {}
    for x in dict2['medcenters']:
        hierarchy['health']['medcenters'][x] = {}
    hierarchy['health']['dentalhygienists'] = {}
    for y in dict2['dentalhygienists']:
        hierarchy['health']['dentalhygienists'][y] = {}
    hierarchy['health']['c_and_mh'] = {}
    for z in dict2['c_and_mh']:
        hierarchy['health']['c_and_mh'][z] = {}
    hierarchy['health']['diagnosticservices'] = {}
    for zz in dict2['diagnosticservices']:
        hierarchy['health']['diagnosticservices'][zz] = {}
    hierarchy['health']['physicians'] = {}
    for a in dict2['physicians']:
        hierarchy['health']['physicians'][a] = {}

    #add latin localflavor and mideastern to restaurants
    hierarchy['restaurants']['mideastern'] = {}
    for b in dict2['mideastern']:
        hierarchy['restaurants']['mideastern'][b] = {}
    hierarchy['restaurants']['latin'] = {}
    for c in dict2['latin']:
        hierarchy['restaurants']['latin'][c] = {}
    hierarchy['restaurants']['localflavor'] = {}
    for cc in dict2['localflavor']:
        hierarchy['restaurants']['localflavor'][cc] = {}

    #add gourmet and restaurants to food
    hierarchy['food']['gourmet'] = {}
    for d in dict2['gourmet']:
        hierarchy['food']['gourmet'][d] = {}
    hierarchy['food']['restaurants'] = {}
    for key, value in hierarchy['restaurants'].iteritems():
        hierarchy['food']['restaurants'][key] = {}
        if key in dict2.keys():
            for e in dict2[key]:
                hierarchy['food']['restaurants'][key][e] = {}

    #remove restaurants entry
    hierarchy.pop('restaurants')

    #add fitness sportgoods and bicycles to active
    hierarchy['active']['fitness'] = {}
    for f in dict2['fitness']:
        hierarchy['active']['fitness'][f] = {}
    hierarchy['active']['sportgoods'] = {}
    for g in dict2['sportgoods']:
        hierarchy['active']['sportgoods'][g] = {}
    hierarchy['active']['bicycles'] = {}
    for h in dict2['bicycles']:
        hierarchy['active']['bicycles'][h] = {}

    #add petservices to pets
    hierarchy['pets']['petservices'] = {}
    for i in dict2['petservices']:
        hierarchy['pets']['petservices'][i] = {}

    #add itservices, fincancialservices, flowers, localservices eventservices homeservices homeandgarden and realestate to professional
    hierarchy['professional']['itservices'] = {}
    for j in dict2['itservices']:
        hierarchy['professional']['itservices'][j] = {}
    hierarchy['professional']['financialservices'] = {}
    for k in dict2['financialservices']:
        hierarchy['professional']['financialservices'][k] = {}
    hierarchy['professional']['flowers'] = {}
    for l in dict2['flowers']:
        hierarchy['professional']['flowers'][l] = {}
    hierarchy['professional']['realestate'] = {}
    for m in dict2['realestate']:
        hierarchy['professional']['realestate'][m] = {}
    hierarchy['professional']['localservices'] = {}
    for n in dict2['localservices']:
        hierarchy['professional']['localservices'][n] = {}
    hierarchy['professional']['homeservices'] = {}
    for o in dict2['homeservices']:
        hierarchy['professional']['homeservices'][o] = {}
    hierarchy['professional']['eventservices'] = {}
    for p in dict2['eventservices']:
        hierarchy['professional']['eventservices'][p] = {}
    hierarchy['professional']['homeandgarden'] = {}
    for pp in dict2['homeandgarden']:
        hierarchy['professional']['homeandgarden'][pp] = {}

    #add arstandcrafts media shopping musicinstrumentservices and massmedia to arts
    hierarchy['arts']['artsandcrafts'] = {}
    for q in dict2['artsandcrafts']:
        hierarchy['arts']['artsandcrafts'][q] = {}
    hierarchy['arts']['media'] = {}
    for r in dict2['media']:
        hierarchy['arts']['media'][r] = {}
    hierarchy['arts']['musicinstrumentservices'] = {}
    for t in dict2['musicinstrumentservices']:
        hierarchy['arts']['musicinstrumentservices'][t] = {}
    hierarchy['arts']['massmedia'] = {}
    for u in dict2['massmedia']:
        hierarchy['arts']['massmedia'][u] = {}

    #add specialtyschools to education
    hierarchy['education']['specialtyschools'] = {}
    for v in dict2['specialtyschools']:
        hierarchy['education']['specialtyschools'][v] = {}

    #add hair and hairremoval to beautysvc
    hierarchy['beautysvc']['hair'] = {}
    for w in dict2['hair']:
        hierarchy['beautysvc']['hair'][w] = {}
    hierarchy['beautysvc']['hairremoval'] = {}
    for ww in dict2['hairremoval']:
        hierarchy['beautysvc']['hairremoval'][ww] = {}

    #add transport and auto to hotelstravel
    hierarchy['hotelstravel']['transport'] = {}
    for x in dict2['transport']:
        hierarchy['hotelstravel']['transport'][x] = {}
    hierarchy['hotelstravel']['auto'] = {}
    for y in dict2['auto']:
        hierarchy['hotelstravel']['auto'][y] = {}

    hierarchy = delete_key(hierarchy, 'medcenters')
    hierarchy = delete_key(hierarchy, 'dentalhygienists')
    hierarchy = delete_key(hierarchy, 'c_and_mh')
    hierarchy = delete_key(hierarchy, 'diagnosticservices')
    hierarchy = delete_key(hierarchy, 'physicians')
    hierarchy = delete_key(hierarchy, 'latin')
    hierarchy = delete_key(hierarchy, 'mideastern')
    hierarchy = delete_key(hierarchy, 'localflavor')
    hierarchy = delete_key(hierarchy, 'gourmet')
    hierarchy = delete_key(hierarchy, 'fitness')
    hierarchy = delete_key(hierarchy, 'sportgoods')
    hierarchy = delete_key(hierarchy, 'bicycles')
    hierarchy = delete_key(hierarchy, 'petservices')
    hierarchy = delete_key(hierarchy, 'itservices')
    hierarchy = delete_key(hierarchy, 'financialservices')
    hierarchy = delete_key(hierarchy, 'realestate')
    hierarchy = delete_key(hierarchy, 'localservices')
    hierarchy = delete_key(hierarchy, 'homeservices')
    hierarchy = delete_key(hierarchy, 'homeandgarden')
    hierarchy = delete_key(hierarchy, 'eventservices')
    hierarchy = delete_key(hierarchy, 'artsandcrafts')
    hierarchy = delete_key(hierarchy, 'media')
    hierarchy = delete_key(hierarchy, 'musicinstrumentservices')
    hierarchy = delete_key(hierarchy, 'massmedia')
    hierarchy = delete_key(hierarchy, 'specialtyschools')
    hierarchy = delete_key(hierarchy, 'hair')
    hierarchy = delete_key(hierarchy, 'hairremoval')
    hierarchy = delete_key(hierarchy, 'flowers')
    hierarchy = delete_key(hierarchy, 'auto')
    hierarchy = delete_key(hierarchy, 'transport')

    for key, value in hierarchy.iteritems():
        for k, v in value.iteritems():
            if type(k) == str:
                for k1, v1 in v.iteritems():
                    if k1.lower() in pset:
                        for x in dict2[k1.lower()]:
                            hierarchy[key][k][k1][x] = {}

    #remove key for professional -> eventservices -> Hotels
    hierarchy['professional']['eventservices'][u'Hotels'] = {}

    #rename professional as services
    hierarchy['services'] = hierarchy.pop('professional')

    #rename categories to those of "None":
    hierarchy['food'][u'Restaurants'] = hierarchy['food'].pop('restaurants')
    hierarchy['hotelstravel'][u'Automotive'] = hierarchy['hotelstravel'].pop(
        'auto')
    hierarchy['services'][u'Home Services'] = hierarchy['services'].pop(
        'homeservices')
    hierarchy[u'Health & Medical'] = hierarchy.pop('health')
    hierarchy[u'Active Life'] = hierarchy.pop('active')
    hierarchy[u'Beauty & Spas'] = hierarchy.pop('beautysvc')
    hierarchy['arts'][u'Mass Media'] = hierarchy['arts'].pop('massmedia')
    hierarchy[u'Professional Services'] = hierarchy.pop('services')
    hierarchy[u'Food'] = hierarchy.pop('food')
    hierarchy[u'Pets'] = hierarchy.pop('pets')
    hierarchy[u'Professional Services'][
        u'Local Services'
    ] = hierarchy[u'Professional Services'].pop('localservices')
    hierarchy[u'Arts & Entertainment'] = hierarchy.pop('arts')
    hierarchy[u'Public Services & Government'] = hierarchy.pop(
        'publicservicesgovt')
    hierarchy[u'Education'] = hierarchy.pop('education')
    hierarchy[u'Professional Services'][
        u'Real Estate'
    ] = hierarchy[u'Professional Services'].pop('realestate')
    hierarchy[u'Active Life'][u'Bicycles'] = hierarchy[u'Active Life'].pop(
        'bicycles')
    hierarchy[u'Shopping'] = hierarchy.pop('shopping')
    hierarchy[u'Religious Organizations'] = hierarchy.pop('religiousorgs')
    hierarchy[u'Food'][u'Restaurants'][
        u'Local Flavor'
    ] = hierarchy[u'Food'][u'Restaurants'].pop('localflavor')
    hierarchy[u'Hotels & Travel'] = hierarchy.pop('hotelstravel')
    hierarchy[u'Professional Services'][
        u'Event Planning & Services'
    ] = hierarchy[u'Professional Services'].pop('eventservices')
    hierarchy[u'Nightlife'] = hierarchy.pop('nightlife')
    hierarchy[u'Professional Services'][
        u'Financial Services'
    ] = hierarchy[u'Professional Services'].pop('financialservices')

    # delete doubly listed entries
    del hierarchy['Shopping']['Fashion']['Department Stores']
    del hierarchy['Professional Services']['Event Planning & Services'][
        'Hotels'
    ]
    del hierarchy['Beauty & Spas']['Perfume']
    del hierarchy['Beauty & Spas']['Cosmetics & Beauty Supply']
    del hierarchy['Shopping']['Fashion']['Sports Wear']
    del hierarchy['Professional Services']['Internet Service Providers']
    del hierarchy['Arts & Entertainment']['Wineries']
    del hierarchy['Professional Services']['Home Services']['Real Estate']
    del hierarchy['Active Life']['fitness']['Swimming Lessons/Schools']
    del hierarchy['Food']['Restaurants']['Parent Cafes']
    del hierarchy['Shopping']['Art Galleries']
    del hierarchy['Beauty & Spas']['Hair Extensions']
    del hierarchy['Nightlife']['Music Venues']
    del hierarchy['Nightlife']['Jazz & Blues']
    del hierarchy['Food']['Kiosk']
    del hierarchy['Health & Medical']['Medical Spas']
    del hierarchy['Professional Services']['flowers']['Cards & Stationery']
    del hierarchy['Arts & Entertainment']['artsandcrafts']['Cards & Stationery'
                                                           ]

    return hierarchy
